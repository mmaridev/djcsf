import { isObject, isArray } from "./core/util/types";
import { values } from "./core/util/object";
import { isString } from "./core/util/types";
import { HasProps } from "./core/has_props";
import { ModelResolver } from "./core/resolvers";
export const default_resolver = new ModelResolver(null);
export const Models = new Proxy(default_resolver, {
    get(target, name, receiver) {
        if (isString(name)) {
            const model = target.get(name);
            if (model != null) {
                return model;
            }
        }
        return Reflect.get(target, name, receiver);
    },
    has(target, name) {
        if (isString(name)) {
            const model = target.get(name);
            if (model != null) {
                return true;
            }
        }
        return Reflect.has(target, name);
    },
    ownKeys(target) {
        return target.names;
    },
    getOwnPropertyDescriptor(target, name) {
        if (isString(name)) {
            const model = target.get(name);
            if (model != null) {
                return { configurable: true, enumerable: true, writable: false, value: model };
            }
        }
        return Reflect.getOwnPropertyDescriptor(target, name);
    },
});
function is_HasProps(obj) {
    return isObject(obj) && obj.prototype instanceof HasProps;
}
export function register_models(models, force = false) {
    for (const model of isArray(models) ? models : values(models)) {
        if (is_HasProps(model)) {
            default_resolver.register(model, force);
        }
    }
}
//# sourceMappingURL=base.js.map