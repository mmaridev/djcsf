import { isObject, isNumber } from "./types";
import { BYTE_ORDER } from "./platform";
import { equals } from "./eq";
import { clone } from "./cloneable";
import { serialize } from "../serialization";
const __ndarray__ = Symbol("__ndarray__");
function encode_NDArray(array, serializer) {
    const encoded = serializer.encode(array.dtype == "object" ? Array.from(array) : array.buffer);
    return {
        type: "ndarray",
        array: encoded,
        order: BYTE_ORDER,
        dtype: array.dtype,
        shape: array.shape,
    };
}
export class BoolNDArray extends Uint8Array {
    static __name__ = "BoolNDArray";
    [__ndarray__] = true;
    dtype = "bool";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new BoolNDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i] == 1;
    }
}
export class Uint8NDArray extends Uint8Array {
    static __name__ = "Uint8NDArray";
    [__ndarray__] = true;
    dtype = "uint8";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Uint8NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class Int8NDArray extends Int8Array {
    static __name__ = "Int8NDArray";
    [__ndarray__] = true;
    dtype = "int8";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Int8NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class Uint16NDArray extends Uint16Array {
    static __name__ = "Uint16NDArray";
    [__ndarray__] = true;
    dtype = "uint16";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Uint16NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class Int16NDArray extends Int16Array {
    static __name__ = "Int16NDArray";
    [__ndarray__] = true;
    dtype = "int16";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Int16NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class Uint32NDArray extends Uint32Array {
    static __name__ = "Uint32NDArray";
    [__ndarray__] = true;
    dtype = "uint32";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Uint32NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class Int32NDArray extends Int32Array {
    static __name__ = "Int32NDArray";
    [__ndarray__] = true;
    dtype = "int32";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Int32NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class Float32NDArray extends Float32Array {
    static __name__ = "Float32NDArray";
    [__ndarray__] = true;
    dtype = "float32";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Float32NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class Float64NDArray extends Float64Array {
    static __name__ = "Float64NDArray";
    [__ndarray__] = true;
    dtype = "float64";
    shape;
    dimension;
    constructor(init, shape) {
        super(init); // XXX: typescript bug?
        this.shape = shape ?? (is_NDArray(init) ? init.shape : [this.length]);
        this.dimension = this.shape.length;
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new Float64NDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export class ObjectNDArray extends Array {
    static __name__ = "ObjectNDArray";
    [__ndarray__] = true;
    dtype = "object";
    _shape;
    get shape() {
        return this._shape ?? [this.length];
    }
    get dimension() {
        return this.shape.length;
    }
    constructor(init_, shape) {
        const init = init_ instanceof ArrayBuffer ? new Float64Array(init_) : init_;
        const size = isNumber(init) ? init : init.length;
        super(size);
        if (!isNumber(init)) {
            for (let i = 0; i < init.length; i++) {
                this[i] = init[i];
            }
        }
        this._shape = shape ?? (is_NDArray(init) ? init.shape : undefined);
    }
    [equals](that, cmp) {
        return cmp.eq(this.shape, that.shape) && cmp.arrays(this, that);
    }
    [clone](cloner) {
        return new ObjectNDArray(this, cloner.clone(this.shape));
    }
    [serialize](serializer) {
        return encode_NDArray(this, serializer);
    }
    get(i) {
        return this[i];
    }
}
export function is_NDArray(v) {
    return isObject(v) && __ndarray__ in v;
}
export function ndarray(init, { dtype, shape } = {}) {
    if (dtype == null) {
        dtype = (() => {
            switch (true) {
                case init instanceof Uint8Array: return "uint8";
                case init instanceof Int8Array: return "int8";
                case init instanceof Uint16Array: return "uint16";
                case init instanceof Int16Array: return "int16";
                case init instanceof Uint32Array: return "uint32";
                case init instanceof Int32Array: return "int32";
                case init instanceof Float32Array: return "float32";
                case init instanceof ArrayBuffer:
                case init instanceof Float64Array: return "float64";
                default: return "object";
            }
        })();
    }
    switch (dtype) {
        case "bool": return new BoolNDArray(init, shape);
        case "uint8": return new Uint8NDArray(init, shape);
        case "int8": return new Int8NDArray(init, shape);
        case "uint16": return new Uint16NDArray(init, shape);
        case "int16": return new Int16NDArray(init, shape);
        case "uint32": return new Uint32NDArray(init, shape);
        case "int32": return new Int32NDArray(init, shape);
        case "float32": return new Float32NDArray(init, shape);
        case "float64": return new Float64NDArray(init, shape);
        case "object": return new ObjectNDArray(init, shape);
    }
}
//# sourceMappingURL=ndarray.js.map