export class AssertionError extends Error {
    static __name__ = "AssertionError";
}
export class UnreachableError extends Error {
    static __name__ = "UnreachableError";
}
export function assert(condition, message) {
    if (condition === true || (condition !== false && condition())) {
        return;
    }
    throw new AssertionError(message ?? "Assertion failed");
}
export function assert_debug(condition, message) {
    if (typeof DEBUG !== "undefined" && DEBUG) {
        assert(condition, message);
    }
}
export function unreachable(msg) {
    const suffix = msg != null ? `: ${msg}` : "";
    throw new UnreachableError(`unreachable code${suffix}`);
}
//# sourceMappingURL=assert.js.map