import { minmax } from "./math";
export function scale_interval(range, factor, center) {
    const [min, max] = minmax(range.start, range.end);
    const x = center ?? (max + min) / 2.0;
    const x0 = min - (min - x) * factor;
    const x1 = max - (max - x) * factor;
    return [x0, x1];
}
export function get_info(scales, [sxy0, sxy1]) {
    const info = new Map();
    for (const scale of scales) {
        const [start, end] = scale.r_invert(sxy0, sxy1);
        info.set(scale.source_range, { start, end });
    }
    return info;
}
export function rescale(scales, factor, center) {
    const output = new Map();
    for (const scale of scales) {
        const [v0, v1] = scale_interval(scale.target_range, factor, center);
        const [start, end] = scale.r_invert(v0, v1);
        output.set(scale.source_range, { start, end });
    }
    return output;
}
export function scale_range(x_scales, y_scales, _x_target, _y_range, factor, x_axis = true, y_axis = true, center) {
    /*
     * Utility function for zoom tools to calculate/create the zoom_info object
     * of the form required by `PlotView.update_range`.
     */
    const x_factor = x_axis ? factor : 0;
    //const [sx0, sx1] = scale_interval(x_target, x_factor, center?.x)
    //const xrs = get_info(x_scales, [sx0, sx1])
    const xrs = rescale(x_scales, x_factor, center?.x);
    const y_factor = y_axis ? factor : 0;
    //const [sy0, sy1] = scale_interval(y_range, y_factor, center?.y)
    //const yrs = get_info(y_scales, [sy0, sy1])
    const yrs = rescale(y_scales, y_factor, center?.y);
    // OK this sucks we can't set factor independently in each direction. It is used
    // for GMap plots, and GMap plots always preserve aspect, so effective the value
    // of 'dimensions' is ignored.
    return { xrs, yrs, factor };
}
//# sourceMappingURL=zoom.js.map