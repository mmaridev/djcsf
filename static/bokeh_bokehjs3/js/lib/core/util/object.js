import { isPlainObject } from "./types";
import { union } from "./array";
export const { assign } = Object;
export const extend = assign;
export function to_object(obj) {
    return isPlainObject(obj) ? obj : Object.fromEntries(obj);
}
export function keys(obj) {
    return obj instanceof Map ? [...obj.keys()] : Object.keys(obj);
}
export function values(obj) {
    return obj instanceof Map ? [...obj.values()] : Object.values(obj);
}
export function entries(obj) {
    return obj instanceof Map ? [...obj.entries()] : Object.entries(obj);
}
export const typed_keys = Object.keys;
export const typed_values = Object.values;
export const typed_entries = Object.entries;
export function clone(obj) {
    return obj instanceof Map ? new Map(obj) : { ...obj };
}
export function merge(obj0, obj1) {
    /*
    * Returns an object with the array values for obj1 and obj2 unioned by key.
    */
    const result = new Map();
    const keys = [...obj0.keys(), ...obj1.keys()];
    for (const key of keys) {
        const v0 = obj0.get(key);
        const v1 = obj1.get(key);
        const arr0 = v0 === undefined ? [] : v0;
        const arr1 = v1 === undefined ? [] : v1;
        result.set(key, union(arr0, arr1));
    }
    return result;
}
export function size(obj) {
    return obj instanceof Map ? obj.size : Object.keys(obj).length;
}
export function is_empty(obj) {
    return size(obj) == 0;
}
const { hasOwnProperty } = Object.prototype;
export class PlainObjectProxy {
    obj;
    static __name__ = "PlainObjectProxy";
    constructor(obj) {
        this.obj = obj;
    }
    [Symbol.toStringTag] = "PlainObjectProxy";
    clear() {
        for (const key of this.keys()) {
            delete this.obj[key];
        }
    }
    delete(key) {
        const exists = this.has(key);
        if (exists) {
            delete this.obj[key];
        }
        return exists;
    }
    has(key) {
        return hasOwnProperty.call(this.obj, key);
    }
    get(key) {
        return this.has(key) ? this.obj[key] : undefined;
    }
    set(key, value) {
        this.obj[key] = value;
        return this;
    }
    get size() {
        return size(this.obj);
    }
    [Symbol.iterator]() {
        return this.entries();
    }
    *keys() {
        yield* keys(this.obj);
    }
    *values() {
        yield* values(this.obj);
    }
    *entries() {
        yield* entries(this.obj);
    }
    forEach(callback, that) {
        for (const [key, value] of this.entries()) {
            callback.call(that, value, key, this);
        }
    }
}
export function dict(obj) {
    return isPlainObject(obj) ? new PlainObjectProxy(obj) : obj;
}
//# sourceMappingURL=object.js.map