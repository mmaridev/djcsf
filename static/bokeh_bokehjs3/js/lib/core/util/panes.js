import { div, empty, InlineStyleSheet, ClassList } from "../dom";
import { isString } from "./types";
import panes_css /*, * as panes*/ from "../../styles/panes.css";
import base_css from "../../styles/base.css";
//import {DOMComponentView} from "../dom_view"
export class DropPane {
    contents;
    static __name__ = "DropPane";
    el = div();
    shadow_el;
    _open = false;
    get is_open() {
        return this._open;
    }
    target;
    orientation;
    reversed;
    prevent_hide;
    extra_stylesheets;
    class_list;
    constructor(contents, options) {
        this.contents = contents;
        this.target = options.target;
        this.prevent_hide = options.prevent_hide;
        this.extra_stylesheets = options.extra_stylesheets ?? [];
        this.shadow_el = this.el.attachShadow({ mode: "open" });
        this.class_list = new ClassList(this.el.classList);
    }
    _on_mousedown = (event) => {
        if (event.composedPath().includes(this.el)) {
            return;
        }
        const { prevent_hide } = this;
        if (prevent_hide instanceof HTMLElement) {
            if (event.composedPath().includes(prevent_hide)) {
                return;
            }
        }
        else if (prevent_hide != null) {
            if (prevent_hide(event)) {
                return;
            }
        }
        this.hide();
    };
    _on_keydown = (event) => {
        switch (event.key) {
            case "Escape": {
                this.hide();
                break;
            }
            default:
        }
    };
    _on_blur = () => {
        this.hide();
    };
    remove() {
        this._unlisten();
        this.el.remove();
    }
    _listen() {
        document.addEventListener("mousedown", this._on_mousedown);
        document.addEventListener("keydown", this._on_keydown);
        window.addEventListener("blur", this._on_blur);
    }
    _unlisten() {
        document.removeEventListener("mousedown", this._on_mousedown);
        document.removeEventListener("keydown", this._on_keydown);
        window.removeEventListener("blur", this._on_blur);
    }
    stylesheets() {
        return [base_css, /*...super.stylesheets(), */ panes_css, ...this.extra_stylesheets];
    }
    empty() {
        empty(this.shadow_el);
        this.class_list.clear();
    }
    render() {
        this.empty();
        for (const style of this.stylesheets()) {
            const stylesheet = isString(style) ? new InlineStyleSheet(style) : style;
            stylesheet.install(this.shadow_el);
        }
        this.shadow_el.append(...this.contents);
    }
    show() {
        if (!this._open) {
            this.render();
            const actual_target = this.target.shadowRoot ?? this.target;
            actual_target.appendChild(this.el);
            this._listen();
            this._open = true;
        }
    }
    hide() {
        if (this._open) {
            this._open = false;
            this._unlisten();
            this.el.remove();
        }
    }
    toggle() {
        this._open ? this.hide() : this.show();
    }
}
//# sourceMappingURL=panes.js.map