//     Underscore.js 1.8.3
//     http://underscorejs.org
//     (c) 2009-2015 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
import { randomIn } from "./math";
import { assert } from "./assert";
import { isInteger } from "./types";
import { min, min_by, max_by, includes, filter } from "./arrayable";
export { map, reduce, min, min_by, max, max_by, sum, cumsum, every, some, find, find_last, find_index, find_last_index, sorted_index, is_empty, includes, contains, sort_by, } from "./arrayable";
const { slice } = Array.prototype;
export function head(array) {
    if (array.length != 0) {
        return array[0];
    }
    else {
        throw new Error("out of bounds access");
    }
}
export function last(array) {
    if (array.length != 0) {
        return array[array.length - 1];
    }
    else {
        throw new Error("out of bounds access");
    }
}
export function copy(array) {
    return slice.call(array);
}
export function concat(arrays) {
    return [].concat(...arrays);
}
export function nth(array, index) {
    return array[index >= 0 ? index : array.length + index];
}
export function zip(...arrays) {
    if (arrays.length == 0) {
        return [];
    }
    const n = min(arrays.map((a) => a.length));
    const k = arrays.length;
    const result = new Array(n);
    for (let i = 0; i < n; i++) {
        result[i] = new Array(k);
        for (let j = 0; j < k; j++) {
            result[i][j] = arrays[j][i];
        }
    }
    return result;
}
export function unzip(array) {
    const n = array.length;
    if (n == 0) {
        return [];
    }
    const k = min(array.map((a) => a.length));
    const results = Array(k);
    for (let j = 0; j < k; j++) {
        results[j] = new Array(n);
    }
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < k; j++) {
            results[j][i] = array[i][j];
        }
    }
    return results;
}
export function range(start, stop, step = 1) {
    assert(step > 0, "'step' must be a positive number");
    if (stop == null) {
        stop = start;
        start = 0;
    }
    const { max, ceil, abs } = Math;
    const delta = start <= stop ? step : -step;
    const length = max(ceil(abs(stop - start) / step), 0);
    const range = new Array(length);
    for (let i = 0; i < length; i++, start += delta) {
        range[i] = start;
    }
    return range;
}
export function linspace(start, stop, num = 100) {
    const step = num == 1 ? 0 : (stop - start) / (num - 1);
    const array = new Array(num);
    for (let i = 0; i < num; i++) {
        array[i] = start + step * i;
    }
    return array;
}
export function transpose(array) {
    const rows = array.length;
    const cols = array[0].length;
    const transposed = [];
    for (let j = 0; j < cols; j++) {
        transposed[j] = [];
        for (let i = 0; i < rows; i++) {
            transposed[j][i] = array[i][j];
        }
    }
    return transposed;
}
export function argmin(array) {
    return min_by(range(array.length), (i) => array[i]);
}
export function argmax(array) {
    return max_by(range(array.length), (i) => array[i]);
}
export function uniq(array) {
    const result = new Set();
    for (const value of array) {
        result.add(value);
    }
    return [...result];
}
export function uniq_by(array, key) {
    const result = [];
    const seen = [];
    for (const value of array) {
        const computed = key(value);
        if (!includes(seen, computed)) {
            seen.push(computed);
            result.push(value);
        }
    }
    return result;
}
export function _union(arrays) {
    const result = new Set();
    for (const array of arrays) {
        for (const value of array) {
            result.add(value);
        }
    }
    return result;
}
export function union(...arrays) {
    return [..._union(arrays)];
}
export function intersection(array, ...arrays) {
    const result = [];
    top: for (const item of array) {
        if (includes(result, item)) {
            continue;
        }
        for (const other of arrays) {
            if (!includes(other, item)) {
                continue top;
            }
        }
        result.push(item);
    }
    return result;
}
export function difference(array, ...arrays) {
    const rest = _union(arrays);
    return filter(array, (value) => !rest.has(value));
}
export function symmetric_difference(array0, array1) {
    const set0 = new Set(array0);
    const set1 = new Set(array1);
    const result = [];
    for (const val of set0) {
        if (!set1.has(val)) {
            result.push(val);
        }
    }
    for (const val of set1) {
        if (!set0.has(val)) {
            result.push(val);
        }
    }
    return result;
}
export function remove_at(array, i) {
    assert(isInteger(i) && i >= 0);
    const result = copy(array);
    result.splice(i, 1);
    return result;
}
export function remove(array, item) {
    remove_by(array, (value) => value == item);
}
export function remove_by(array, key) {
    for (let i = 0; i < array.length;) {
        if (key(array[i])) {
            array.splice(i, 1);
        }
        else {
            i++;
        }
    }
}
export function clear(array) {
    array.splice(0, array.length);
}
export function split(array, separator) {
    const chunks = [];
    const n = array.length;
    let i = 0;
    let j = 0;
    while (j < n) {
        if (array[j] === separator) {
            chunks.push(array.slice(i, j));
            i = ++j;
        }
        else {
            ++j;
        }
    }
    chunks.push(array.slice(i));
    return chunks;
}
// Shuffle a collection, using the modern version of the
// [Fisher-Yates shuffle](http://en.wikipedia.org/wiki/Fisher–Yates_shuffle).
export function shuffle(array) {
    const length = array.length;
    const shuffled = new Array(length);
    for (let i = 0; i < length; i++) {
        const rand = randomIn(0, i);
        if (rand !== i) {
            shuffled[i] = shuffled[rand];
        }
        shuffled[rand] = array[i];
    }
    return shuffled;
}
export function pairwise(array, fn) {
    const n = array.length;
    const result = new Array(n - 1);
    for (let i = 0; i < n - 1; i++) {
        result[i] = fn(array[i], array[i + 1]);
    }
    return result;
}
export function elementwise(array0, array1, fn) {
    const n = Math.min(array0.length, array1.length);
    const result = Array(n);
    for (let i = 0; i < n; i++) {
        result[i] = fn(array0[i], array1[i]);
    }
    return result;
}
export function reversed(array) {
    const n = array.length;
    const result = new Array(n);
    for (let i = 0; i < n; i++) {
        result[n - i - 1] = array[i];
    }
    return result;
}
export function repeat(value, n) {
    const result = new Array(n).fill(value);
    return result;
}
export function resize(array, new_length, fill_value) {
    if (array.length >= new_length) {
        return array.slice(0, new_length);
    }
    else {
        const suffix = new Array(new_length - array.length);
        if (fill_value !== undefined) {
            suffix.fill(fill_value);
        }
        return array.concat(suffix);
    }
}
//# sourceMappingURL=array.js.map