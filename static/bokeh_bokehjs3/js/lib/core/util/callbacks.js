import { isFunction } from "./types";
export function execute(cb, obj, ...args) {
    if (isFunction(cb)) {
        return cb(obj, ...args);
    }
    else {
        return cb.execute(obj, ...args);
    }
}
//# sourceMappingURL=callbacks.js.map