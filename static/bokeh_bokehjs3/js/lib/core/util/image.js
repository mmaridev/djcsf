import { logger } from "../logging";
export async function load_image(url, options) {
    return new ImageLoader(url, options).promise;
}
export class ImageLoader {
    static __name__ = "ImageLoader";
    image = new Image();
    promise;
    constructor(src, config = {}) {
        const { attempts = 1, timeout = 1 } = config;
        const url = (() => {
            if (src instanceof ArrayBuffer) {
                const blob = new Blob([src], { type: "image/png" }); // TODO mime
                return URL.createObjectURL(blob); // TODO revoke
            }
            else {
                return src;
            }
        })();
        this.promise = new Promise((resolve, _reject) => {
            this.image.crossOrigin = "anonymous";
            let retries = 0;
            this.image.onerror = () => {
                if (++retries == attempts) {
                    const message = `unable to load ${url} image after ${attempts} attempts`;
                    logger.warn(message);
                    if (this.image.crossOrigin != null) {
                        logger.warn(`attempting to load ${url} without a cross origin policy`);
                        this.image.crossOrigin = null;
                        retries = 0;
                    }
                    else {
                        config.failed?.();
                        return; // XXX reject(new Error(message))
                    }
                }
                setTimeout(() => this.image.src = url, timeout);
            };
            this.image.onload = () => {
                this._finished = true;
                config.loaded?.(this.image);
                resolve(this.image);
            };
            this.image.src = url;
        });
    }
    _finished = false;
    get finished() {
        return this._finished;
    }
}
//# sourceMappingURL=image.js.map