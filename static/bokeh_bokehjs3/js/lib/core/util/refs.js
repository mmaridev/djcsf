import { isPlainObject, isObject } from "./types";
export function is_ref(obj) {
    return isPlainObject(obj) && "id" in obj && !("type" in obj);
}
export const has_refs = Symbol("has_refs");
function _is_HasRefs(v) {
    return has_refs in v;
}
export function is_HasRefs(v) {
    return isObject(v) && _is_HasRefs(v);
}
export function may_have_refs(obj) {
    if (_is_HasRefs(obj)) {
        return obj[has_refs];
    }
    const type = obj.constructor;
    if (is_HasRefs(type)) {
        return type[has_refs];
    }
    return true;
}
//# sourceMappingURL=refs.js.map