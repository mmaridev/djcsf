import { VisualProperties, VisualUniforms } from "./visual";
import * as mixins from "../property_mixins";
import { LineJoin, LineCap, LineDash } from "../enums";
import { color2css } from "../util/color";
import { isArray, isInteger } from "../util/types";
export function resolve_line_dash(line_dash) {
    if (isArray(line_dash)) {
        return line_dash;
    }
    else {
        switch (line_dash) {
            case "solid": return [];
            case "dashed": return [6];
            case "dotted": return [2, 4];
            case "dotdash": return [2, 4, 6, 4];
            case "dashdot": return [6, 4, 2, 4];
            default:
                return line_dash.split(" ").map(Number).filter(isInteger);
        }
    }
}
export class Line extends VisualProperties {
    static __name__ = "Line";
    get doit() {
        const color = this.get_line_color();
        const alpha = this.get_line_alpha();
        const width = this.get_line_width();
        return !(color == null || alpha == 0 || width == 0);
    }
    apply(ctx) {
        const { doit } = this;
        if (doit) {
            this.set_value(ctx);
            ctx.stroke();
        }
        return doit;
    }
    values() {
        return {
            color: this.get_line_color(),
            alpha: this.get_line_alpha(),
            width: this.get_line_width(),
            join: this.get_line_join(),
            cap: this.get_line_cap(),
            dash: this.get_line_dash(),
            offset: this.get_line_dash_offset(),
        };
    }
    set_value(ctx) {
        const color = this.get_line_color();
        const alpha = this.get_line_alpha();
        ctx.strokeStyle = color2css(color, alpha);
        ctx.lineWidth = this.get_line_width();
        ctx.lineJoin = this.get_line_join();
        ctx.lineCap = this.get_line_cap();
        ctx.setLineDash(resolve_line_dash(this.get_line_dash()));
        ctx.lineDashOffset = this.get_line_dash_offset();
    }
    get_line_color() {
        const css_color = this._get_css_value("line-color");
        if (css_color != "") {
            return css_color;
        }
        return this.line_color.get_value();
    }
    get_line_alpha() {
        const css_alpha = this._get_css_value("line-alpha");
        if (css_alpha != "") {
            const alpha = Number(css_alpha);
            if (isFinite(alpha)) {
                return alpha;
            }
        }
        return this.line_alpha.get_value();
    }
    get_line_width() {
        const css_width = this._get_css_value("line-width");
        if (css_width != "") {
            const width = Number(css_width);
            if (isFinite(width)) {
                return width;
            }
        }
        return this.line_width.get_value();
    }
    get_line_join() {
        const css_join = this._get_css_value("line-join");
        if (LineJoin.valid(css_join)) {
            return css_join;
        }
        return this.line_join.get_value();
    }
    get_line_cap() {
        const css_cap = this._get_css_value("line-cap");
        if (LineCap.valid(css_cap)) {
            return css_cap;
        }
        return this.line_cap.get_value();
    }
    get_line_dash() {
        const css_dash = this._get_css_value("line-dash");
        if (LineDash.valid(css_dash)) {
            return css_dash;
        }
        return this.line_dash.get_value();
    }
    get_line_dash_offset() {
        const css_dash_offset = this._get_css_value("line-dash-offset");
        if (css_dash_offset != "") {
            const dash_offset = Number(css_dash_offset);
            if (isFinite(dash_offset)) {
                return dash_offset;
            }
        }
        return this.line_dash_offset.get_value();
    }
}
export class LineScalar extends VisualUniforms {
    static __name__ = "LineScalar";
    get doit() {
        const color = this.line_color.value;
        const alpha = this.line_alpha.value;
        const width = this.line_width.value;
        return !(color == 0 || alpha == 0 || width == 0);
    }
    apply(ctx) {
        const { doit } = this;
        if (doit) {
            this.set_value(ctx);
            ctx.stroke();
        }
        return doit;
    }
    values() {
        return {
            color: this.line_color.value,
            alpha: this.line_alpha.value,
            width: this.line_width.value,
            join: this.line_join.value,
            cap: this.line_cap.value,
            dash: this.line_dash.value,
            offset: this.line_dash_offset.value,
        };
    }
    set_value(ctx) {
        const color = this.line_color.value;
        const alpha = this.line_alpha.value;
        ctx.strokeStyle = color2css(color, alpha);
        ctx.lineWidth = this.line_width.value;
        ctx.lineJoin = this.line_join.value;
        ctx.lineCap = this.line_cap.value;
        ctx.setLineDash(resolve_line_dash(this.line_dash.value));
        ctx.lineDashOffset = this.line_dash_offset.value;
    }
}
export class LineVector extends VisualUniforms {
    static __name__ = "LineVector";
    get doit() {
        const { line_color } = this;
        if (line_color.is_Scalar() && line_color.value == 0) {
            return false;
        }
        const { line_alpha } = this;
        if (line_alpha.is_Scalar() && line_alpha.value == 0) {
            return false;
        }
        const { line_width } = this;
        if (line_width.is_Scalar() && line_width.value == 0) {
            return false;
        }
        return true;
    }
    v_doit(i) {
        if (this.line_color.get(i) == 0) {
            return false;
        }
        if (this.line_alpha.get(i) == 0) {
            return false;
        }
        if (this.line_width.get(i) == 0) {
            return false;
        }
        return true;
    }
    apply(ctx, i) {
        const doit = this.v_doit(i);
        if (doit) {
            this.set_vectorize(ctx, i);
            ctx.stroke();
        }
        return doit;
    }
    values(i) {
        return {
            color: this.line_color.get(i),
            alpha: this.line_alpha.get(i),
            width: this.line_width.get(i),
            join: this.line_join.get(i),
            cap: this.line_cap.get(i),
            dash: this.line_dash.get(i),
            offset: this.line_dash_offset.get(i),
        };
    }
    set_vectorize(ctx, i) {
        const color = this.line_color.get(i);
        const alpha = this.line_alpha.get(i);
        const width = this.line_width.get(i);
        const join = this.line_join.get(i);
        const cap = this.line_cap.get(i);
        const dash = this.line_dash.get(i);
        const offset = this.line_dash_offset.get(i);
        ctx.strokeStyle = color2css(color, alpha);
        ctx.lineWidth = width;
        ctx.lineJoin = join;
        ctx.lineCap = cap;
        ctx.setLineDash(resolve_line_dash(dash));
        ctx.lineDashOffset = offset;
    }
}
Line.prototype.type = "line";
Line.prototype.attrs = Object.keys(mixins.Line);
LineScalar.prototype.type = "line";
LineScalar.prototype.attrs = Object.keys(mixins.LineScalar);
LineVector.prototype.type = "line";
LineVector.prototype.attrs = Object.keys(mixins.LineVector);
//# sourceMappingURL=line.js.map