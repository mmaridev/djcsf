const global_css_prefix = "--bk-";
export class VisualProperties {
    obj;
    prefix;
    static __name__ = "VisualProperties";
    _props;
    *[Symbol.iterator]() {
        yield* this._props;
    }
    css_prefix;
    constructor(obj, prefix = "") {
        this.obj = obj;
        this.prefix = prefix;
        this.css_prefix = `${global_css_prefix}${prefix.replaceAll("_", "-")}`;
        const self = this;
        this._props = [];
        for (const attr of this.attrs) {
            const prop = obj.model.properties[prefix + attr];
            prop.change.connect(() => this.update());
            self[attr] = prop;
            this._props.push(prop);
        }
    }
    update() { }
    _get_css_value(name) {
        const style = getComputedStyle(this.obj.el);
        return style.getPropertyValue(`${this.css_prefix}${name}`);
    }
}
export class VisualUniforms {
    obj;
    prefix;
    static __name__ = "VisualUniforms";
    *[Symbol.iterator]() {
        for (const attr of this.attrs) {
            yield this.obj.model.properties[this.prefix + attr];
        }
    }
    constructor(obj, prefix = "") {
        this.obj = obj;
        this.prefix = prefix;
        for (const attr of this.attrs) {
            Object.defineProperty(this, attr, {
                get() {
                    return obj[prefix + attr];
                },
            });
        }
    }
    update() { }
}
//# sourceMappingURL=visual.js.map