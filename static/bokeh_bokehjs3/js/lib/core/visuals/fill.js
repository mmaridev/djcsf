import { VisualProperties, VisualUniforms } from "./visual";
import * as mixins from "../property_mixins";
import { color2css } from "../util/color";
export class Fill extends VisualProperties {
    static __name__ = "Fill";
    get doit() {
        const color = this.get_fill_color();
        const alpha = this.get_fill_alpha();
        return !(color == null || alpha == 0);
    }
    apply(ctx, rule = "nonzero") {
        const { doit } = this;
        if (doit) {
            this.set_value(ctx);
            ctx.fill(rule);
        }
        return doit;
    }
    Values;
    values() {
        return {
            color: this.get_fill_color(),
            alpha: this.get_fill_alpha(),
        };
    }
    set_value(ctx) {
        const color = this.get_fill_color();
        const alpha = this.get_fill_alpha();
        ctx.fillStyle = color2css(color, alpha);
    }
    get_fill_color() {
        const css_color = this._get_css_value("fill-color");
        if (css_color != "") {
            return css_color;
        }
        return this.fill_color.get_value();
    }
    get_fill_alpha() {
        const css_alpha = this._get_css_value("fill-alpha");
        if (css_alpha != "") {
            const alpha = Number(css_alpha);
            if (isFinite(alpha)) {
                return alpha;
            }
        }
        return this.fill_alpha.get_value();
    }
}
export class FillScalar extends VisualUniforms {
    static __name__ = "FillScalar";
    get doit() {
        const color = this.fill_color.value;
        const alpha = this.fill_alpha.value;
        return !(color == 0 || alpha == 0);
    }
    apply(ctx, rule = "nonzero") {
        const { doit } = this;
        if (doit) {
            this.set_value(ctx);
            ctx.fill(rule);
        }
        return doit;
    }
    Values;
    values() {
        return {
            color: this.fill_color.value,
            alpha: this.fill_alpha.value,
        };
    }
    set_value(ctx) {
        const color = this.fill_color.value;
        const alpha = this.fill_alpha.value;
        ctx.fillStyle = color2css(color, alpha);
    }
}
export class FillVector extends VisualUniforms {
    static __name__ = "FillVector";
    get doit() {
        const { fill_color } = this;
        if (fill_color.is_Scalar() && fill_color.value == 0) {
            return false;
        }
        const { fill_alpha } = this;
        if (fill_alpha.is_Scalar() && fill_alpha.value == 0) {
            return false;
        }
        return true;
    }
    v_doit(i) {
        if (this.fill_color.get(i) == 0) {
            return false;
        }
        if (this.fill_alpha.get(i) == 0) {
            return false;
        }
        return true;
    }
    apply(ctx, i, rule = "nonzero") {
        const doit = this.v_doit(i);
        if (doit) {
            this.set_vectorize(ctx, i);
            ctx.fill(rule);
        }
        return doit;
    }
    Values;
    values(i) {
        return {
            color: this.fill_color.get(i),
            alpha: this.fill_alpha.get(i),
        };
    }
    set_vectorize(ctx, i) {
        const color = this.fill_color.get(i);
        const alpha = this.fill_alpha.get(i);
        ctx.fillStyle = color2css(color, alpha);
    }
}
Fill.prototype.type = "fill";
Fill.prototype.attrs = Object.keys(mixins.Fill);
FillScalar.prototype.type = "fill";
FillScalar.prototype.attrs = Object.keys(mixins.FillScalar);
FillVector.prototype.type = "fill";
FillVector.prototype.attrs = Object.keys(mixins.FillVector);
//# sourceMappingURL=fill.js.map