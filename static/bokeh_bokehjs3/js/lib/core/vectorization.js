import { isPlainObject } from "./util/types";
import { size } from "./util/object";
function is_of_type(obj, field) {
    if (!isPlainObject(obj)) {
        return false;
    }
    if (!(field in obj)) {
        return false;
    }
    let n = size(obj) - 1;
    if ("transform" in obj) {
        n -= 1;
    }
    if ("units" in obj) {
        n -= 1;
    }
    return n == 0;
}
export function isValue(obj) {
    return is_of_type(obj, "value");
}
export function isField(obj) {
    return is_of_type(obj, "field");
}
export function isExpr(obj) {
    return is_of_type(obj, "expr");
}
export function isVectorized(obj) {
    return isValue(obj) || isField(obj) || isExpr(obj);
}
//# sourceMappingURL=vectorization.js.map