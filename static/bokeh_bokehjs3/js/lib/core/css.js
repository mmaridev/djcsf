import { logger } from "./logging";
import { entries } from "./util/object";
import { isString, isPlainObject } from "./util/types";
const _style_decl = document.createElement("div").style;
function _css_name(attr) {
    if (attr.startsWith("--")) {
        return attr;
    }
    const name = attr.replaceAll(/_/g, "-").replaceAll(/[A-Z]/g, (c) => `-${c.toLowerCase()}`);
    // XXX hasOwnProperty() doesn't work for unknown reasons (e.g. in Firefox)
    if (name in _style_decl) {
        return name;
    }
    const webkit_name = `-webkit-${name}`;
    if (webkit_name in _style_decl) {
        return webkit_name;
    }
    const moz_name = `-moz-${name}`;
    if (moz_name in _style_decl) {
        return moz_name;
    }
    logger.warn(`unknown CSS property '${attr}'`);
    return null;
}
function* _iter_styles(styles) {
    if (isPlainObject(styles) || styles instanceof Map) {
        for (const [key, val] of entries(styles)) {
            const name = _css_name(key);
            if (name != null) {
                yield [name, val];
            }
        }
    }
    else {
        for (const prop of styles.own_properties()) {
            if (prop.dirty) {
                const name = _css_name(prop.attr);
                if (name != null) {
                    yield [name, prop.get_value()];
                }
            }
        }
    }
}
export function apply_styles(declaration, styles) {
    for (const [name, value] of _iter_styles(styles)) {
        if (isString(value)) {
            declaration.setProperty(name, value);
        }
        else {
            declaration.removeProperty(name);
        }
    }
}
export function compose_stylesheet(stylesheet) {
    const css = [];
    for (const [selector, styles] of entries(stylesheet)) {
        css.push(`${selector} {`);
        for (const [name, value] of _iter_styles(styles)) {
            if (isString(value) && value.length != 0) {
                css.push(`  ${name}: ${value};`);
            }
        }
        css.push("}");
    }
    return css.join("\n");
}
//# sourceMappingURL=css.js.map