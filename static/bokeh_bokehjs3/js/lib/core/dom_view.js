import { View } from "./view";
import { create_element, empty, InlineStyleSheet, ClassList } from "./dom";
import { isString } from "./util/types";
import { assert } from "./util/assert";
import base_css from "../styles/base.css";
export class DOMView extends View {
    static __name__ = "DOMView";
    static tag_name = "div";
    el;
    shadow_el;
    get bbox() {
        return undefined;
    }
    serializable_state() {
        const state = super.serializable_state();
        const { bbox } = this;
        return bbox != null ? { ...state, bbox: bbox.round() } : state;
    }
    get children_el() {
        return this.shadow_el ?? this.el;
    }
    initialize() {
        super.initialize();
        this.el = this._create_element();
    }
    remove() {
        this.el.remove();
        super.remove();
    }
    stylesheets() {
        return [];
    }
    css_classes() {
        return [];
    }
    render_to(target) {
        this.render();
        target.appendChild(this.el);
    }
    after_render() {
        this.reposition();
    }
    r_after_render() {
        for (const child_view of this.children()) {
            if (child_view instanceof DOMView) {
                child_view.r_after_render();
            }
        }
        this.after_render();
        this._was_built = true;
    }
    _create_element() {
        return create_element(this.constructor.tag_name, {});
    }
    reposition(_displayed) { }
    _was_built = false;
    /**
     * Build a top-level DOM view (e.g. during embedding).
     */
    build(target) {
        assert(this.is_root);
        this.render_to(target);
        this.r_after_render();
        this.notify_finished();
    }
    /**
     * Define where to render this element or let the parent decide.
     *
     * This is useful when creating "floating" components or adding
     * components to canvas' layers.
     */
    rendering_target() {
        return null;
    }
}
export class DOMElementView extends DOMView {
    static __name__ = "DOMElementView";
    class_list;
    initialize() {
        super.initialize();
        this.class_list = new ClassList(this.el.classList);
    }
}
export class DOMComponentView extends DOMElementView {
    static __name__ = "DOMComponentView";
    initialize() {
        super.initialize();
        this.shadow_el = this.el.attachShadow({ mode: "open" });
    }
    stylesheets() {
        return [...super.stylesheets(), base_css];
    }
    empty() {
        empty(this.shadow_el);
        this.class_list.clear();
        this._applied_css_classes = [];
        this._applied_stylesheets = [];
    }
    render() {
        this.empty();
        this._update_stylesheets();
        this._update_css_classes();
        this._update_css_variables();
    }
    reposition(_displayed) {
        this._update_css_variables(); // TODO remove this when node invalidation is implemented
    }
    *_stylesheets() {
        for (const style of this.stylesheets()) {
            yield isString(style) ? new InlineStyleSheet(style) : style;
        }
    }
    *_css_classes() {
        yield `bk-${this.model.type.replace(/\./g, "-")}`;
        yield* this.css_classes();
    }
    *_css_variables() { }
    _applied_stylesheets = [];
    _apply_stylesheets(stylesheets) {
        this._applied_stylesheets.push(...stylesheets);
        stylesheets.forEach((stylesheet) => stylesheet.install(this.shadow_el));
    }
    _applied_css_classes = [];
    _apply_css_classes(classes) {
        this._applied_css_classes.push(...classes);
        this.class_list.add(...classes);
    }
    _update_stylesheets() {
        this._applied_stylesheets.forEach((stylesheet) => stylesheet.uninstall());
        this._applied_stylesheets = [];
        this._apply_stylesheets([...this._stylesheets()]);
    }
    _update_css_classes() {
        this.class_list.remove(this._applied_css_classes);
        this._applied_css_classes = [];
        this._apply_css_classes([...this._css_classes()]);
    }
    _update_css_variables() {
        for (const [name, value] of this._css_variables()) {
            const full_name = name.startsWith("--") ? name : `--${name}`;
            this.el.style.setProperty(full_name, value);
        }
    }
}
//# sourceMappingURL=dom_view.js.map