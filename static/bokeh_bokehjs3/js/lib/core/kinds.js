import * as tp from "./util/types";
import { is_Color } from "./util/color";
import { keys, values, typed_values, typed_entries, is_empty, PlainObjectProxy } from "./util/object";
import { has_refs } from "./util/refs";
const ESMap = globalThis.Map;
const ESSet = globalThis.Set;
const DOMNode = globalThis.Node;
export class Kind {
    static __name__ = "Kind";
    __type__;
}
export var Kinds;
(function (Kinds) {
    class Primitive extends Kind {
        static __name__ = "Primitive";
        may_have_refs() {
            return false;
        }
    }
    Kinds.Primitive = Primitive;
    class Any extends Primitive {
        static __name__ = "Any";
        valid(value) {
            return value !== undefined;
        }
        toString() {
            return "Any";
        }
        may_have_refs() {
            return true;
        }
    }
    Kinds.Any = Any;
    class Unknown extends Primitive {
        static __name__ = "Unknown";
        valid(value) {
            return value !== undefined;
        }
        toString() {
            return "Unknown";
        }
        may_have_refs() {
            return true;
        }
    }
    Kinds.Unknown = Unknown;
    class Bool extends Primitive {
        static __name__ = "Bool";
        valid(value) {
            return tp.isBoolean(value);
        }
        toString() {
            return "Bool";
        }
    }
    Kinds.Bool = Bool;
    class Ref extends Kind {
        obj_type;
        static __name__ = "Ref";
        constructor(obj_type) {
            super();
            this.obj_type = obj_type;
        }
        valid(value) {
            return value instanceof this.obj_type;
        }
        toString() {
            const tp = this.obj_type;
            // NOTE: `__name__` is injected by a compiler transform
            const name = tp.__name__ ?? tp.toString();
            return `Ref(${name})`;
        }
        may_have_refs() {
            const { obj_type } = this;
            return has_refs in obj_type ? obj_type[has_refs] : true;
        }
    }
    Kinds.Ref = Ref;
    class AnyRef extends Kind {
        static __name__ = "AnyRef";
        valid(value) {
            return tp.isObject(value);
        }
        toString() {
            return "AnyRef";
        }
        may_have_refs() {
            return true;
        }
    }
    Kinds.AnyRef = AnyRef;
    class Float extends Primitive {
        static __name__ = "Float";
        valid(value) {
            return tp.isNumber(value);
        }
        toString() {
            return "Float";
        }
    }
    Kinds.Float = Float;
    class Int extends Float {
        static __name__ = "Int";
        valid(value) {
            return super.valid(value) && tp.isInteger(value);
        }
        toString() {
            return "Int";
        }
    }
    Kinds.Int = Int;
    class Percent extends Float {
        static __name__ = "Percent";
        valid(value) {
            return super.valid(value) && 0 <= value && value <= 1;
        }
        toString() {
            return "Percent";
        }
    }
    Kinds.Percent = Percent;
    class Or extends Kind {
        types;
        static __name__ = "Or";
        constructor(types) {
            super();
            this.types = types;
            this.types = types;
        }
        valid(value) {
            return this.types.some((type) => type.valid(value));
        }
        toString() {
            return `Or(${this.types.map((type) => type.toString()).join(", ")})`;
        }
        may_have_refs() {
            return this.types.some((type) => type.may_have_refs());
        }
    }
    Kinds.Or = Or;
    class And extends Kind {
        static __name__ = "And";
        types;
        constructor(type0, type1) {
            super();
            this.types = [type0, type1];
        }
        valid(value) {
            return this.types.some((type) => type.valid(value)); // TODO not sure if this is correct, probably not
        }
        toString() {
            return `And(${this.types.map((type) => type.toString()).join(", ")})`;
        }
        may_have_refs() {
            return this.types.some((type) => type.may_have_refs());
        }
    }
    Kinds.And = And;
    class Tuple extends Kind {
        types;
        static __name__ = "Tuple";
        constructor(types) {
            super();
            this.types = types;
            this.types = types;
        }
        valid(value) {
            if (!tp.isArray(value)) {
                return false;
            }
            for (let i = 0; i < this.types.length; i++) {
                const type = this.types[i];
                const item = value[i];
                if (!type.valid(item)) {
                    return false;
                }
            }
            return true;
        }
        toString() {
            return `Tuple(${this.types.map((type) => type.toString()).join(", ")})`;
        }
        may_have_refs() {
            return this.types.some((type) => type.may_have_refs());
        }
    }
    Kinds.Tuple = Tuple;
    class Struct extends Kind {
        struct_type;
        static __name__ = "Struct";
        constructor(struct_type) {
            super();
            this.struct_type = struct_type;
        }
        valid(value) {
            if (!tp.isPlainObject(value)) {
                return false;
            }
            const struct_type_proxy = new PlainObjectProxy(this.struct_type);
            for (const key of keys(value)) {
                if (!struct_type_proxy.has(key)) {
                    return false;
                }
            }
            for (const [key, item_type] of struct_type_proxy) {
                const item = value[key];
                if (!item_type.valid(item)) {
                    return false;
                }
            }
            return true;
        }
        toString() {
            const items = typed_entries(this.struct_type).map(([key, kind]) => `${key.toString()}: ${kind}`).join(", ");
            return `Struct({${items}})`;
        }
        may_have_refs() {
            return typed_values(this.struct_type).some((kind) => kind.may_have_refs());
        }
    }
    Kinds.Struct = Struct;
    class PartialStruct extends Kind {
        struct_type;
        static __name__ = "PartialStruct";
        constructor(struct_type) {
            super();
            this.struct_type = struct_type;
        }
        valid(value) {
            if (!tp.isPlainObject(value)) {
                return false;
            }
            const value_proxy = new PlainObjectProxy(value);
            const struct_type_proxy = new PlainObjectProxy(this.struct_type);
            for (const key of value_proxy.keys()) {
                if (!struct_type_proxy.has(key)) {
                    return false;
                }
            }
            for (const [key, item_type] of struct_type_proxy) {
                const item = value_proxy.get(key);
                if (item === undefined) {
                    continue;
                }
                if (!item_type.valid(item)) {
                    return false;
                }
            }
            return true;
        }
        toString() {
            const items = typed_entries(this.struct_type).map(([key, kind]) => `${key.toString()}?: ${kind}`).join(", ");
            return `Struct({${items}})`;
        }
        may_have_refs() {
            return typed_values(this.struct_type).some((kind) => kind.may_have_refs());
        }
    }
    Kinds.PartialStruct = PartialStruct;
    class Iterable extends Kind {
        item_type;
        static __name__ = "Iterable";
        constructor(item_type) {
            super();
            this.item_type = item_type;
        }
        valid(value) {
            return tp.isIterable(value);
        }
        toString() {
            return `Iterable(${this.item_type.toString()})`;
        }
        may_have_refs() {
            return this.item_type.may_have_refs();
        }
    }
    Kinds.Iterable = Iterable;
    class Arrayable extends Kind {
        item_type;
        static __name__ = "Arrayable";
        constructor(item_type) {
            super();
            this.item_type = item_type;
        }
        valid(value) {
            return tp.isArray(value) || tp.isTypedArray(value); // TODO: too specific
        }
        toString() {
            return `Arrayable(${this.item_type.toString()})`;
        }
        may_have_refs() {
            return this.item_type.may_have_refs();
        }
    }
    Kinds.Arrayable = Arrayable;
    class List extends Kind {
        item_type;
        static __name__ = "List";
        constructor(item_type) {
            super();
            this.item_type = item_type;
        }
        valid(value) {
            return tp.isArray(value) && value.every((item) => this.item_type.valid(item));
        }
        toString() {
            return `List(${this.item_type.toString()})`;
        }
        may_have_refs() {
            return this.item_type.may_have_refs();
        }
    }
    Kinds.List = List;
    class NonEmptyList extends List {
        static __name__ = "NonEmptyList";
        valid(value) {
            return super.valid(value) && value.length != 0;
        }
        toString() {
            return `NonEmptyList(${this.item_type.toString()})`;
        }
    }
    Kinds.NonEmptyList = NonEmptyList;
    class Null extends Primitive {
        static __name__ = "Null";
        valid(value) {
            return value === null;
        }
        toString() {
            return "Null";
        }
    }
    Kinds.Null = Null;
    class Nullable extends Kind {
        base_type;
        static __name__ = "Nullable";
        constructor(base_type) {
            super();
            this.base_type = base_type;
        }
        valid(value) {
            return value === null || this.base_type.valid(value);
        }
        toString() {
            return `Nullable(${this.base_type.toString()})`;
        }
        may_have_refs() {
            return this.base_type.may_have_refs();
        }
    }
    Kinds.Nullable = Nullable;
    class Opt extends Kind {
        base_type;
        static __name__ = "Opt";
        constructor(base_type) {
            super();
            this.base_type = base_type;
        }
        valid(value) {
            return value === undefined || this.base_type.valid(value);
        }
        toString() {
            return `Opt(${this.base_type.toString()})`;
        }
        may_have_refs() {
            return this.base_type.may_have_refs();
        }
    }
    Kinds.Opt = Opt;
    class Bytes extends Kind {
        static __name__ = "Bytes";
        valid(value) {
            return value instanceof ArrayBuffer;
        }
        toString() {
            return "Bytes";
        }
        may_have_refs() {
            return false;
        }
    }
    Kinds.Bytes = Bytes;
    class Str extends Primitive {
        static __name__ = "Str";
        valid(value) {
            return tp.isString(value);
        }
        toString() {
            return "Str";
        }
    }
    Kinds.Str = Str;
    class Regex extends Str {
        regex;
        static __name__ = "Regex";
        constructor(regex) {
            super();
            this.regex = regex;
        }
        valid(value) {
            return super.valid(value) && this.regex.test(value);
        }
        toString() {
            return `Regex(${this.regex.toString()})`;
        }
    }
    Kinds.Regex = Regex;
    class Enum extends Primitive {
        static __name__ = "Enum";
        values;
        constructor(values) {
            super();
            this.values = new ESSet(values);
        }
        valid(value) {
            return this.values.has(value);
        }
        *[Symbol.iterator]() {
            yield* this.values;
        }
        toString() {
            return `Enum(${[...this.values].map((v) => v.toString()).join(", ")})`;
        }
    }
    Kinds.Enum = Enum;
    class Dict extends Kind {
        item_type;
        static __name__ = "Dict";
        constructor(item_type) {
            super();
            this.item_type = item_type;
        }
        valid(value) {
            if (!(value instanceof ESMap || tp.isPlainObject(value))) {
                return false;
            }
            for (const item of values(value)) {
                if (!this.item_type.valid(item)) {
                    return false;
                }
            }
            return true;
        }
        toString() {
            return `Dict(${this.item_type.toString()})`;
        }
        may_have_refs() {
            return this.item_type.may_have_refs();
        }
    }
    Kinds.Dict = Dict;
    class Mapping extends Kind {
        key_type;
        item_type;
        static __name__ = "Mapping";
        constructor(key_type, item_type) {
            super();
            this.key_type = key_type;
            this.item_type = item_type;
        }
        coerce(value) {
            // HACK accommodate for deserialization of {type: "map"}
            if (tp.isPlainObject(value) && is_empty(value)) {
                return new ESMap();
            }
            else {
                return value;
            }
        }
        valid(value) {
            if (!(value instanceof ESMap)) {
                return false;
            }
            for (const [key, item] of value.entries()) {
                if (!(this.key_type.valid(key) && this.item_type.valid(item))) {
                    return false;
                }
            }
            return true;
        }
        toString() {
            return `Mapping(${this.key_type.toString()}, ${this.item_type.toString()})`;
        }
        may_have_refs() {
            return this.key_type.may_have_refs() || this.item_type.may_have_refs();
        }
    }
    Kinds.Mapping = Mapping;
    class Set extends Kind {
        item_type;
        static __name__ = "Set";
        constructor(item_type) {
            super();
            this.item_type = item_type;
        }
        valid(value) {
            if (!(value instanceof ESSet)) {
                return false;
            }
            for (const item of value) {
                if (!this.item_type.valid(item)) {
                    return false;
                }
            }
            return true;
        }
        toString() {
            return `Set(${this.item_type.toString()})`;
        }
        may_have_refs() {
            return this.item_type.may_have_refs();
        }
    }
    Kinds.Set = Set;
    class Color extends Kind {
        static __name__ = "Color";
        valid(value) {
            return is_Color(value);
        }
        toString() {
            return "Color";
        }
        may_have_refs() {
            return false;
        }
    }
    Kinds.Color = Color;
    class CSSLength extends Str {
        static __name__ = "CSSLength";
        /*
        override valid(value: unknown): value is string {
          return super.valid(value) // TODO: && this._parse(value)
        }
        */
        toString() {
            return "CSSLength";
        }
    }
    Kinds.CSSLength = CSSLength;
    class Func extends Kind {
        static __name__ = "Func";
        valid(value) {
            return tp.isFunction(value);
        }
        toString() {
            return "Func(...)";
        }
        may_have_refs() {
            return false;
        }
    }
    Kinds.Func = Func;
    class NonNegative extends Kind {
        base_type;
        static __name__ = "NonNegative";
        constructor(base_type) {
            super();
            this.base_type = base_type;
        }
        valid(value) {
            return this.base_type.valid(value) && value >= 0;
        }
        toString() {
            return `NonNegative(${this.base_type.toString()})`;
        }
        may_have_refs() {
            return this.base_type.may_have_refs();
        }
    }
    Kinds.NonNegative = NonNegative;
    class Positive extends Kind {
        base_type;
        static __name__ = "Positive";
        constructor(base_type) {
            super();
            this.base_type = base_type;
        }
        valid(value) {
            return this.base_type.valid(value) && value > 0;
        }
        toString() {
            return `Positive(${this.base_type.toString()})`;
        }
        may_have_refs() {
            return this.base_type.may_have_refs();
        }
    }
    Kinds.Positive = Positive;
    class Node extends Kind {
        static __name__ = "Node";
        valid(value) {
            return value instanceof DOMNode;
        }
        toString() {
            return "Node";
        }
        may_have_refs() {
            return false;
        }
    }
    Kinds.Node = Node;
})(Kinds || (Kinds = {}));
export const Any = new Kinds.Any();
export const Unknown = new Kinds.Unknown();
export const Bool = new Kinds.Bool();
export const Float = new Kinds.Float();
export const Int = new Kinds.Int();
export const Bytes = new Kinds.Bytes();
export const Str = new Kinds.Str();
export const Regex = (regex) => new Kinds.Regex(regex);
export const Null = new Kinds.Null();
export const Nullable = (base_type) => new Kinds.Nullable(base_type);
export const Opt = (base_type) => new Kinds.Opt(base_type);
export const Or = (...types) => new Kinds.Or(types);
export const And = (type0, type1) => new Kinds.And(type0, type1);
export const Tuple = (...types) => new Kinds.Tuple(types);
export const Struct = (struct_type) => new Kinds.Struct(struct_type);
export const PartialStruct = (struct_type) => new Kinds.PartialStruct(struct_type);
export const Iterable = (item_type) => new Kinds.Iterable(item_type);
export const Arrayable = (item_type) => new Kinds.Arrayable(item_type);
export const List = (item_type) => new Kinds.List(item_type);
export const NonEmptyList = (item_type) => new Kinds.NonEmptyList(item_type);
export const Dict = (item_type) => new Kinds.Dict(item_type);
export const Mapping = (key_type, item_type) => new Kinds.Mapping(key_type, item_type);
export const Set = (item_type) => new Kinds.Set(item_type);
export const Enum = (...values) => new Kinds.Enum(values);
export const Ref = (obj_type) => new Kinds.Ref(obj_type);
export const AnyRef = () => new Kinds.AnyRef();
export const Func = () => new Kinds.Func();
export const Node = new Kinds.Node();
export const NonNegative = (base_type) => new Kinds.NonNegative(base_type);
export const Positive = (base_type) => new Kinds.Positive(base_type);
export const Percent = new Kinds.Percent();
export const Alpha = Percent;
export const Color = new Kinds.Color();
export const Auto = Enum("auto");
export const CSSLength = new Kinds.CSSLength();
export const FontSize = Str;
export const Font = Str;
export const Angle = Float;
// backwards compatibility aliases (these collide with built-in types)
/** @deprecated */
export const Boolean = Bool;
/** @deprecated */
export const String = Str;
/** @deprecated */
export const Number = Float;
/** @deprecated */
export const Array = List;
/** @deprecated */
export const Map = Mapping;
/** @deprecated */
export const Function = Func;
//# sourceMappingURL=kinds.js.map