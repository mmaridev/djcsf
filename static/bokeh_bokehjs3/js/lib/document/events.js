import { equals } from "../core/util/eq";
import { serialize } from "../core/serialization";
export class DocumentEvent {
    document;
    static __name__ = "DocumentEvent";
    constructor(document) {
        this.document = document;
    }
    get [Symbol.toStringTag]() {
        return this.constructor.__name__;
    }
    [equals](that, cmp) {
        return cmp.eq(this.document, that.document);
    }
    /**
     * Indicates whether this event should be emitted internally within bokehjs,
     * or whether it should also be synchronized with the server, if any session
     * is listening for such events.
     */
    sync = true;
}
export class DocumentEventBatch extends DocumentEvent {
    events;
    static __name__ = "DocumentEventBatch";
    constructor(document, events) {
        super(document);
        this.events = events;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.events, that.events);
    }
}
export class DocumentChangedEvent extends DocumentEvent {
    static __name__ = "DocumentChangedEvent";
}
export class MessageSentEvent extends DocumentChangedEvent {
    msg_type;
    msg_data;
    static __name__ = "MessageSentEvent";
    kind = "MessageSent";
    constructor(document, msg_type, msg_data) {
        super(document);
        this.msg_type = msg_type;
        this.msg_data = msg_data;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.msg_type, that.msg_type) &&
            cmp.eq(this.msg_data, that.msg_data);
    }
    [serialize](serializer) {
        return {
            kind: this.kind,
            msg_type: this.msg_type,
            msg_data: serializer.encode(this.msg_data),
        };
    }
}
export class ModelChangedEvent extends DocumentChangedEvent {
    model;
    attr;
    value;
    static __name__ = "ModelChangedEvent";
    kind = "ModelChanged";
    constructor(document, model, attr, value) {
        super(document);
        this.model = model;
        this.attr = attr;
        this.value = value;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.model, that.model) &&
            cmp.eq(this.attr, that.attr) &&
            cmp.eq(this.value, that.value);
    }
    [serialize](serializer) {
        return {
            kind: this.kind,
            model: this.model.ref(),
            attr: this.attr,
            new: serializer.encode(this.value),
        };
    }
}
export class ColumnDataChangedEvent extends DocumentChangedEvent {
    model;
    attr;
    data;
    cols;
    static __name__ = "ColumnDataChangedEvent";
    kind = "ColumnDataChanged";
    constructor(document, model, attr, data, cols) {
        super(document);
        this.model = model;
        this.attr = attr;
        this.data = data;
        this.cols = cols;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.model, that.model) &&
            cmp.eq(this.attr, that.attr) &&
            cmp.eq(this.data, that.data) &&
            cmp.eq(this.cols, that.cols);
    }
    [serialize](serializer) {
        return {
            kind: this.kind,
            model: this.model.ref(),
            attr: this.attr,
            data: serializer.encode(this.data),
            cols: this.cols,
        };
    }
}
export class ColumnsStreamedEvent extends DocumentChangedEvent {
    model;
    attr;
    data;
    rollover;
    static __name__ = "ColumnsStreamedEvent";
    kind = "ColumnsStreamed";
    constructor(document, model, attr, data, rollover) {
        super(document);
        this.model = model;
        this.attr = attr;
        this.data = data;
        this.rollover = rollover;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.model, that.model) &&
            cmp.eq(this.attr, that.attr) &&
            cmp.eq(this.data, that.data) &&
            cmp.eq(this.rollover, that.rollover);
    }
    [serialize](serializer) {
        return {
            kind: this.kind,
            model: this.model.ref(),
            attr: this.attr,
            data: serializer.encode(this.data),
            rollover: this.rollover,
        };
    }
}
export class ColumnsPatchedEvent extends DocumentChangedEvent {
    model;
    attr;
    patches;
    static __name__ = "ColumnsPatchedEvent";
    kind = "ColumnsPatched";
    constructor(document, model, attr, patches) {
        super(document);
        this.model = model;
        this.attr = attr;
        this.patches = patches;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.model, that.model) &&
            cmp.eq(this.attr, that.attr) &&
            cmp.eq(this.patches, that.patches);
    }
    [serialize](serializer) {
        return {
            kind: this.kind,
            attr: this.attr,
            model: this.model.ref(),
            patches: serializer.encode(this.patches),
        };
    }
}
export class TitleChangedEvent extends DocumentChangedEvent {
    title;
    static __name__ = "TitleChangedEvent";
    kind = "TitleChanged";
    constructor(document, title) {
        super(document);
        this.title = title;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.title, that.title);
    }
    [serialize](_serializer) {
        return {
            kind: this.kind,
            title: this.title,
        };
    }
}
export class RootAddedEvent extends DocumentChangedEvent {
    model;
    static __name__ = "RootAddedEvent";
    kind = "RootAdded";
    constructor(document, model) {
        super(document);
        this.model = model;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.model, that.model);
    }
    [serialize](serializer) {
        return {
            kind: this.kind,
            model: serializer.encode(this.model),
        };
    }
}
export class RootRemovedEvent extends DocumentChangedEvent {
    model;
    static __name__ = "RootRemovedEvent";
    kind = "RootRemoved";
    constructor(document, model) {
        super(document);
        this.model = model;
    }
    [equals](that, cmp) {
        return super[equals](that, cmp) &&
            cmp.eq(this.model, that.model);
    }
    [serialize](_serializer) {
        return {
            kind: this.kind,
            model: this.model.ref(),
        };
    }
}
//# sourceMappingURL=events.js.map