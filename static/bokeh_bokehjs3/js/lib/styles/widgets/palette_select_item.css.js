export const entry = "bk-entry"
export const swatch = "bk-swatch"
export const item = "bk-item"
export const active = "bk-active"
export default `.bk-entry{display:flex;flex-direction:row;flex-wrap:nowrap;align-items:center;gap:0.5em;}.bk-swatch{width:100px;height:auto;align-self:stretch;}.bk-item{--active-tool-highlight:#26aae1;border:1px solid transparent;}.bk-item.bk-active{border-color:var(--active-tool-highlight);}.bk-item:hover{background-color:#f9f9f9;}.bk-item:focus,.bk-item:focus-visible{outline:1px dotted var(--active-tool-highlight);outline-offset:-1px;}.bk-item::-moz-focus-inner{border:0;}`
