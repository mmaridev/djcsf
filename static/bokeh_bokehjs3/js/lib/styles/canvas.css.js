export const layer = "bk-layer"
export const events = "bk-events"
export default `.bk-layer{position:absolute;top:0;left:0;width:100%;height:100%;overflow:hidden;}.bk-events{touch-action:none;overflow:visible;}`
