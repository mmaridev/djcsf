export const nullish = "bk-nullish"
export const token = "bk-token"
export const boolean = "bk-boolean"
export const number = "bk-number"
export const string = "bk-string"
export const symbol = "bk-symbol"
export const model = "bk-model"
export const attr = "bk-attr"
export const array = "bk-array"
export const object = "bk-object"
export const iterable = "bk-iterable"
export default `.bk-nullish{color:#7724c1;}.bk-token{color:#881280;}.bk-boolean{color:#007500;}.bk-number{color:#1a1aa6;}.bk-string{color:#994500;}.bk-symbol{color:#c80000;}.bk-model{color:initial;}.bk-attr{color:#c80000;}.bk-array{color:initial;}.bk-object{color:initial;}.bk-iterable{color:initial;}`
