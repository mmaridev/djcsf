import { TickFormatter } from "./tick_formatter";
import { sprintf } from "../../core/util/templating";
export class PrintfTickFormatter extends TickFormatter {
    static __name__ = "PrintfTickFormatter";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str }) => ({
            format: [Str, "%s"],
        }));
    }
    doFormat(ticks, _opts) {
        return ticks.map((tick) => sprintf(this.format, tick));
    }
}
//# sourceMappingURL=printf_tick_formatter.js.map