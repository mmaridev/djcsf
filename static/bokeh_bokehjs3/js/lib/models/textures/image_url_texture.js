import { Texture } from "./texture";
import { ImageLoader } from "../../core/util/image";
export class ImageURLTexture extends Texture {
    static __name__ = "ImageURLTexture";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str }) => ({
            url: [Str],
        }));
    }
    _loader;
    initialize() {
        super.initialize();
        this._loader = new ImageLoader(this.url);
    }
    get_pattern(_color, _scale, _weight) {
        const { _loader } = this;
        return this._loader.finished ? _loader.image : _loader.promise;
    }
}
//# sourceMappingURL=image_url_texture.js.map