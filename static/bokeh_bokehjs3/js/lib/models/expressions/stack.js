import { Expression } from "./expression";
import { dict } from "../../core/util/object";
export class Stack extends Expression {
    static __name__ = "Stack";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str, List }) => ({
            fields: [List(Str), []],
        }));
    }
    _v_compute(source) {
        const n = source.get_length() ?? 0;
        const result = new Float64Array(n);
        for (const f of this.fields) {
            const column = dict(source.data).get(f);
            if (column != null) {
                const k = Math.min(n, column.length);
                for (let i = 0; i < k; i++) {
                    result[i] += column[i];
                }
            }
        }
        return result;
    }
}
//# sourceMappingURL=stack.js.map