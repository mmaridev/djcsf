import { Expression } from "./expression";
import { dict } from "../../core/util/object";
export class CumSum extends Expression {
    static __name__ = "CumSum";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Str }) => ({
            field: [Str],
            include_zero: [Bool, false],
        }));
    }
    _v_compute(source) {
        const result = new Float64Array(source.get_length() ?? 0);
        const column = (dict(source.data).get(this.field) ?? []);
        const offset = this.include_zero ? 1 : 0;
        result[0] = this.include_zero ? 0 : column[0];
        for (let i = 1; i < result.length; i++) {
            result[i] = result[i - 1] + column[i - offset];
        }
        return result;
    }
}
//# sourceMappingURL=cumsum.js.map