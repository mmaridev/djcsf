import { ValueRef, ValueRefView } from "./value_ref";
import { _get_column_value } from "../../core/util/templating";
import { span } from "../../core/dom";
import * as styles from "../../styles/tooltips.css";
export class ColorRefView extends ValueRefView {
    static __name__ = "ColorRefView";
    value_el;
    swatch_el;
    render() {
        super.render();
        this.value_el = span();
        this.swatch_el = span({ class: styles.tooltip_color_block }, " ");
        this.el.appendChild(this.value_el);
        this.el.appendChild(this.swatch_el);
    }
    update(source, i, _vars, _formatters) {
        const value = _get_column_value(this.model.field, source, i);
        const text = value == null ? "???" : `${value}`;
        this.el.textContent = text;
    }
}
export class ColorRef extends ValueRef {
    static __name__ = "ColorRef";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ColorRefView;
        this.define(({ Bool }) => ({
            hex: [Bool, true],
            swatch: [Bool, true],
        }));
    }
}
//# sourceMappingURL=color_ref.js.map