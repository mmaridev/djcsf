import { DOMElement, DOMElementView } from "./dom_element";
import { Action } from "./action";
import { PlaceholderView } from "./placeholder";
import { build_views, remove_views, traverse_views } from "../../core/build_views";
export class TemplateView extends DOMElementView {
    static __name__ = "TemplateView";
    action_views = new Map();
    *children() {
        yield* super.children();
        yield* this.action_views.values();
    }
    async lazy_initialize() {
        await super.lazy_initialize();
        await build_views(this.action_views, this.model.actions, { parent: this });
    }
    remove() {
        remove_views(this.action_views);
        super.remove();
    }
    update(source, i, vars, formatters) {
        traverse_views([this], (view) => {
            if (view instanceof PlaceholderView) {
                view.update(source, i, vars, formatters);
            }
        });
        for (const action of this.action_views.values()) {
            action.update(source, i, vars);
        }
    }
}
export class Template extends DOMElement {
    static __name__ = "Template";
    static {
        this.prototype.default_view = TemplateView;
        this.define(({ List, Ref }) => ({
            actions: [List(Ref(Action)), []],
        }));
    }
}
//# sourceMappingURL=template.js.map