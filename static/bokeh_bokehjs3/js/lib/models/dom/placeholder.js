import { DOMElement, DOMElementView } from "./dom_element";
import { CustomJS } from "../callbacks/customjs";
import { CustomJSHover } from "../tools/inspectors/customjs_hover";
import { BuiltinFormatter } from "../../core/enums";
import { Or, Ref } from "../../core/kinds";
export const Formatter = Or(BuiltinFormatter, Ref(CustomJS), Ref(CustomJSHover));
export class PlaceholderView extends DOMElementView {
    static __name__ = "PlaceholderView";
    static tag_name = "span";
}
export class Placeholder extends DOMElement {
    static __name__ = "Placeholder";
    constructor(attrs) {
        super(attrs);
    }
}
//# sourceMappingURL=placeholder.js.map