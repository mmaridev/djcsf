import { DOMNode, DOMNodeView } from "./dom_node";
import { StylesLike } from "../ui/styled_element";
import { UIElement } from "../ui/ui_element";
import { build_views, remove_views } from "../../core/build_views";
import { isString } from "../../core/util/types";
import { apply_styles } from "../../core/css";
import { empty } from "../../core/dom";
export class DOMElementView extends DOMNodeView {
    static __name__ = "DOMElementView";
    child_views = new Map();
    *children() {
        yield* super.children();
        yield* this.child_views.values();
    }
    async lazy_initialize() {
        await super.lazy_initialize();
        const children = this.model.children.filter((obj) => !isString(obj));
        await build_views(this.child_views, children, { parent: this });
    }
    remove() {
        remove_views(this.child_views);
        super.remove();
    }
    render() {
        empty(this.el);
        apply_styles(this.el.style, this.model.style);
        for (const child of this.model.children) {
            if (isString(child)) {
                const node = document.createTextNode(child);
                this.el.appendChild(node);
            }
            else {
                const child_view = this.child_views.get(child);
                child_view.render_to(this.el);
            }
        }
        this.finish();
    }
}
export class DOMElement extends DOMNode {
    static __name__ = "DOMElement";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str, List, Or, Ref }) => {
            return {
                style: [StylesLike, {}],
                children: [List(Or(Str, Ref(DOMNode), Ref(UIElement))), []],
            };
        });
    }
}
//# sourceMappingURL=dom_element.js.map