import { Placeholder, PlaceholderView, Formatter } from "./placeholder";
import { CustomJS } from "../callbacks/customjs";
import { CustomJSHover } from "../tools/inspectors/customjs_hover";
import { _get_column_value, MISSING, DEFAULT_FORMATTERS } from "../../core/util/templating";
import { execute } from "../../core/util/callbacks";
import { isArray } from "../../core/util/types";
export class ValueRefView extends PlaceholderView {
    static __name__ = "ValueRefView";
    update(source, i, vars, _formatters) {
        const { field, format, formatter } = this.model;
        const value = _get_column_value(field, source, i);
        const render = (output) => {
            if (output == null) {
                this.el.textContent = MISSING;
            }
            else if (output instanceof Node) {
                this.el.replaceChildren(output);
            }
            else if (isArray(output)) {
                this.el.replaceChildren(...output.map((item) => item instanceof Node ? item : `${item}`));
            }
            else {
                this.el.textContent = `${output}`;
            }
        };
        if (formatter instanceof CustomJS) {
            void (async () => {
                const output = await execute(formatter, this.model, { value, format, vars });
                render(output);
            })();
        }
        else {
            const output = (() => {
                if (format == null) {
                    return DEFAULT_FORMATTERS.basic(value, "", vars);
                }
                else {
                    if (formatter instanceof CustomJSHover) {
                        return formatter.format(value, format, vars);
                    }
                    else {
                        return DEFAULT_FORMATTERS[formatter](value, format, vars);
                    }
                }
            })();
            render(output);
        }
    }
}
export class ValueRef extends Placeholder {
    static __name__ = "ValueRef";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ValueRefView;
        this.define(({ Str, Nullable }) => ({
            field: [Str],
            format: [Nullable(Str), null],
            formatter: [Formatter, "raw"],
        }));
    }
}
//# sourceMappingURL=value_ref.js.map