import { Model } from "../../model";
import * as dom from "../../core/dom";
export class StyleSheet extends Model {
    static __name__ = "StyleSheet";
    constructor(attrs) {
        super(attrs);
    }
}
export class InlineStyleSheet extends StyleSheet {
    static __name__ = "InlineStyleSheet";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str }) => ({
            css: [Str],
        }));
    }
    underlying() {
        return new dom.InlineStyleSheet(this.css);
    }
}
export class ImportedStyleSheet extends StyleSheet {
    static __name__ = "ImportedStyleSheet";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str }) => ({
            url: [Str],
        }));
    }
    underlying() {
        return new dom.ImportedStyleSheet(this.url);
    }
}
export class GlobalInlineStyleSheet extends InlineStyleSheet {
    static __name__ = "GlobalInlineStyleSheet";
    constructor(attrs) {
        super(attrs);
    }
    _underlying = null;
    underlying() {
        if (this._underlying == null) {
            this._underlying = new dom.GlobalInlineStyleSheet(this.css);
        }
        return this._underlying;
    }
}
export class GlobalImportedStyleSheet extends ImportedStyleSheet {
    static __name__ = "GlobalImportedStyleSheet";
    constructor(attrs) {
        super(attrs);
    }
    _underlying = null;
    underlying() {
        if (this._underlying == null) {
            this._underlying = new dom.GlobalImportedStyleSheet(this.url);
        }
        return this._underlying;
    }
}
//# sourceMappingURL=stylesheets.js.map