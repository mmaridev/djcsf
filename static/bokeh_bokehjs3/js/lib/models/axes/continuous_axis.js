import { Axis, AxisView } from "./axis";
export class ContinuousAxisView extends AxisView {
    static __name__ = "ContinuousAxisView";
    _hit_value(sx, sy) {
        const [range] = this.ranges;
        const { start, end, span } = range;
        switch (this.dimension) {
            case 0: {
                const { x0, width } = this.bbox;
                return span * (sx - x0) / width + start;
            }
            case 1: {
                const { y0, height } = this.bbox;
                return end - span * (sy - y0) / height;
            }
        }
    }
}
export class ContinuousAxis extends Axis {
    static __name__ = "ContinuousAxis";
    constructor(attrs) {
        super(attrs);
    }
}
//# sourceMappingURL=continuous_axis.js.map