import { LinearAxis, LinearAxisView } from "./linear_axis";
import { MercatorTickFormatter } from "../formatters/mercator_tick_formatter";
import { MercatorTicker } from "../tickers/mercator_ticker";
export class MercatorAxisView extends LinearAxisView {
    static __name__ = "MercatorAxisView";
}
export class MercatorAxis extends LinearAxis {
    static __name__ = "MercatorAxis";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = MercatorAxisView;
        this.override({
            ticker: () => new MercatorTicker({ dimension: "lat" }),
            formatter: () => new MercatorTickFormatter({ dimension: "lat" }),
        });
    }
}
//# sourceMappingURL=mercator_axis.js.map