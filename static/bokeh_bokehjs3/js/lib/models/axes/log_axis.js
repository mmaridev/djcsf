import { ContinuousAxis, ContinuousAxisView } from "./continuous_axis";
import { LogTickFormatter } from "../formatters/log_tick_formatter";
import { LogTicker } from "../tickers/log_ticker";
export class LogAxisView extends ContinuousAxisView {
    static __name__ = "LogAxisView";
    _hit_value(sx, sy) {
        const [range] = this.ranges;
        const { start, end } = range;
        const { log10 } = Math;
        switch (this.dimension) {
            case 0: {
                const { x0, width } = this.bbox;
                return log10(end / start) * (sx - x0) / width + log10(start);
            }
            case 1: {
                const { y0, height } = this.bbox;
                return log10(end) - log10(end / start) * (sy - y0) / height;
            }
        }
    }
}
export class LogAxis extends ContinuousAxis {
    static __name__ = "LogAxis";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = LogAxisView;
        this.override({
            ticker: () => new LogTicker(),
            formatter: () => new LogTickFormatter(),
        });
    }
}
//# sourceMappingURL=log_axis.js.map