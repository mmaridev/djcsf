import { Data } from "@bokeh/slickgrid";
const { Avg, Min, Max, Sum } = Data.Aggregators;
import { Model } from "../../../model";
export class RowAggregator extends Model {
    static __name__ = "RowAggregator";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str }) => ({
            field_: [Str, ""],
        }));
    }
}
const avg = new Avg();
export class AvgAggregator extends RowAggregator {
    static __name__ = "AvgAggregator";
    key = "avg";
    init = avg.init;
    accumulate = avg.accumulate;
    storeResult = avg.storeResult;
}
const min = new Min();
export class MinAggregator extends RowAggregator {
    static __name__ = "MinAggregator";
    key = "min";
    init = min.init;
    accumulate = min.accumulate;
    storeResult = min.storeResult;
}
const max = new Max();
export class MaxAggregator extends RowAggregator {
    static __name__ = "MaxAggregator";
    key = "max";
    init = max.init;
    accumulate = max.accumulate;
    storeResult = max.storeResult;
}
const sum = new Sum();
export class SumAggregator extends RowAggregator {
    static __name__ = "SumAggregator";
    key = "sum";
    init = sum.init;
    accumulate = sum.accumulate;
    storeResult = sum.storeResult;
}
//# sourceMappingURL=row_aggregators.js.map