import { CellFormatter, StringFormatter } from "./cell_formatters";
import { CellEditor, StringEditor } from "./cell_editors";
import { unique_id } from "../../../core/util/string";
import { Sort } from "../../../core/enums";
import { Comparison } from "../../../models/comparisons";
import { Model } from "../../../model";
export class TableColumn extends Model {
    static __name__ = "TableColumn";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Float, Str, Nullable, Ref }) => ({
            field: [Str],
            title: [Nullable(Str), null],
            width: [Float, 300],
            formatter: [Ref(CellFormatter), () => new StringFormatter()],
            editor: [Ref(CellEditor), () => new StringEditor()],
            sortable: [Bool, true],
            default_sort: [Sort, "ascending"],
            visible: [Bool, true],
            sorter: [Nullable(Ref(Comparison)), null],
        }));
    }
    toColumn() {
        return {
            id: unique_id(),
            field: this.field,
            name: this.title ?? this.field,
            width: this.width,
            formatter: this.formatter.doFormat.bind(this.formatter),
            model: this.editor,
            editor: this.editor.default_view,
            sortable: this.sortable,
            defaultSortAsc: this.default_sort == "ascending",
            sorter: this.sorter,
        };
    }
}
//# sourceMappingURL=table_column.js.map