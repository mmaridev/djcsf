import { AbstractButton, AbstractButtonView } from "./abstract_button";
import { ButtonClick } from "../../core/bokeh_events";
import * as buttons from "../../styles/buttons.css";
export class ToggleView extends AbstractButtonView {
    static __name__ = "ToggleView";
    connect_signals() {
        super.connect_signals();
        this.connect(this.model.properties.active.change, () => this._update_active());
    }
    render() {
        super.render();
        this._update_active();
    }
    click() {
        this.model.active = !this.model.active;
        this.model.trigger_event(new ButtonClick());
        super.click();
    }
    _update_active() {
        this.button_el.classList.toggle(buttons.active, this.model.active);
    }
}
export class Toggle extends AbstractButton {
    static __name__ = "Toggle";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ToggleView;
        this.define(({ Bool }) => ({
            active: [Bool, false],
        }));
        this.override({
            label: "Toggle",
        });
    }
}
//# sourceMappingURL=toggle.js.map