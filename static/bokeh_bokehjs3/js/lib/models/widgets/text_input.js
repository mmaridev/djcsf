import { TextLikeInput, TextLikeInputView } from "./text_like_input";
import { input, div } from "../../core/dom";
import { ValueSubmit } from "../../core/bokeh_events";
import * as inputs from "../../styles/widgets/inputs.css";
export class TextInputView extends TextLikeInputView {
    static __name__ = "TextInputView";
    connect_signals() {
        super.connect_signals();
        const { prefix, suffix } = this.model.properties;
        this.on_change([prefix, suffix], () => this.render());
    }
    _render_input() {
        this.input_el = input({ type: "text", class: inputs.input });
        const { prefix, suffix } = this.model;
        const prefix_el = prefix != null ? div({ class: "bk-input-prefix" }, prefix) : null;
        const suffix_el = suffix != null ? div({ class: "bk-input-suffix" }, suffix) : null;
        const container_el = div({ class: "bk-input-container" }, prefix_el, this.input_el, suffix_el);
        return container_el;
    }
    render() {
        super.render();
        this.input_el.addEventListener("keyup", (event) => this._keyup(event));
    }
    _keyup(event) {
        if (event.key == "Enter" && !event.shiftKey && !event.ctrlKey && !event.altKey) {
            this.model.trigger_event(new ValueSubmit(this.input_el.value));
        }
    }
}
export class TextInput extends TextLikeInput {
    static __name__ = "TextInput";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = TextInputView;
        this.define(({ Str, Nullable }) => ({
            prefix: [Nullable(Str), null],
            suffix: [Nullable(Str), null],
        }));
    }
}
//# sourceMappingURL=text_input.js.map