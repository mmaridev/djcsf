import * as numbro from "@bokeh/numbro";
import { NumericalRangeSlider, NumericalRangeSliderView } from "./numerical_range_slider";
import { isString } from "../../../core/util/types";
export class RangeSliderView extends NumericalRangeSliderView {
    static __name__ = "RangeSliderView";
    behaviour = "drag";
    connected = [false, true, false];
    _formatter(value, format) {
        if (isString(format)) {
            return numbro.format(value, format);
        }
        else {
            return format.compute(value);
        }
    }
}
export class RangeSlider extends NumericalRangeSlider {
    static __name__ = "RangeSlider";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = RangeSliderView;
        this.override({
            format: "0[.]00",
        });
    }
}
//# sourceMappingURL=range_slider.js.map