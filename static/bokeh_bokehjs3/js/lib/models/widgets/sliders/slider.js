import * as numbro from "@bokeh/numbro";
import { NumericalSlider, NumericalSliderView } from "./numerical_slider";
import { isString } from "../../../core/util/types";
export class SliderView extends NumericalSliderView {
    static __name__ = "SliderView";
    behaviour = "tap";
    connected = [true, false];
    _formatter(value, format) {
        if (isString(format)) {
            return numbro.format(value, format);
        }
        else {
            return format.compute(value);
        }
    }
}
export class Slider extends NumericalSlider {
    static __name__ = "Slider";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = SliderView;
        this.override({
            format: "0[.]00",
        });
    }
}
//# sourceMappingURL=slider.js.map