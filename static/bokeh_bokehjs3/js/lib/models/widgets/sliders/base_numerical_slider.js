import { AbstractSlider, AbstractSliderView } from "./abstract_slider";
import { TickFormatter } from "../../formatters/tick_formatter";
export class BaseNumericalSliderView extends AbstractSliderView {
    static __name__ = "BaseNumericalSliderView";
    connect_signals() {
        super.connect_signals();
        const { start, end, step } = this.model.properties;
        this.on_change([start, end, step], () => this._update_slider());
    }
    pretty(value) {
        return this._formatter(value, this.model.format);
    }
}
export class BaseNumericalSlider extends AbstractSlider {
    static __name__ = "BaseNumericalSlider";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float, Str, Or, Ref }) => {
            return {
                start: [Float],
                end: [Float],
                step: [Float, 1],
                format: [Or(Str, Ref(TickFormatter))],
            };
        });
    }
}
//# sourceMappingURL=base_numerical_slider.js.map