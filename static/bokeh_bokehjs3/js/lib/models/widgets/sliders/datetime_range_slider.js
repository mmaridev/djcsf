import tz from "timezone";
import { NumericalRangeSlider, NumericalRangeSliderView } from "./numerical_range_slider";
import { isString } from "../../../core/util/types";
export class DatetimeRangeSliderView extends NumericalRangeSliderView {
    static __name__ = "DatetimeRangeSliderView";
    behaviour = "drag";
    connected = [false, true, false];
    _formatter(value, format) {
        if (isString(format)) {
            return tz(value, format);
        }
        else {
            return format.compute(value);
        }
    }
}
export class DatetimeRangeSlider extends NumericalRangeSlider {
    static __name__ = "DatetimeRangeSlider";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = DatetimeRangeSliderView;
        this.override({
            format: "%d %b %Y %H:%M:%S",
            step: 3_600_000, // 1 hour
        });
    }
}
//# sourceMappingURL=datetime_range_slider.js.map