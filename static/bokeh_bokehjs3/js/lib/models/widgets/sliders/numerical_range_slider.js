import { BaseNumericalSlider, BaseNumericalSliderView } from "./base_numerical_slider";
export class NumericalRangeSliderView extends BaseNumericalSliderView {
    static __name__ = "NumericalRangeSliderView";
    _calc_to() {
        return {
            range: {
                min: this.model.start,
                max: this.model.end,
            },
            start: this.model.value,
            step: this.model.step,
        };
    }
    _calc_from(values) {
        return values;
    }
}
export class NumericalRangeSlider extends BaseNumericalSlider {
    static __name__ = "NumericalRangeSlider";
    constructor(attrs) {
        super(attrs);
    }
}
//# sourceMappingURL=numerical_range_slider.js.map