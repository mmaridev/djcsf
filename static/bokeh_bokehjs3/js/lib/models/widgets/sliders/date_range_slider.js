import tz from "timezone";
import { NumericalRangeSlider, NumericalRangeSliderView } from "./numerical_range_slider";
import { isString } from "../../../core/util/types";
export class DateRangeSliderView extends NumericalRangeSliderView {
    static __name__ = "DateRangeSliderView";
    behaviour = "drag";
    connected = [false, true, false];
    _calc_to() {
        const spec = super._calc_to();
        spec.step *= 86_400_000;
        return spec;
    }
    _formatter(value, format) {
        if (isString(format)) {
            return tz(value, format);
        }
        else {
            return format.compute(value);
        }
    }
}
export class DateRangeSlider extends NumericalRangeSlider {
    static __name__ = "DateRangeSlider";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = DateRangeSliderView;
        this.override({
            format: "%d %b %Y",
        });
    }
}
//# sourceMappingURL=date_range_slider.js.map