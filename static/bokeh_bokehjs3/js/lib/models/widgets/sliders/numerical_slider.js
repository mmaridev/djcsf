import { BaseNumericalSlider, BaseNumericalSliderView } from "./base_numerical_slider";
export class NumericalSliderView extends BaseNumericalSliderView {
    static __name__ = "NumericalSliderView";
    _calc_to() {
        const { start, end, value, step } = this.model;
        return {
            range: {
                min: start,
                max: end,
            },
            start: [value],
            step,
        };
    }
    _calc_from([value]) {
        if (Number.isInteger(this.model.start) && Number.isInteger(this.model.end) && Number.isInteger(this.model.step)) {
            return Math.round(value);
        }
        else {
            return value;
        }
    }
}
export class NumericalSlider extends BaseNumericalSlider {
    static __name__ = "NumericalSlider";
    constructor(attrs) {
        super(attrs);
    }
}
//# sourceMappingURL=numerical_slider.js.map