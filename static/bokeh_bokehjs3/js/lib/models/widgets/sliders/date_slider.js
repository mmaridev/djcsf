import tz from "timezone";
import { NumericalSlider, NumericalSliderView } from "./numerical_slider";
import { isString } from "../../../core/util/types";
export class DateSliderView extends NumericalSliderView {
    static __name__ = "DateSliderView";
    behaviour = "tap";
    connected = [true, false];
    _calc_to() {
        const spec = super._calc_to();
        spec.step *= 86_400_000;
        return spec;
    }
    _formatter(value, format) {
        if (isString(format)) {
            return tz(value, format);
        }
        else {
            return format.compute(value);
        }
    }
}
export class DateSlider extends NumericalSlider {
    static __name__ = "DateSlider";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = DateSliderView;
        this.override({
            format: "%d %b %Y",
        });
    }
}
//# sourceMappingURL=date_slider.js.map