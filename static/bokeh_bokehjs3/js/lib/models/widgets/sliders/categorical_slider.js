import { AbstractSlider, AbstractSliderView } from "./abstract_slider";
import { isNumber } from "../../../core/util/types";
export class CategoricalSliderView extends AbstractSliderView {
    static __name__ = "CategoricalSliderView";
    behaviour = "tap";
    connect_signals() {
        super.connect_signals();
        const { categories } = this.model.properties;
        this.on_change([categories], () => this._update_slider());
    }
    _calc_to() {
        const { categories } = this.model;
        return {
            range: {
                min: 0,
                max: categories.length - 1,
            },
            start: [this.model.value],
            step: 1,
            format: {
                to: (value) => categories[value],
                from: (value) => categories.indexOf(value),
            },
        };
    }
    _calc_from([value]) {
        const { categories } = this.model;
        return categories[value | 0]; // value may not be an integer due to noUiSlider's FP math
    }
    pretty(value) {
        return isNumber(value) ? this.model.categories[value] : value;
    }
}
export class CategoricalSlider extends AbstractSlider {
    static __name__ = "CategoricalSlider";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = CategoricalSliderView;
        this.define(({ List, Str }) => ({
            categories: [List(Str)],
        }));
    }
}
//# sourceMappingURL=categorical_slider.js.map