import { ButtonType } from "../../core/enums";
import { prepend, nbsp, text, button, div } from "../../core/dom";
import { build_view } from "../../core/build_views";
import { isString } from "../../core/util/types";
import { Control, ControlView } from "./control";
import { DOMNode } from "../dom/dom_node";
import { Text } from "../dom/text";
import { Icon } from "../ui/icons/icon";
import buttons_css, * as buttons from "../../styles/buttons.css";
export class AbstractButtonView extends ControlView {
    static __name__ = "AbstractButtonView";
    label_view;
    icon_view;
    button_el;
    group_el;
    *controls() {
        yield this.button_el;
    }
    *children() {
        yield* super.children();
        if (this.label_view != null) {
            yield this.label_view;
        }
        if (this.icon_view != null) {
            yield this.icon_view;
        }
    }
    async lazy_initialize() {
        await super.lazy_initialize();
        await this._rebuild_label();
        await this._rebuild_icon();
    }
    async _rebuild_label() {
        this.label_view?.remove();
        const label = (() => {
            const { label } = this.model;
            return isString(label) ? new Text({ content: label }) : label;
        })();
        this.label_view = await this.owner.build_view(label, this);
    }
    async _rebuild_icon() {
        this.icon_view?.remove();
        const { icon } = this.model;
        if (icon != null) {
            this.icon_view = await build_view(icon, { parent: this });
        }
    }
    connect_signals() {
        super.connect_signals();
        const { label, icon, button_type, disabled } = this.model.properties;
        this.on_transitive_change(label, async () => {
            await this._rebuild_label();
            this.render();
        });
        this.on_transitive_change(icon, async () => {
            await this._rebuild_icon();
            this.render();
        });
        this.on_change([button_type, disabled], () => {
            this.render();
        });
    }
    remove() {
        this.label_view?.remove();
        this.icon_view?.remove();
        super.remove();
    }
    stylesheets() {
        return [...super.stylesheets(), buttons_css];
    }
    _render_button(...children) {
        return button({
            type: "button",
            disabled: this.model.disabled,
            class: [buttons.btn, buttons[`btn_${this.model.button_type}`]],
        }, ...children);
    }
    render() {
        super.render();
        this.label_view?.render();
        this.button_el = this._render_button(this.label_view?.el);
        this.button_el.addEventListener("click", () => this.click());
        if (this.icon_view != null) {
            const separator = this.model.label != "" ? nbsp() : text("");
            prepend(this.button_el, this.icon_view.el, separator);
            this.icon_view.render();
        }
        this.group_el = div({ class: buttons.btn_group }, this.button_el);
        this.shadow_el.append(this.group_el);
    }
    click() { }
}
export class AbstractButton extends Control {
    static __name__ = "AbstractButton";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str, Ref, Or, Nullable }) => ({
            label: [Or(Ref(DOMNode), Str), "Button"],
            icon: [Nullable(Ref(Icon)), null],
            button_type: [ButtonType, "default"],
        }));
    }
}
//# sourceMappingURL=abstract_button.js.map