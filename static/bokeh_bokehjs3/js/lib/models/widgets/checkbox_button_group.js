import { ToggleButtonGroup, ToggleButtonGroupView } from "./toggle_button_group";
import * as buttons from "../../styles/buttons.css";
export class CheckboxButtonGroupView extends ToggleButtonGroupView {
    static __name__ = "CheckboxButtonGroupView";
    get active() {
        return new Set(this.model.active);
    }
    change_active(i) {
        const { active } = this;
        active.has(i) ? active.delete(i) : active.add(i);
        this.model.active = [...active].sort();
    }
    _update_active() {
        const { active } = this;
        this._buttons.forEach((button_el, i) => {
            button_el.classList.toggle(buttons.active, active.has(i));
        });
    }
}
export class CheckboxButtonGroup extends ToggleButtonGroup {
    static __name__ = "CheckboxButtonGroup";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = CheckboxButtonGroupView;
        this.define(({ Int, List }) => ({
            active: [List(Int), []],
        }));
    }
}
//# sourceMappingURL=checkbox_button_group.js.map