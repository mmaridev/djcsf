import { div } from "../../core/dom";
import { Widget, WidgetView } from "./widget";
import clearfix_css, { clearfix } from "../../styles/clearfix.css";
export class MarkupView extends WidgetView {
    static __name__ = "MarkupView";
    markup_el;
    _auto_width = "fit-content";
    _auto_height = "auto";
    async lazy_initialize() {
        await super.lazy_initialize();
        if (this.provider.status == "not_started" || this.provider.status == "loading") {
            this.provider.ready.connect(() => {
                if (this.contains_tex_string(this.model.text)) {
                    this.rerender();
                }
            });
        }
    }
    has_math_disabled() {
        return this.model.disable_math || !this.contains_tex_string(this.model.text);
    }
    rerender() {
        this.render();
    }
    connect_signals() {
        super.connect_signals();
        this.connect(this.model.change, () => {
            this.rerender();
        });
    }
    stylesheets() {
        return [...super.stylesheets(), clearfix_css, "p { margin: 0; }"];
    }
    render() {
        super.render();
        this.markup_el = div({ class: clearfix, style: { display: "inline-block" } });
        this.shadow_el.appendChild(this.markup_el);
        if (this.provider.status == "failed" || this.provider.status == "loaded") {
            this._has_finished = true;
        }
    }
}
export class Markup extends Widget {
    static __name__ = "Markup";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Str }) => ({
            text: [Str, ""],
            disable_math: [Bool, false],
        }));
    }
}
//# sourceMappingURL=markup.js.map