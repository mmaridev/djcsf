import { ToggleInput, ToggleInputView } from "./toggle_input";
import { input, span } from "../../core/dom";
import checkbox_css from "../../styles/widgets/checkbox.css";
export class CheckboxView extends ToggleInputView {
    static __name__ = "CheckboxView";
    checkbox_el;
    label_el;
    stylesheets() {
        return [...super.stylesheets(), checkbox_css];
    }
    connect_signals() {
        super.connect_signals();
        const { label } = this.model.properties;
        this.on_change(label, () => this._update_label());
    }
    render() {
        super.render();
        this.checkbox_el = input({ type: "checkbox" });
        this.label_el = span(this.model.label);
        this.checkbox_el.addEventListener("change", () => this._toggle_active());
        this._update_active();
        this._update_disabled();
        this.shadow_el.append(this.checkbox_el, this.label_el);
    }
    _update_active() {
        this.checkbox_el.checked = this.model.active;
    }
    _update_disabled() {
        this.checkbox_el.toggleAttribute("disabled", this.model.disabled);
    }
    _update_label() {
        this.label_el.textContent = this.model.label;
    }
}
export class Checkbox extends ToggleInput {
    static __name__ = "Checkbox";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = CheckboxView;
        this.define(({ Str }) => ({
            label: [Str, ""],
        }));
    }
}
//# sourceMappingURL=checkbox.js.map