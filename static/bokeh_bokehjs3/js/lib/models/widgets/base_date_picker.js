import { PickerBase, PickerBaseView } from "./picker_base";
import { isArray } from "../../core/util/types";
import { Or, Tuple, Str, Float, List, Ref, Struct } from "../../core/kinds";
export const DateLike = Or(Ref(Date), Str, Float);
export const DateLikeList = List(Or(DateLike, Tuple(DateLike, DateLike), Struct({ from: DateLike, to: DateLike })));
export class BaseDatePickerView extends PickerBaseView {
    static __name__ = "BaseDatePickerView";
    _format_date(date) {
        const { picker } = this;
        return picker.formatDate(date, picker.config.dateFormat);
    }
    connect_signals() {
        super.connect_signals();
        const { value, min_date, max_date, disabled_dates, enabled_dates, date_format } = this.model.properties;
        this.connect(value.change, () => {
            const { value } = this.model;
            if (value != null) {
                this.picker.setDate(value);
            }
            else {
                this.picker.clear();
            }
        });
        this.connect(min_date.change, () => this.picker.set("minDate", this.model.min_date));
        this.connect(max_date.change, () => this.picker.set("maxDate", this.model.max_date));
        this.connect(disabled_dates.change, () => {
            const { disabled_dates } = this.model;
            this.picker.set("disable", disabled_dates != null ? this._convert_date_list(disabled_dates) : []);
        });
        this.connect(enabled_dates.change, () => {
            const { enabled_dates } = this.model;
            if (enabled_dates != null) {
                this.picker.set("enable", this._convert_date_list(enabled_dates));
            }
            else {
                // this reimplements `set()` for the `undefined` case
                this.picker.config._enable = undefined;
                this.picker.redraw();
                this.picker.updateValue(true);
            }
        });
        this.connect(date_format.change, () => this.picker.set("altFormat", this.model.date_format));
    }
    get flatpickr_options() {
        const { value, min_date, max_date, disabled_dates, enabled_dates, date_format } = this.model;
        const options = super.flatpickr_options;
        options.altInput = true;
        options.altFormat = date_format;
        options.dateFormat = "Y-m-d";
        if (value != null) {
            options.defaultDate = value;
        }
        if (min_date != null) {
            options.minDate = min_date;
        }
        if (max_date != null) {
            options.maxDate = max_date;
        }
        if (disabled_dates != null) {
            options.disable = this._convert_date_list(disabled_dates);
        }
        if (enabled_dates != null) {
            options.enable = this._convert_date_list(enabled_dates);
        }
        return options;
    }
    _convert_date_list(value) {
        const result = [];
        for (const item of value) {
            if (isArray(item)) {
                const [from, to] = item;
                result.push({ from, to });
            }
            else {
                result.push(item);
            }
        }
        return result;
    }
}
export class BaseDatePicker extends PickerBase {
    static __name__ = "BaseDatePicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Nullable }) => ({
            min_date: [Nullable(DateLike), null],
            max_date: [Nullable(DateLike), null],
            disabled_dates: [Nullable(DateLikeList), null],
            enabled_dates: [Nullable(DateLikeList), null],
            date_format: [Str, "Y-m-d"],
        }));
    }
}
//# sourceMappingURL=base_date_picker.js.map