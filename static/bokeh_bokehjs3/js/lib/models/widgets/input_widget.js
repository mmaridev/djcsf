var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ClearInput_1;
import { Control, ControlView } from "./control";
import { Tooltip } from "../ui/tooltip";
import { HTML, HTMLView } from "../dom/html";
import { isString } from "../../core/util/types";
import { build_view } from "../../core/build_views";
import { div, label } from "../../core/dom";
import { View } from "../../core/view";
import { server_event, ModelEvent } from "../../core/bokeh_events";
import inputs_css, * as inputs from "../../styles/widgets/inputs.css";
import icons_css from "../../styles/icons.css";
let ClearInput = class ClearInput extends ModelEvent {
    static { ClearInput_1 = this; }
    model;
    static __name__ = "ClearInput";
    constructor(model) {
        super();
        this.model = model;
        this.origin = model;
    }
    static from_values(values) {
        const { model } = values;
        return new ClearInput_1(model);
    }
};
ClearInput = ClearInput_1 = __decorate([
    server_event("clear_input")
], ClearInput);
export { ClearInput };
export class InputWidgetView extends ControlView {
    static __name__ = "InputWidgetView";
    title;
    description = null;
    input_el;
    title_el;
    desc_el = null;
    group_el;
    *controls() {
        yield this.input_el;
    }
    *children() {
        yield* super.children();
        const { title, description } = this;
        if (title instanceof View) {
            yield title;
        }
        if (description instanceof View) {
            yield description;
        }
    }
    async lazy_initialize() {
        await super.lazy_initialize();
        await this._build_title();
        await this._build_description();
    }
    remove() {
        const { title, description } = this;
        if (title instanceof View) {
            title.remove();
        }
        if (description instanceof View) {
            description.remove();
        }
        super.remove();
    }
    connect_signals() {
        super.connect_signals();
        const { title, description } = this.model.properties;
        this.on_change(title, async () => {
            await this._build_title();
            this.render();
        });
        this.on_change(description, async () => {
            await this._build_description();
            this.render();
        });
    }
    stylesheets() {
        return [...super.stylesheets(), inputs_css, icons_css];
    }
    render() {
        super.render();
        this.desc_el = this._build_description_el();
        this.title_el = this._build_title_el();
        const input_or_container_el = this._render_input();
        this.input_el.id = "input";
        this.group_el = div({ class: inputs.input_group }, this.title_el, input_or_container_el);
        this.shadow_el.append(this.group_el);
    }
    _build_description_el() {
        const { description } = this;
        if (description == null) {
            return null;
        }
        else {
            const icon_el = div({ class: inputs.icon });
            const desc_el = div({ class: inputs.description }, icon_el);
            if (isString(description)) {
                desc_el.title = description;
            }
            else {
                if (description.model.target == "auto") {
                    description.target = desc_el;
                }
                let persistent = false;
                const toggle = (visible) => {
                    description.model.setv({
                        visible,
                        closable: persistent,
                    });
                    icon_el.classList.toggle(inputs.opaque, visible && persistent);
                };
                this.on_change(description.model.properties.visible, () => {
                    const { visible } = description.model;
                    if (!visible) {
                        persistent = false;
                    }
                    toggle(visible);
                });
                desc_el.addEventListener("mouseenter", () => {
                    toggle(true);
                });
                desc_el.addEventListener("mouseleave", () => {
                    if (!persistent) {
                        toggle(false);
                    }
                });
                document.addEventListener("mousedown", (event) => {
                    const path = event.composedPath();
                    if (path.includes(description.el)) {
                        return;
                    }
                    else if (path.includes(desc_el)) {
                        persistent = !persistent;
                        toggle(persistent);
                    }
                    else {
                        persistent = false;
                        toggle(false);
                    }
                });
                window.addEventListener("blur", () => {
                    persistent = false;
                    toggle(false);
                });
            }
            return desc_el;
        }
    }
    async _build_title() {
        const { title } = this.model;
        if (title instanceof HTML) {
            this.title = await build_view(title, { parent: this });
        }
        else {
            this.title = title;
        }
    }
    async _build_description() {
        const { description } = this.model;
        if (description instanceof Tooltip) {
            this.description = await build_view(description, { parent: this });
        }
        else {
            this.description = description;
        }
    }
    _build_title_el() {
        const { title } = this;
        const content = (() => {
            if (title instanceof HTMLView) {
                title.render();
                return title.el;
            }
            else {
                return title;
            }
        })();
        const display = title == "" ? "none" : "";
        return label({ for: "input", style: { display } }, content, this.desc_el);
    }
    change_input() { }
}
export class InputWidget extends Control {
    static __name__ = "InputWidget";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str, Nullable, Or, Ref }) => ({
            title: [Or(Str, Ref(HTML)), ""],
            description: [Nullable(Or(Str, Ref(Tooltip))), null],
        }));
    }
}
//# sourceMappingURL=input_widget.js.map