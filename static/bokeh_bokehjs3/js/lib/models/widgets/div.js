import { Markup, MarkupView } from "./markup";
export class DivView extends MarkupView {
    static __name__ = "DivView";
    render() {
        super.render();
        if (this.model.render_as_text) {
            this.markup_el.textContent = this.model.text;
        }
        else {
            this.markup_el.innerHTML = this.has_math_disabled() ? this.model.text : this.process_tex(this.model.text);
        }
    }
}
export class Div extends Markup {
    static __name__ = "Div";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = DivView;
        this.define(({ Bool }) => ({
            render_as_text: [Bool, false],
        }));
    }
}
//# sourceMappingURL=div.js.map