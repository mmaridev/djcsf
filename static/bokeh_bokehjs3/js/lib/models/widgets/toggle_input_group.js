import { Control, ControlView } from "./control";
import inputs_css from "../../styles/widgets/inputs.css";
import checkbox_css from "../../styles/widgets/checkbox.css";
export class ToggleInputGroupView extends ControlView {
    static __name__ = "ToggleInputGroupView";
    _inputs;
    *controls() {
        yield* this._inputs;
    }
    connect_signals() {
        super.connect_signals();
        const { labels, inline } = this.model.properties;
        this.on_change([labels, inline], () => this.render());
    }
    stylesheets() {
        return [...super.stylesheets(), inputs_css, checkbox_css];
    }
}
export class ToggleInputGroup extends Control {
    static __name__ = "ToggleInputGroup";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Str, List }) => ({
            labels: [List(Str), []],
            inline: [Bool, false],
        }));
    }
}
//# sourceMappingURL=toggle_input_group.js.map