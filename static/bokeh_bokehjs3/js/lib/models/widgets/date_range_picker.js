import { BaseDatePicker, BaseDatePickerView, DateLike } from "./base_date_picker";
import { assert } from "../../core/util/assert";
export class DateRangePickerView extends BaseDatePickerView {
    static __name__ = "DateRangePickerView";
    get flatpickr_options() {
        return {
            ...super.flatpickr_options,
            mode: "range",
        };
    }
    _on_change(selected) {
        switch (selected.length) {
            case 0:
                this.model.value = null;
                break;
            case 1: {
                // Selection in progress, so do nothing and wait for two selected
                // dates. Single date selection is still possible and represented
                // by [date, date] tuple.
                break;
            }
            case 2: {
                const [from, to] = selected;
                const from_date = this._format_date(from);
                const to_date = this._format_date(to);
                this.model.value = [from_date, to_date];
                break;
            }
            default: {
                assert(false, "invalid length");
            }
        }
    }
}
export class DateRangePicker extends BaseDatePicker {
    static __name__ = "DateRangePicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = DateRangePickerView;
        this.define(({ Tuple, Nullable }) => ({
            value: [Nullable(Tuple(DateLike, DateLike)), null],
        }));
    }
}
//# sourceMappingURL=date_range_picker.js.map