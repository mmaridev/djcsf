import { InputWidget, InputWidgetView } from "./input_widget";
import { input } from "../../core/dom";
import { color2hexrgb } from "../../core/util/color";
import * as inputs from "../../styles/widgets/inputs.css";
export class ColorPickerView extends InputWidgetView {
    static __name__ = "ColorPickerView";
    connect_signals() {
        super.connect_signals();
        this.connect(this.model.properties.name.change, () => this.input_el.name = this.model.name ?? "");
        this.connect(this.model.properties.color.change, () => this.input_el.value = color2hexrgb(this.model.color));
        this.connect(this.model.properties.disabled.change, () => this.input_el.disabled = this.model.disabled);
    }
    _render_input() {
        return this.input_el = input({
            type: "color",
            class: inputs.input,
            name: this.model.name,
            value: color2hexrgb(this.model.color),
            disabled: this.model.disabled,
        });
    }
    render() {
        super.render();
        this.input_el.addEventListener("change", () => this.change_input());
    }
    change_input() {
        this.model.color = this.input_el.value;
        super.change_input();
    }
}
export class ColorPicker extends InputWidget {
    static __name__ = "ColorPicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ColorPickerView;
        this.define(({ Color }) => ({
            color: [Color, "#000000"],
        }));
    }
}
//# sourceMappingURL=color_picker.js.map