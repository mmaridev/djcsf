import { Widget, WidgetView } from "./widget";
export class ToggleInputView extends WidgetView {
    static __name__ = "ToggleInputView";
    connect_signals() {
        super.connect_signals();
        const { active, disabled } = this.model.properties;
        this.on_change(active, () => this._update_active());
        this.on_change(disabled, () => this._update_disabled());
    }
    _toggle_active() {
        if (!this.model.disabled) {
            this.model.active = !this.model.active;
        }
    }
}
export class ToggleInput extends Widget {
    static __name__ = "ToggleInput";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool }) => ({
            active: [Bool, false],
        }));
    }
}
//# sourceMappingURL=toggle_input.js.map