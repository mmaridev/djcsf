import { BaseDatePicker, BaseDatePickerView, DateLike } from "./base_date_picker";
import { assert } from "../../core/util/assert";
export class DatePickerView extends BaseDatePickerView {
    static __name__ = "DatePickerView";
    get flatpickr_options() {
        return {
            ...super.flatpickr_options,
            mode: "single",
        };
    }
    _on_change(selected) {
        assert(selected.length <= 1);
        this.model.value = (() => {
            if (selected.length == 0) {
                return null;
            }
            else {
                const [datetime] = selected;
                const date = this._format_date(datetime);
                return date;
            }
        })();
    }
}
export class DatePicker extends BaseDatePicker {
    static __name__ = "DatePicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = DatePickerView;
        this.define(({ Nullable }) => ({
            value: [Nullable(DateLike), null],
        }));
    }
}
//# sourceMappingURL=date_picker.js.map