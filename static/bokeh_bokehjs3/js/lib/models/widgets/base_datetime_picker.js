import { BaseDatePicker, BaseDatePickerView } from "./base_date_picker";
import { Clock } from "../../core/enums";
export class BaseDatetimePickerView extends BaseDatePickerView {
    static __name__ = "BaseDatetimePickerView";
    connect_signals() {
        super.connect_signals();
        const { value, hour_increment, minute_increment, second_increment, seconds, clock, } = this.model.properties;
        this.connect(value.change, () => {
            const { value } = this.model;
            if (value != null) {
                this.picker.setDate(value);
            }
            else {
                this.picker.clear();
            }
        });
        this.connect(hour_increment.change, () => this.picker.set("hourIncrement", this.model.hour_increment));
        this.connect(minute_increment.change, () => this.picker.set("minuteIncrement", this.model.minute_increment));
        this.connect(second_increment.change, () => this._update_second_increment());
        this.connect(seconds.change, () => this.picker.set("enableSeconds", this.model.seconds));
        this.connect(clock.change, () => this.picker.set("time_24hr", this.model.clock == "24h"));
    }
    get flatpickr_options() {
        const { hour_increment, minute_increment, seconds, clock } = this.model;
        const options = super.flatpickr_options;
        options.enableTime = true;
        options.dateFormat = "Y-m-dTH:i:S";
        options.hourIncrement = hour_increment;
        options.minuteIncrement = minute_increment;
        options.enableSeconds = seconds;
        options.time_24hr = clock == "24h";
        return options;
    }
    render() {
        super.render();
        this._update_second_increment();
    }
    _update_second_increment() {
        const { second_increment } = this.model;
        this.picker.secondElement?.setAttribute("step", second_increment.toString());
    }
}
export class BaseDatetimePicker extends BaseDatePicker {
    static __name__ = "BaseDatetimePicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Positive, Int }) => ({
            hour_increment: [Positive(Int), 1],
            minute_increment: [Positive(Int), 1],
            second_increment: [Positive(Int), 1],
            seconds: [Bool, false],
            clock: [Clock, "24h"],
        }));
        this.override({
            date_format: "Y-m-d H:i",
        });
    }
}
//# sourceMappingURL=base_datetime_picker.js.map