import { BaseDatetimePicker, BaseDatetimePickerView } from "./base_datetime_picker";
import { DateLike } from "./base_date_picker";
import { assert } from "../../core/util/assert";
export class DatetimePickerView extends BaseDatetimePickerView {
    static __name__ = "DatetimePickerView";
    get flatpickr_options() {
        return {
            ...super.flatpickr_options,
            mode: "single",
        };
    }
    _on_change(selected) {
        assert(selected.length <= 1);
        this.model.value = (() => {
            if (selected.length == 0) {
                return null;
            }
            else {
                const [datetime] = selected;
                return this._format_date(datetime);
            }
        })();
    }
}
export class DatetimePicker extends BaseDatetimePicker {
    static __name__ = "DatetimePicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = DatetimePickerView;
        this.define(({ Nullable }) => ({
            value: [Nullable(DateLike), null],
        }));
    }
}
//# sourceMappingURL=datetime_picker.js.map