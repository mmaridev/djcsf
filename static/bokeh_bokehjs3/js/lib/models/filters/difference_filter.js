import { CompositeFilter } from "./composite_filter";
export class DifferenceFilter extends CompositeFilter {
    static __name__ = "DifferenceFilter";
    constructor(attrs) {
        super(attrs);
    }
    _inplace_op(index, op) {
        index.subtract(op);
    }
}
//# sourceMappingURL=difference_filter.js.map