import { CompositeFilter } from "./composite_filter";
export class SymmetricDifferenceFilter extends CompositeFilter {
    static __name__ = "SymmetricDifferenceFilter";
    constructor(attrs) {
        super(attrs);
    }
    _inplace_op(index, op) {
        index.symmetric_subtract(op);
    }
}
//# sourceMappingURL=symmetric_difference_filter.js.map