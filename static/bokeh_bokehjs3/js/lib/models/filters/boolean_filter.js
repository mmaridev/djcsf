import { Filter } from "./filter";
import { Indices } from "../../core/types";
export class BooleanFilter extends Filter {
    static __name__ = "BooleanFilter";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Iterable, Nullable }) => ({
            booleans: [Nullable(Iterable(Bool)), null],
        }));
    }
    compute_indices(source) {
        const size = source.get_length() ?? 1;
        const { booleans } = this;
        if (booleans == null) {
            return Indices.all_set(size);
        }
        else {
            return Indices.from_booleans(size, booleans);
        }
    }
}
//# sourceMappingURL=boolean_filter.js.map