import { Filter } from "./filter";
import { Indices } from "../../core/types";
export class IndexFilter extends Filter {
    static __name__ = "IndexFilter";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Int, Iterable, Nullable }) => ({
            indices: [Nullable(Iterable(Int)), null],
        }));
    }
    compute_indices(source) {
        const size = source.get_length() ?? 1;
        const { indices } = this;
        if (indices == null) {
            return Indices.all_set(size);
        }
        else {
            return Indices.from_indices(size, indices);
        }
    }
}
//# sourceMappingURL=index_filter.js.map