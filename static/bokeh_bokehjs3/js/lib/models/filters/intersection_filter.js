import { CompositeFilter } from "./composite_filter";
export class IntersectionFilter extends CompositeFilter {
    static __name__ = "IntersectionFilter";
    constructor(attrs) {
        super(attrs);
    }
    _inplace_op(index, op) {
        index.intersect(op);
    }
}
//# sourceMappingURL=intersection_filter.js.map