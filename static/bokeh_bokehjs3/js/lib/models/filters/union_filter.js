import { CompositeFilter } from "./composite_filter";
export class UnionFilter extends CompositeFilter {
    static __name__ = "UnionFilter";
    constructor(attrs) {
        super(attrs);
    }
    _inplace_op(index, op) {
        index.add(op);
    }
}
//# sourceMappingURL=union_filter.js.map