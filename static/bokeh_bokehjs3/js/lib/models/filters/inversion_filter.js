import { Filter } from "./filter";
export class InversionFilter extends Filter {
    static __name__ = "InversionFilter";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Ref }) => ({
            operand: [Ref(Filter)],
        }));
    }
    connect_signals() {
        super.connect_signals();
        const emit_changed = () => {
            this.change.emit();
        };
        const connect_operands = (operands) => {
            for (const operand of operands) {
                this.connect(operand.change, emit_changed);
            }
        };
        const disconnect_operands = (operands) => {
            for (const operand of operands) {
                this.disconnect(operand.change, emit_changed);
            }
        };
        let operands = (() => {
            const { operand } = this.properties;
            return operand.is_unset ? [] : [operand.get_value()];
        })();
        connect_operands(operands);
        this.on_change(this.properties.operand, () => {
            disconnect_operands(operands);
            operands = [this.operand];
            connect_operands(operands);
        });
    }
    compute_indices(source) {
        const index = this.operand.compute_indices(source);
        index.invert();
        return index;
    }
}
//# sourceMappingURL=inversion_filter.js.map