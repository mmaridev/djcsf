import { Filter } from "./filter";
import { Indices } from "../../core/types";
export class CompositeFilter extends Filter {
    static __name__ = "CompositeFilter";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ List, Ref }) => ({
            operands: [List(Ref(Filter))],
        }));
    }
    connect_signals() {
        super.connect_signals();
        const emit_changed = () => {
            this.change.emit();
        };
        const connect_operands = (operands) => {
            for (const operand of operands) {
                this.connect(operand.change, emit_changed);
            }
        };
        const disconnect_operands = (operands) => {
            for (const operand of operands) {
                this.disconnect(operand.change, emit_changed);
            }
        };
        let operands = (() => {
            const { operands } = this.properties;
            return operands.is_unset ? [] : operands.get_value();
        })();
        connect_operands(operands);
        this.on_change(this.properties.operands, () => {
            disconnect_operands(operands);
            operands = this.operands;
            connect_operands(operands);
        });
    }
    compute_indices(source) {
        const { operands } = this;
        if (operands.length == 0) {
            const size = source.get_length() ?? 1;
            return Indices.all_set(size);
        }
        else {
            const [index, ...rest] = operands.map((op) => op.compute_indices(source));
            for (const op of rest) {
                this._inplace_op(index, op);
            }
            return index;
        }
    }
}
//# sourceMappingURL=composite_filter.js.map