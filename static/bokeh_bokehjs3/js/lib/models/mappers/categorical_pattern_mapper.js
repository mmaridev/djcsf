import { cat_v_compute } from "./categorical_mapper";
import { FactorSeq } from "../ranges/factor_range";
import { Mapper } from "./mapper";
import { HatchPatternType } from "../../core/enums";
export class CategoricalPatternMapper extends Mapper {
    static __name__ = "CategoricalPatternMapper";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float, List, Nullable }) => ({
            factors: [FactorSeq],
            patterns: [List(HatchPatternType)],
            start: [Float, 0],
            end: [Nullable(Float), null],
            default_value: [HatchPatternType, " "],
        }));
    }
    v_compute(xs) {
        const values = new Array(xs.length);
        cat_v_compute(xs, this.factors, this.patterns, values, this.start, this.end, this.default_value);
        return values;
    }
}
//# sourceMappingURL=categorical_pattern_mapper.js.map