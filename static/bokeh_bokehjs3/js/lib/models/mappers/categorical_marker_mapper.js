import { cat_v_compute } from "./categorical_mapper";
import { FactorSeq } from "../ranges/factor_range";
import { Mapper } from "./mapper";
import { MarkerType } from "../../core/enums";
export class CategoricalMarkerMapper extends Mapper {
    static __name__ = "CategoricalMarkerMapper";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float, List, Nullable }) => ({
            factors: [FactorSeq],
            markers: [List(MarkerType)],
            start: [Float, 0],
            end: [Nullable(Float), null],
            default_value: [MarkerType, "circle"],
        }));
    }
    v_compute(xs) {
        const values = new Array(xs.length);
        cat_v_compute(xs, this.factors, this.markers, values, this.start, this.end, this.default_value);
        return values;
    }
}
//# sourceMappingURL=categorical_marker_mapper.js.map