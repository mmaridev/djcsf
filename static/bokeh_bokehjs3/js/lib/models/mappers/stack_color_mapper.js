import { ColorMapper, _convert_palette, _convert_color } from "./color_mapper";
export class StackColorMapper extends ColorMapper {
    static __name__ = "StackColorMapper";
    constructor(attrs) {
        super(attrs);
    }
}
//# sourceMappingURL=stack_color_mapper.js.map