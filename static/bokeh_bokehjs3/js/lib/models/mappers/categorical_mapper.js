import { index_of, find_index } from "../../core/util/arrayable";
import { isArray } from "../../core/util/types";
export function _cat_equals(a, b) {
    if (a.length != b.length) {
        return false;
    }
    const n = a.length;
    for (let i = 0; i < n; i++) {
        if (a[i] !== b[i]) {
            return false;
        }
    }
    return true;
}
export function cat_v_compute(data, factors, targets, values, start, end, extra_value) {
    let i = 0;
    for (const item of data) {
        let key;
        if (!isArray(item)) {
            key = index_of(factors, item);
        }
        else {
            const d = item.slice(start, end ?? undefined);
            if (d.length == 1) {
                key = index_of(factors, d[0]);
            }
            else {
                key = find_index(factors, (x) => _cat_equals(x, d));
            }
        }
        const value = key in targets ? targets[key] : extra_value;
        values[i++] = value;
    }
}
//# sourceMappingURL=categorical_mapper.js.map