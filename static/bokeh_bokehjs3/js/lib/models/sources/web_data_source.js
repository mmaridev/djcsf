import { ColumnDataSource } from "./column_data_source";
import { UpdateMode } from "../../core/enums";
import { execute } from "../../core/util/callbacks";
import { dict } from "../../core/util/object";
export class WebDataSource extends ColumnDataSource {
    static __name__ = "WebDataSource";
    constructor(attrs) {
        super(attrs);
    }
    get_column(name) {
        const data = dict(this.data);
        return data.get(name) ?? [];
    }
    get_length() {
        return super.get_length() ?? 0;
    }
    initialize() {
        super.initialize();
        this.setup();
    }
    async load_data(raw_data, mode, max_size) {
        const { adapter } = this;
        let data;
        if (adapter != null) {
            data = await execute(adapter, this, { response: raw_data });
        }
        else {
            data = raw_data;
        }
        switch (mode) {
            case "replace": {
                break;
            }
            case "append": {
                const old_data = dict(this.data);
                const new_data = dict(data);
                for (const column of this.columns()) {
                    // XXX: support typed arrays
                    const old_col = Array.from(old_data.get(column) ?? []);
                    const new_col = Array.from(new_data.get(column) ?? []);
                    const array = old_col.concat(new_col);
                    new_data.set(column, max_size != null ? array.slice(-max_size) : array);
                }
                break;
            }
        }
        this.data = data;
    }
    static {
        this.define(({ Any, Int, Str, Nullable }) => ({
            max_size: [Nullable(Int), null],
            mode: [UpdateMode, "replace"],
            adapter: [Nullable(Any /*TODO*/), null],
            data_url: [Str],
        }));
    }
}
//# sourceMappingURL=web_data_source.js.map