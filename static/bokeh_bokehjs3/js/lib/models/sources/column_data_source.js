import { ColumnarDataSource } from "./columnar_data_source";
export class ColumnDataSource extends ColumnarDataSource {
    static __name__ = "ColumnDataSource";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Unknown, Dict, Arrayable }) => ({
            data: [Dict(Arrayable(Unknown)), {}],
        }));
    }
}
//# sourceMappingURL=column_data_source.js.map