import { WebDataSource } from "./web_data_source";
export class ServerSentDataSource extends WebDataSource {
    static __name__ = "ServerSentDataSource";
    constructor(attrs) {
        super(attrs);
    }
    initialized = false;
    setup() {
        if (!this.initialized) {
            this.initialized = true;
            const source = new EventSource(this.data_url);
            source.onmessage = async (event) => {
                await this.load_data(JSON.parse(event.data), this.mode, this.max_size ?? undefined);
            };
        }
    }
}
//# sourceMappingURL=server_sent_data_source.js.map