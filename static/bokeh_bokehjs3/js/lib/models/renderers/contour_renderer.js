import { DataRenderer, DataRendererView } from "./data_renderer";
import { GlyphRenderer } from "./glyph_renderer";
import { build_view } from "../../core/build_views";
export class ContourRendererView extends DataRendererView {
    static __name__ = "ContourRendererView";
    fill_view;
    line_view;
    *children() {
        yield* super.children();
        yield this.fill_view;
        yield this.line_view;
    }
    get glyph_view() {
        if (this.fill_view.glyph.data_size > 0) {
            return this.fill_view.glyph;
        }
        else {
            return this.line_view.glyph;
        }
    }
    async lazy_initialize() {
        await super.lazy_initialize();
        const { parent } = this;
        const { fill_renderer, line_renderer } = this.model;
        this.fill_view = await build_view(fill_renderer, { parent });
        this.line_view = await build_view(line_renderer, { parent });
    }
    remove() {
        this.fill_view.remove();
        this.line_view.remove();
        super.remove();
    }
    _paint() {
        this.fill_view.paint();
        this.line_view.paint();
    }
    hit_test(geometry) {
        return this.fill_view.hit_test(geometry);
    }
}
export class ContourRenderer extends DataRenderer {
    static __name__ = "ContourRenderer";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ContourRendererView;
        this.define(({ List, Float, Ref }) => ({
            fill_renderer: [Ref(GlyphRenderer)],
            line_renderer: [Ref(GlyphRenderer)],
            levels: [List(Float), []],
        }));
    }
    get_selection_manager() {
        return this.fill_renderer.data_source.selection_manager;
    }
}
//# sourceMappingURL=contour_renderer.js.map