import { Model } from "../../model";
export class RendererGroup extends Model {
    static __name__ = "RendererGroup";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool }) => ({
            visible: [Bool, true],
        }));
    }
}
//# sourceMappingURL=renderer_group.js.map