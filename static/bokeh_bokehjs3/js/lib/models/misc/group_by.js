import { Model } from "../../model";
import { List, Ref } from "../../core/kinds";
export class GroupBy extends Model {
    static __name__ = "GroupBy";
    constructor(attrs) {
        super(attrs);
    }
}
export class GroupByModels extends GroupBy {
    static __name__ = "GroupByModels";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define({
            groups: [List(List(Ref(Model)))],
        });
    }
    *query_groups(models, _pool) {
        for (const model of models) {
            for (const group of this.groups) {
                if (group.includes(model)) {
                    yield group;
                }
            }
        }
    }
}
export class GroupByName extends GroupBy {
    static __name__ = "GroupByName";
    constructor(attrs) {
        super(attrs);
    }
    *query_groups(models, pool) {
        const groups = new Map();
        for (const model of pool) {
            const { name } = model;
            if (name != null) {
                let group = groups.get(name);
                if (group === undefined) {
                    group = new Set();
                    groups.set(name, group);
                }
                group.add(model);
            }
        }
        for (const model of models) {
            for (const group of groups.values()) {
                if (model.name != null && group.has(model)) {
                    yield [...group];
                }
            }
        }
    }
}
//# sourceMappingURL=group_by.js.map