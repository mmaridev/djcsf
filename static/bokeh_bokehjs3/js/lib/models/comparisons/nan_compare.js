import { Comparison } from "./comparison";
import { isNumber } from "../../core/util/types";
export class NanCompare extends Comparison {
    static __name__ = "NanCompare";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool }) => ({
            ascending_first: [Bool, false],
        }));
    }
    compute(x, y) {
        if (isNumber(x) && isNaN(x)) {
            return this.ascending_first ? -1 : 1;
        }
        if (isNumber(y) && isNaN(y)) {
            return this.ascending_first ? 1 : -1;
        }
        if (isNumber(x) && isNumber(y)) {
            return x == y ? 0 : x < y ? -1 : 1;
        }
        return 0;
    }
}
//# sourceMappingURL=nan_compare.js.map