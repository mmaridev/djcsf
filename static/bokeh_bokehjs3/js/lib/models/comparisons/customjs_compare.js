import { Comparison } from "./comparison";
import { keys, values } from "../../core/util/object";
import { use_strict } from "../../core/util/string";
export class CustomJSCompare extends Comparison {
    static __name__ = "CustomJSCompare";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Unknown, Str, Dict }) => ({
            args: [Dict(Unknown), {}],
            code: [Str, ""],
        }));
    }
    get names() {
        return keys(this.args);
    }
    get values() {
        return values(this.args);
    }
    _make_func() {
        const code = use_strict(this.code);
        return new Function("x", "y", ...this.names, code);
    }
    compute(x, y) {
        const func = this._make_func();
        return func(x, y, this.values);
    }
}
//# sourceMappingURL=customjs_compare.js.map