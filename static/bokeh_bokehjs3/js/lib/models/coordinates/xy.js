import { Coordinate } from "./coordinate";
export class XY extends Coordinate {
    static __name__ = "XY";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float }) => ({
            x: [Float],
            y: [Float],
        }));
    }
}
//# sourceMappingURL=xy.js.map