import { Coordinate } from "./coordinate";
export class Indexed extends Coordinate {
    static __name__ = "Indexed";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Int, AnyRef }) => ({
            index: [Int],
            renderer: [AnyRef()],
        }));
    }
}
//# sourceMappingURL=indexed.js.map