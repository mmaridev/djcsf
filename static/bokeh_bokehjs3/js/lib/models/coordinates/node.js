import { Coordinate } from "./coordinate";
import { Model } from "../../model";
import { Enum, Or, Ref } from "../../core/kinds";
export const ImplicitTarget = Enum("canvas", "plot", "frame", "parent");
export const NodeTarget = Or(Ref(Model), ImplicitTarget);
export class BoxNodes {
    target;
    frozen;
    static __name__ = "BoxNodes";
    constructor(target, frozen = false) {
        this.target = target;
        this.frozen = frozen;
    }
    _node(symbol) {
        const { target, frozen } = this;
        const node = new Node({ target, symbol });
        if (frozen) {
            this[`_${symbol}`] = node;
        }
        return node;
    }
    _left = null;
    get left() {
        return this._left ?? this._node("left");
    }
    _right = null;
    get right() {
        return this._right ?? this._node("right");
    }
    _top = null;
    get top() {
        return this._top ?? this._node("top");
    }
    _bottom = null;
    get bottom() {
        return this._bottom ?? this._node("bottom");
    }
    freeze() {
        return new BoxNodes(this.target, true);
    }
}
export class Node extends Coordinate {
    static __name__ = "Node";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str, Int }) => ({
            target: [NodeTarget],
            symbol: [Str],
            offset: [Int, 0],
        }));
    }
    static _frame_nodes = new BoxNodes("frame");
    static get frame() {
        return this._frame_nodes;
    }
    static _canvas_nodes = new BoxNodes("canvas");
    static get canvas() {
        return this._canvas_nodes;
    }
}
//# sourceMappingURL=node.js.map