import { MenuItem } from "./menu_item";
import { Callback } from "../../callbacks/callback";
import { ToolIcon } from "../../../core/enums";
import { Or, Regex } from "../../../core/kinds";
const IconLike = Or(ToolIcon, Regex(/^--/), Regex(/^\./), Regex(/^data:image/));
export class ActionItem extends MenuItem {
    static __name__ = "ActionItem";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Str, Nullable, AnyRef, Ref, Func }) => ({
            icon: [Nullable(IconLike), null],
            label: [Str],
            tooltip: [Nullable(Str), null],
            shortcut: [Nullable(Str), null],
            menu: [Nullable(AnyRef()), null],
            disabled: [Bool, false],
            action: [Nullable(Or(Ref(Callback), Func())), null],
        }));
    }
}
//# sourceMappingURL=action_item.js.map