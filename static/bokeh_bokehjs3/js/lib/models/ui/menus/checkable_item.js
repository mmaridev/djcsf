import { ActionItem } from "./action_item";
export class CheckableItem extends ActionItem {
    static __name__ = "CheckableItem";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool }) => ({
            checked: [Bool, false],
        }));
    }
}
//# sourceMappingURL=checkable_item.js.map