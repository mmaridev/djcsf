import { Icon, IconView } from "./icon";
import { InlineStyleSheet } from "../../../core/dom";
import { color2css } from "../../../core/util/color";
import { isNumber } from "../../../core/util/types";
import icons_css from "../../../styles/icons.css";
export class BuiltinIconView extends IconView {
    static __name__ = "BuiltinIconView";
    _style = new InlineStyleSheet();
    stylesheets() {
        return [...super.stylesheets(), icons_css, this._style];
    }
    render() {
        super.render();
        const icon = `var(--bokeh-icon-${this.model.icon_name})`;
        const color = color2css(this.model.color);
        const size = (() => {
            const { size } = this.model;
            return isNumber(size) ? `${size}px` : size;
        })();
        this._style.replace(`
      :host {
        display: inline-block;
        vertical-align: middle;
        width: ${size};
        height: ${size};
        background-color: ${color};
        mask-image: ${icon};
        mask-size: contain;
        mask-repeat: no-repeat;
        -webkit-mask-image: ${icon};
        -webkit-mask-size: contain;
        -webkit-mask-repeat: no-repeat;
      }
    `);
    }
}
export class BuiltinIcon extends Icon {
    static __name__ = "BuiltinIcon";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = BuiltinIconView;
        this.define(({ Str, Color }) => ({
            icon_name: [Str],
            color: [Color, "gray"],
        }));
    }
}
//# sourceMappingURL=builtin_icon.js.map