import { Icon, IconView } from "./icon";
import { InlineStyleSheet } from "../../../core/dom";
import { isNumber } from "../../../core/util/types";
export class SVGIconView extends IconView {
    static __name__ = "SVGIconView";
    _style = new InlineStyleSheet();
    stylesheets() {
        return [...super.stylesheets(), this._style];
    }
    render() {
        super.render();
        const size = (() => {
            const { size } = this.model;
            return isNumber(size) ? `${size}px` : size;
        })();
        this._style.replace(`
      :host {
        display: inline-block;
        vertical-align: middle;
      }
      :host svg {
        width: ${size};
        height: ${size};
      }
    `);
        const parser = new DOMParser();
        const doc = parser.parseFromString(this.model.svg, "image/svg+xml");
        this.shadow_el.append(doc.documentElement);
    }
}
export class SVGIcon extends Icon {
    static __name__ = "SVGIcon";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = SVGIconView;
        this.define(({ Str }) => ({
            svg: [Str],
        }));
    }
}
//# sourceMappingURL=svg_icon.js.map