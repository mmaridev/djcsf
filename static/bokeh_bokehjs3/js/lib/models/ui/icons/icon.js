import { UIElement, UIElementView } from "../ui_element";
export class IconView extends UIElementView {
    static __name__ = "IconView";
    connect_signals() {
        super.connect_signals();
        this.connect(this.model.change, () => this.render());
    }
}
export class Icon extends UIElement {
    static __name__ = "Icon";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float, Or, CSSLength }) => ({
            size: [Or(Float, CSSLength), "1em"],
        }));
    }
}
//# sourceMappingURL=icon.js.map