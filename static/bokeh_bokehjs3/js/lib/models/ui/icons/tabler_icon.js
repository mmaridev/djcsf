import { Icon, IconView } from "./icon";
import { span, InlineStyleSheet, ImportedStyleSheet, GlobalInlineStyleSheet } from "../../../core/dom";
import { isNumber } from "../../../core/util/types";
export class TablerIconView extends IconView {
    static __name__ = "TablerIconView";
    static _url = "https://cdn.jsdelivr.net/npm/@tabler/icons-webfont@latest";
    static _fonts = new GlobalInlineStyleSheet(`\
    /*!
    * Tabler Icons 1.68.0 by tabler - https://tabler.io
    * License - https://github.com/tabler/tabler-icons/blob/master/LICENSE
    */
  @font-face {
    font-family: "tabler-icons";
    font-style: normal;
    font-weight: 400;
    src: url("${this._url}/fonts/tabler-icons.eot");
    src: url("${this._url}/fonts/tabler-icons.eot?#iefix") format("embedded-opentype"),
         url("${this._url}/fonts/tabler-icons.woff2") format("woff2"),
         url("${this._url}/fonts/tabler-icons.woff") format("woff"),
         url("${this._url}/fonts/tabler-icons.ttf") format("truetype"),
         url("${this._url}/fonts/tabler-icons.svg#tabler-icons") format("svg");
  }

  @media screen and (-webkit-min-device-pixel-ratio: 0) {
    @font-face {
      font-family: "tabler-icons";
      src: url("${this._url}/fonts/tabler-icons.svg#tabler-icons") format("svg");
    }
  }
`);
    _tabler = new ImportedStyleSheet(`${TablerIconView._url}/tabler-icons.min.css`);
    _style = new InlineStyleSheet();
    stylesheets() {
        return [...super.stylesheets(), TablerIconView._fonts, this._tabler, this._style];
    }
    render() {
        super.render();
        const size = (() => {
            const { size } = this.model;
            return isNumber(size) ? `${size}px` : size;
        })();
        this._style.replace(`
      :host {
        display: inline-block;
        vertical-align: middle;
        font-size: ${size};
      }
    `);
        const icon = span({ class: ["ti", `ti-${this.model.icon_name}`] });
        this.shadow_el.appendChild(icon);
    }
}
export class TablerIcon extends Icon {
    static __name__ = "TablerIcon";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = TablerIconView;
        this.define(({ Str }) => ({
            icon_name: [Str],
        }));
    }
}
//# sourceMappingURL=tabler_icon.js.map