import { XYGlyph, XYGlyphView } from "./xy_glyph";
import * as mixins from "../../core/property_mixins";
import { catmullrom_spline } from "../../core/util/interpolation";
export class SplineView extends XYGlyphView {
    static __name__ = "SplineView";
    _set_data() {
        const { tension, closed } = this.model;
        const [xt, yt] = catmullrom_spline(this.x, this.y, 20, tension, closed);
        this._define_attr("xt", xt);
        this._define_attr("yt", yt);
    }
    _map_data() {
        const { x_scale, y_scale } = this.renderer.coordinates;
        const sxt = x_scale.v_compute(this.xt);
        const syt = y_scale.v_compute(this.yt);
        this._define_attr("sxt", sxt);
        this._define_attr("syt", syt);
    }
    _paint(ctx, _indices, data) {
        const { sxt, syt } = { ...this, ...data };
        let move = true;
        ctx.beginPath();
        const n = sxt.length;
        for (let j = 0; j < n; j++) {
            const sx_i = sxt[j];
            const sy_i = syt[j];
            if (!isFinite(sx_i + sy_i)) {
                move = true;
            }
            else {
                if (move) {
                    ctx.moveTo(sx_i, sy_i);
                    move = false;
                }
                else {
                    ctx.lineTo(sx_i, sy_i);
                }
            }
        }
        this.visuals.line.set_value(ctx);
        ctx.stroke();
    }
}
export class Spline extends XYGlyph {
    static __name__ = "Spline";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = SplineView;
        this.mixins(mixins.LineScalar);
        this.define(({ Bool, Float }) => ({
            tension: [Float, 0.5],
            closed: [Bool, false],
        }));
    }
}
//# sourceMappingURL=spline.js.map