import { MathTextGlyph, MathTextGlyphView } from "./math_text_glyph";
import { MathML } from "../text/math_text";
export class MathMLGlyphView extends MathTextGlyphView {
    static __name__ = "MathMLGlyphView";
    _build_label(text) {
        return new MathML({ text });
    }
}
export class MathMLGlyph extends MathTextGlyph {
    static __name__ = "MathMLGlyph";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = MathMLGlyphView;
    }
}
//# sourceMappingURL=mathml_glyph.js.map