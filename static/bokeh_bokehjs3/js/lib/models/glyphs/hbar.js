import { LRTB, LRTBView } from "./lrtb";
import { ScreenArray } from "../../core/types";
import * as p from "../../core/properties";
export class HBarView extends LRTBView {
    static __name__ = "HBarView";
    scenterxy(i) {
        const scx = (this.sleft[i] + this.sright[i]) / 2;
        const scy = this.sy[i];
        return [scx, scy];
    }
    _lrtb(i) {
        const left_i = this.left[i];
        const right_i = this.right[i];
        const y_i = this.y[i];
        const half_height_i = this.height.get(i) / 2;
        const l = Math.min(left_i, right_i);
        const r = Math.max(left_i, right_i);
        const t = y_i + half_height_i;
        const b = y_i - half_height_i;
        return { l, r, t, b };
    }
    _map_data() {
        if (this.inherited_y && this.inherited_height) {
            this._inherit_attr("sheight");
            this._inherit_attr("stop");
            this._inherit_attr("sbottom");
        }
        else {
            const sheight = this.sdist(this.renderer.yscale, this.y, this.height, "center");
            const { sy } = this;
            const n = this.sy.length;
            const stop = new ScreenArray(n);
            const sbottom = new ScreenArray(n);
            for (let i = 0; i < n; i++) {
                const sy_i = sy[i];
                const sheight_i = sheight[i];
                stop[i] = sy_i - sheight_i / 2;
                sbottom[i] = sy_i + sheight_i / 2;
            }
            this._define_attr("sheight", sheight);
            this._define_attr("stop", stop);
            this._define_attr("sbottom", sbottom);
        }
        this._clamp_to_viewport();
    }
}
export class HBar extends LRTB {
    static __name__ = "HBar";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = HBarView;
        this.define(({}) => ({
            left: [p.XCoordinateSpec, { value: 0 }],
            y: [p.YCoordinateSpec, { field: "y" }],
            height: [p.DistanceSpec, { value: 1 }],
            right: [p.XCoordinateSpec, { field: "right" }],
        }));
    }
}
//# sourceMappingURL=hbar.js.map