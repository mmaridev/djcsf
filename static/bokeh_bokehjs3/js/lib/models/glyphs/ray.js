import { XYGlyph, XYGlyphView } from "./xy_glyph";
import { inherit } from "./glyph";
import { generic_line_vector_legend } from "./utils";
import { LineVector } from "../../core/property_mixins";
import { to_screen } from "../../core/types";
import * as p from "../../core/properties";
export class RayView extends XYGlyphView {
    static __name__ = "RayView";
    _map_data() {
        this._define_or_inherit_attr("slength", () => {
            if (this.model.properties.length.units == "data") {
                if (this.inherited_x && this.inherited_length) {
                    return inherit;
                }
                else {
                    return this.sdist(this.renderer.xscale, this.x, this.length);
                }
            }
            else {
                return this.inherited_length ? inherit : to_screen(this.length);
            }
        });
        if (!this.inherited_slength) {
            const { width, height } = this.renderer.plot_view.frame.bbox;
            const inf_len = 2 * (width + height);
            const { slength } = this;
            const n = slength.length;
            for (let i = 0; i < n; i++) {
                if (slength[i] == 0) {
                    slength[i] = inf_len;
                }
            }
        }
    }
    _paint(ctx, indices, data) {
        if (!this.visuals.line.doit) {
            return;
        }
        const { sx, sy, slength, angle } = { ...this, ...data };
        for (const i of indices) {
            const sx_i = sx[i];
            const sy_i = sy[i];
            const angle_i = angle.get(i);
            const slength_i = slength[i];
            if (!isFinite(sx_i + sy_i + angle_i + slength_i)) {
                continue;
            }
            ctx.translate(sx_i, sy_i);
            ctx.rotate(angle_i);
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.lineTo(slength_i, 0);
            this.visuals.line.apply(ctx, i);
            ctx.rotate(-angle_i);
            ctx.translate(-sx_i, -sy_i);
        }
    }
    draw_legend_for_index(ctx, bbox, index) {
        generic_line_vector_legend(this.visuals, ctx, bbox, index);
    }
}
export class Ray extends XYGlyph {
    static __name__ = "Ray";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = RayView;
        this.mixins(LineVector);
        this.define(({}) => ({
            length: [p.DistanceSpec, 0],
            angle: [p.AngleSpec, 0],
        }));
    }
}
//# sourceMappingURL=ray.js.map