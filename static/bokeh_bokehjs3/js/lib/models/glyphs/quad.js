import { LRTB, LRTBView } from "./lrtb";
import * as p from "../../core/properties";
export class QuadView extends LRTBView {
    static __name__ = "QuadView";
    scenterxy(i) {
        const scx = this.sleft[i] / 2 + this.sright[i] / 2;
        const scy = this.stop[i] / 2 + this.sbottom[i] / 2;
        return [scx, scy];
    }
    _lrtb(i) {
        const l = this.left[i];
        const r = this.right[i];
        const t = this.top[i];
        const b = this.bottom[i];
        return { l, r, t, b };
    }
}
export class Quad extends LRTB {
    static __name__ = "Quad";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = QuadView;
        this.define(({}) => ({
            right: [p.XCoordinateSpec, { field: "right" }],
            bottom: [p.YCoordinateSpec, { field: "bottom" }],
            left: [p.XCoordinateSpec, { field: "left" }],
            top: [p.YCoordinateSpec, { field: "top" }],
        }));
    }
}
//# sourceMappingURL=quad.js.map