import * as p from "../../core/properties";
import { Glyph, GlyphView } from "./glyph";
export class XYGlyphView extends GlyphView {
    static __name__ = "XYGlyphView";
    _project_data() {
        this._project_xy("x", this.x, "y", this.y);
    }
    _index_data(index) {
        const { x, y, data_size } = this;
        for (let i = 0; i < data_size; i++) {
            const x_i = x[i];
            const y_i = y[i];
            index.add_point(x_i, y_i);
        }
    }
    scenterxy(i) {
        return [this.sx[i], this.sy[i]];
    }
}
export class XYGlyph extends Glyph {
    static __name__ = "XYGlyph";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(() => ({
            x: [p.XCoordinateSpec, { field: "x" }],
            y: [p.YCoordinateSpec, { field: "y" }],
        }));
    }
}
//# sourceMappingURL=xy_glyph.js.map