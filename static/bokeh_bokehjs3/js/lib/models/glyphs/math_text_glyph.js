import { Text, TextView } from "./text";
import { MathTextView } from "../text/math_text";
import { build_views, remove_views } from "../../core/build_views";
import { enumerate } from "../../core/util/iterator";
export class MathTextGlyphView extends TextView {
    static __name__ = "MathTextGlyphView";
    _label_views = new Map();
    remove() {
        remove_views(this._label_views);
        super.remove();
    }
    *children() {
        yield* super.children();
        yield* this._label_views.values();
    }
    has_finished() {
        if (!super.has_finished()) {
            return false;
        }
        for (const view of this._label_views.values()) {
            if (!view.has_finished()) {
                return false;
            }
        }
        return true;
    }
    async _build_labels(text) {
        const labels = Array.from(text, (text_i) => {
            return text_i == null ? null : this._build_label(text_i);
        });
        await build_views(this._label_views, labels.filter((v) => v != null), { parent: this.renderer });
        return labels.map((label_i) => {
            return label_i == null ? null : this._label_views.get(label_i).graphics();
        });
    }
    async after_lazy_visuals() {
        await super.after_lazy_visuals();
        const promises = [...this._label_views.values()].map((label_view) => {
            if (label_view instanceof MathTextView) {
                return label_view.request_image();
            }
            else {
                return null;
            }
        });
        await Promise.allSettled(promises);
        const { left, right, top, bottom } = this.padding;
        for (const [label, i] of enumerate(this.labels)) {
            if (label == null) {
                continue;
            }
            if (!(label instanceof MathTextView)) {
                continue;
            }
            const size = label.size();
            const width = left + size.width + right;
            const height = top + size.height + bottom;
            this.swidth[i] = width;
            this.sheight[i] = height;
        }
    }
}
export class MathTextGlyph extends Text {
    static __name__ = "MathTextGlyph";
    constructor(attrs) {
        super(attrs);
    }
}
//# sourceMappingURL=math_text_glyph.js.map