import { MathTextGlyph, MathTextGlyphView } from "./math_text_glyph";
import { TeX } from "../text/math_text";
import { Enum, Or, Auto } from "../../core/kinds";
import { parse_delimited_string } from "../text/utils";
export const DisplayMode = Or(Enum("inline", "block"), Auto);
export class TeXGlyphView extends MathTextGlyphView {
    static __name__ = "TeXGlyphView";
    _build_label(text) {
        const { macros, display } = this.model;
        if (display == "auto") {
            const obj = parse_delimited_string(text);
            if (obj instanceof TeX) {
                obj.macros = macros;
            }
            return obj;
        }
        else {
            return new TeX({ text, macros, inline: display == "inline" });
        }
    }
}
export class TeXGlyph extends MathTextGlyph {
    static __name__ = "TeXGlyph";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = TeXGlyphView;
        this.define(({ Float, Str, Dict, Tuple, Or }) => ({
            macros: [Dict(Or(Str, Tuple(Str, Float))), {}],
            display: [DisplayMode, "auto"],
        }));
    }
}
//# sourceMappingURL=tex_glyph.js.map