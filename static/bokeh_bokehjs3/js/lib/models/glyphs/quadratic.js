import { LineVector } from "../../core/property_mixins";
import { Glyph, GlyphView } from "./glyph";
import { generic_line_vector_legend } from "./utils";
import { qbb } from "../../core/util/algorithms";
import * as p from "../../core/properties";
export class QuadraticView extends GlyphView {
    static __name__ = "QuadraticView";
    _project_data() {
        this._project_xy("x0", this.x0, "y0", this.y0);
        this._project_xy("x1", this.x1, "y1", this.y1);
    }
    _index_data(index) {
        const { x0, x1, y0, y1, cx, cy, data_size } = this;
        for (let i = 0; i < data_size; i++) {
            const x0_i = x0[i];
            const x1_i = x1[i];
            const y0_i = y0[i];
            const y1_i = y1[i];
            const cx_i = cx[i];
            const cy_i = cy[i];
            if (!isFinite(x0_i + x1_i + y0_i + y1_i + cx_i + cy_i)) {
                index.add_empty();
            }
            else {
                const { x0, y0, x1, y1 } = qbb(x0_i, y0_i, cx_i, cy_i, x1_i, y1_i);
                index.add_rect(x0, y0, x1, y1);
            }
        }
    }
    _paint(ctx, indices, data) {
        if (!this.visuals.line.doit) {
            return;
        }
        const { sx0, sy0, sx1, sy1, scx, scy } = { ...this, ...data };
        for (const i of indices) {
            const sx0_i = sx0[i];
            const sy0_i = sy0[i];
            const sx1_i = sx1[i];
            const sy1_i = sy1[i];
            const scx_i = scx[i];
            const scy_i = scy[i];
            if (!isFinite(sx0_i + sy0_i + sx1_i + sy1_i + scx_i + scy_i)) {
                continue;
            }
            ctx.beginPath();
            ctx.moveTo(sx0_i, sy0_i);
            ctx.quadraticCurveTo(scx_i, scy_i, sx1_i, sy1_i);
            this.visuals.line.apply(ctx, i);
        }
    }
    draw_legend_for_index(ctx, bbox, index) {
        generic_line_vector_legend(this.visuals, ctx, bbox, index);
    }
    scenterxy() {
        throw new Error(`${this}.scenterxy() is not implemented`);
    }
}
export class Quadratic extends Glyph {
    static __name__ = "Quadratic";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = QuadraticView;
        this.define(({}) => ({
            x0: [p.XCoordinateSpec, { field: "x0" }],
            y0: [p.YCoordinateSpec, { field: "y0" }],
            x1: [p.XCoordinateSpec, { field: "x1" }],
            y1: [p.YCoordinateSpec, { field: "y1" }],
            cx: [p.XCoordinateSpec, { field: "cx" }],
            cy: [p.YCoordinateSpec, { field: "cy" }],
        }));
        this.mixins(LineVector);
    }
}
//# sourceMappingURL=quadratic.js.map