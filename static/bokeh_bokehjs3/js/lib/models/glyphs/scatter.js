import { Marker, MarkerView } from "./marker";
import { marker_funcs } from "./defs";
import * as p from "../../core/properties";
export class ScatterView extends MarkerView {
    static __name__ = "ScatterView";
    async load_glglyph() {
        const { MultiMarkerGL } = await import("./webgl/multi_marker");
        return MultiMarkerGL;
    }
    _paint(ctx, indices, data) {
        const { sx, sy, size, angle, marker } = { ...this, ...data };
        for (const i of indices) {
            const sx_i = sx[i];
            const sy_i = sy[i];
            const size_i = size.get(i);
            const angle_i = angle.get(i);
            const marker_i = marker.get(i);
            if (!isFinite(sx_i + sy_i + size_i + angle_i) || marker_i == null) {
                continue;
            }
            const r = size_i / 2;
            ctx.beginPath();
            ctx.translate(sx_i, sy_i);
            if (angle_i != 0) {
                ctx.rotate(angle_i);
            }
            marker_funcs[marker_i](ctx, i, r, this.visuals);
            if (angle_i != 0) {
                ctx.rotate(-angle_i);
            }
            ctx.translate(-sx_i, -sy_i);
        }
    }
    draw_legend_for_index(ctx, { x0, x1, y0, y1 }, index) {
        const n = index + 1;
        const marker = this.marker.get(index);
        const args = {
            ...this._get_legend_args({ x0, x1, y0, y1 }, index),
            marker: new p.UniformScalar(marker, n),
        };
        this._paint(ctx, [index], args);
    }
}
export class Scatter extends Marker {
    static __name__ = "Scatter";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ScatterView;
        this.define(() => ({
            marker: [p.MarkerSpec, { value: "circle" }],
        }));
    }
}
//# sourceMappingURL=scatter.js.map