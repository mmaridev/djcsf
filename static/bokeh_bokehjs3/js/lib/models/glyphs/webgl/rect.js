import { SXSYGlyphGL } from "./sxsy";
export class RectGL extends SXSYGlyphGL {
    glyph;
    static __name__ = "RectGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    get marker_type() {
        return this._border_radius_nonzero ? "round_rect" : "rect";
    }
    _set_data() {
        super._set_data();
        this._widths.set_from_array(this.glyph.swidth);
        this._heights.set_from_array(this.glyph.sheight);
        this._angles.set_from_prop(this.glyph.angle);
        const { top_left, top_right, bottom_right, bottom_left } = this.glyph.border_radius;
        this._border_radius = [top_left, top_right, bottom_right, bottom_left];
        this._border_radius_nonzero = Math.max(...this._border_radius) > 0.0;
    }
    _set_once() {
        super._set_once();
        this._auxs.set_from_scalar(0);
    }
}
//# sourceMappingURL=rect.js.map