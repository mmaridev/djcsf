export class BaseGLGlyph {
    regl_wrapper;
    glyph;
    static __name__ = "BaseGLGlyph";
    nvertices = 0;
    size_changed = false;
    data_changed = false;
    data_mapped = false;
    visuals_changed = false;
    constructor(regl_wrapper, glyph) {
        this.regl_wrapper = regl_wrapper;
        this.glyph = glyph;
    }
    set_data_changed() {
        const { data_size } = this.glyph;
        if (data_size != this.nvertices) {
            this.nvertices = data_size;
            this.size_changed = true;
        }
        this.data_changed = true;
    }
    set_data_mapped() {
        this.data_mapped = true;
    }
    set_visuals_changed() {
        this.visuals_changed = true;
    }
    render(_ctx, indices, mainglyph) {
        if (indices.length == 0) {
            return;
        }
        const { width, height } = this.glyph.renderer.plot_view.canvas_view.webgl.canvas;
        const { pixel_ratio } = this.glyph.renderer.plot_view.canvas_view;
        const trans = {
            pixel_ratio, // Needed to scale antialiasing
            width: width / pixel_ratio,
            height: height / pixel_ratio,
        };
        this.draw(indices, mainglyph, trans);
    }
}
//# sourceMappingURL=base.js.map