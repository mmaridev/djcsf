import { SingleMarkerGL } from "./single_marker";
import { interleave } from "./webgl_utils";
export class SXSYGlyphGL extends SingleMarkerGL {
    glyph;
    static __name__ = "SXSYGlyphGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    _set_data() {
        const nmarkers = this.nvertices;
        const centers_array = this._centers.get_sized_array(2 * nmarkers);
        interleave(this.glyph.sx, this.glyph.sy, nmarkers, SingleMarkerGL.missing_point, centers_array);
        this._centers.update();
    }
}
//# sourceMappingURL=sxsy.js.map