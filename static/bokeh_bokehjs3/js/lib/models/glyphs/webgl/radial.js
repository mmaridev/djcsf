import { SXSYGlyphGL } from "./sxsy";
import { mul } from "../../../core/util/arrayable";
export class RadialGL extends SXSYGlyphGL {
    glyph;
    static __name__ = "RadialGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    // TODO: should be 'radius'
    get size() {
        return this._widths;
    }
    _set_data() {
        super._set_data();
        // Ideally we wouldn't multiply here, but currently handling of
        // circle glyph and scatter with circle marker is handled with
        // a single code path.
        this.size.set_from_array(mul(this.glyph.sradius, 2.0));
    }
    _set_once() {
        super._set_once();
        this._heights.set_from_scalar(0);
    }
}
//# sourceMappingURL=radial.js.map