import { SXSYGlyphGL } from "./sxsy";
export class AnnularWedgeGL extends SXSYGlyphGL {
    glyph;
    static __name__ = "AnnularWedgeGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    get marker_type() {
        return "annular_wedge";
    }
    get outer_radius() {
        return this._widths;
    }
    get inner_radius() {
        return this._heights;
    }
    get start_angle() {
        return this._angles;
    }
    get end_angle() {
        return this._auxs;
    }
    _set_data() {
        super._set_data();
        this.outer_radius.set_from_array(this.glyph.souter_radius);
        this.inner_radius.set_from_array(this.glyph.sinner_radius);
        if (this.glyph.model.direction == "anticlock") {
            this.start_angle.set_from_prop(this.glyph.start_angle);
            this.end_angle.set_from_prop(this.glyph.end_angle);
        }
        else {
            this.start_angle.set_from_prop(this.glyph.end_angle);
            this.end_angle.set_from_prop(this.glyph.start_angle);
        }
    }
}
//# sourceMappingURL=annular_wedge.js.map