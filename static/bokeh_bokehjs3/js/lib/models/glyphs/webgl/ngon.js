import { RadialGL } from "./radial";
export class NgonGL extends RadialGL {
    glyph;
    static __name__ = "NgonGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    get marker_type() {
        return "ngon";
    }
    _set_data() {
        super._set_data();
        this._angles.set_from_prop(this.glyph.angle);
        this._auxs.set_from_prop(this.glyph.n);
    }
}
//# sourceMappingURL=ngon.js.map