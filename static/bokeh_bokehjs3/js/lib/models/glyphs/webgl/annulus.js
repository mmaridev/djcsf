import { SXSYGlyphGL } from "./sxsy";
export class AnnulusGL extends SXSYGlyphGL {
    glyph;
    static __name__ = "AnnulusGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    get marker_type() {
        return "annulus";
    }
    get outer_radius() {
        return this._widths;
    }
    get inner_radius() {
        return this._heights;
    }
    _set_data() {
        super._set_data();
        this.outer_radius.set_from_array(this.glyph.souter_radius);
        this.inner_radius.set_from_array(this.glyph.sinner_radius);
    }
    _set_once() {
        super._set_once();
        this._angles.set_from_scalar(0);
        this._auxs.set_from_scalar(0);
    }
}
//# sourceMappingURL=annulus.js.map