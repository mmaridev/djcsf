import { SXSYGlyphGL } from "./sxsy";
export class WedgeGL extends SXSYGlyphGL {
    glyph;
    static __name__ = "WedgeGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    get marker_type() {
        return "wedge";
    }
    get radius() {
        return this._widths;
    }
    get start_angle() {
        return this._angles;
    }
    get end_angle() {
        return this._auxs;
    }
    _set_data() {
        super._set_data();
        this.radius.set_from_array(this.glyph.sradius);
        if (this.glyph.model.direction == "anticlock") {
            this.start_angle.set_from_prop(this.glyph.start_angle);
            this.end_angle.set_from_prop(this.glyph.end_angle);
        }
        else {
            this.start_angle.set_from_prop(this.glyph.end_angle);
            this.end_angle.set_from_prop(this.glyph.start_angle);
        }
    }
    _set_once() {
        super._set_once();
        this._heights.set_from_scalar(0);
    }
}
//# sourceMappingURL=wedge.js.map