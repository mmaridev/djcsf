import { SXSYGlyphGL } from "./sxsy";
export class HexTileGL extends SXSYGlyphGL {
    glyph;
    static __name__ = "HexTileGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    get marker_type() {
        return "hex_tile";
    }
    _set_data() {
        super._set_data();
        if (this.glyph.model.orientation == "pointytop") {
            this._angles.set_from_scalar(0.5 * Math.PI);
            this._widths.set_from_scalar(this.glyph.svy[0] * 2);
            this._heights.set_from_scalar(this.glyph.svx[4] * 4 / Math.sqrt(3));
        }
        else {
            this._angles.set_from_scalar(0);
            this._widths.set_from_scalar(this.glyph.svx[0] * 2);
            this._heights.set_from_scalar(this.glyph.svy[4] * 4 / Math.sqrt(3));
        }
    }
    _set_once() {
        super._set_once();
        this._auxs.set_from_scalar(0);
    }
}
//# sourceMappingURL=hex_tile.js.map