import { RadialGL } from "./radial";
export class CircleGL extends RadialGL {
    glyph;
    static __name__ = "CircleGL";
    constructor(regl_wrapper, glyph) {
        super(regl_wrapper, glyph);
        this.glyph = glyph;
    }
    get marker_type() {
        return "circle";
    }
    _set_once() {
        super._set_once();
        this._angles.set_from_scalar(0);
    }
}
//# sourceMappingURL=circle.js.map