import { ImageBase, ImageBaseView } from "./image_base";
import { isTypedArray } from "../../core/util/types";
export class ImageRGBAView extends ImageBaseView {
    static __name__ = "ImageRGBAView";
    async load_glglyph() {
        const { ImageGL } = await import("./webgl/image");
        return ImageGL;
    }
    _flat_img_to_buf8(img) {
        const array = isTypedArray(img) ? img : new Uint32Array(img);
        return new Uint8ClampedArray(array.buffer);
    }
}
export class ImageRGBA extends ImageBase {
    static __name__ = "ImageRGBA";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ImageRGBAView;
    }
}
//# sourceMappingURL=image_rgba.js.map