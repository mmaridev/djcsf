import { LRTB, LRTBView } from "./lrtb";
import { minmax } from "../../core/util/math";
import { ScreenArray } from "../../core/types";
import * as p from "../../core/properties";
export class BlockView extends LRTBView {
    static __name__ = "BlockView";
    scenterxy(i) {
        const scx = this.sleft[i] / 2 + this.sright[i] / 2;
        const scy = this.stop[i] / 2 + this.sbottom[i] / 2;
        return [scx, scy];
    }
    _lrtb(i) {
        const x_i = this.x[i];
        const y_i = this.y[i];
        const width_i = this.width.get(i);
        const height_i = this.height.get(i);
        const [l, r] = minmax(x_i, x_i + width_i);
        const [b, t] = minmax(y_i, y_i + height_i);
        return { l, r, t, b };
    }
    _map_data() {
        const { sx, sy } = this;
        const n = sx.length;
        if (this.inherited_x && this.inherited_width) {
            this._inherit_attr("sleft");
            this._inherit_attr("sright");
        }
        else {
            const sw = this.sdist(this.renderer.xscale, this.x, this.width, "edge");
            const sleft = new ScreenArray(n);
            const sright = new ScreenArray(n);
            for (let i = 0; i < n; i++) {
                sleft[i] = sx[i];
                sright[i] = sx[i] + sw[i];
            }
            this._define_attr("sleft", sleft);
            this._define_attr("sright", sright);
        }
        if (this.inherited_y && this.inherited_height) {
            this._inherit_attr("stop");
            this._inherit_attr("sbottom");
        }
        else {
            const sh = this.sdist(this.renderer.yscale, this.y, this.height, "edge");
            const stop = new ScreenArray(n);
            const sbottom = new ScreenArray(n);
            for (let i = 0; i < n; i++) {
                stop[i] = sy[i] - sh[i];
                sbottom[i] = sy[i];
            }
            this._define_attr("stop", stop);
            this._define_attr("sbottom", sbottom);
        }
        this._clamp_to_viewport();
    }
}
export class Block extends LRTB {
    static __name__ = "Block";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = BlockView;
        this.define(({}) => ({
            x: [p.XCoordinateSpec, { field: "x" }],
            y: [p.YCoordinateSpec, { field: "y" }],
            width: [p.DistanceSpec, { value: 1 }],
            height: [p.DistanceSpec, { value: 1 }],
        }));
    }
}
//# sourceMappingURL=block.js.map