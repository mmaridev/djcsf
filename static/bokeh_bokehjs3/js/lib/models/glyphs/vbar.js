import { LRTB, LRTBView } from "./lrtb";
import { ScreenArray } from "../../core/types";
import * as p from "../../core/properties";
export class VBarView extends LRTBView {
    static __name__ = "VBarView";
    scenterxy(i) {
        const scx = this.sx[i];
        const scy = (this.stop[i] + this.sbottom[i]) / 2;
        return [scx, scy];
    }
    _lrtb(i) {
        const half_width_i = this.width.get(i) / 2;
        const x_i = this.x[i];
        const top_i = this.top[i];
        const bottom_i = this.bottom[i];
        const l = x_i - half_width_i;
        const r = x_i + half_width_i;
        const t = Math.max(top_i, bottom_i);
        const b = Math.min(top_i, bottom_i);
        return { l, r, t, b };
    }
    _map_data() {
        if (this.inherited_x && this.inherited_width) {
            this._inherit_attr("swidth");
            this._inherit_attr("sleft");
            this._inherit_attr("sright");
        }
        else {
            const swidth = this.sdist(this.renderer.xscale, this.x, this.width, "center");
            const { sx } = this;
            const n = sx.length;
            const sleft = new ScreenArray(n);
            const sright = new ScreenArray(n);
            for (let i = 0; i < n; i++) {
                const sx_i = sx[i];
                const swidth_i = swidth[i];
                sleft[i] = sx_i - swidth_i / 2;
                sright[i] = sx_i + swidth_i / 2;
            }
            this._define_attr("swidth", swidth);
            this._define_attr("sleft", sleft);
            this._define_attr("sright", sright);
        }
        this._clamp_to_viewport();
    }
}
export class VBar extends LRTB {
    static __name__ = "VBar";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = VBarView;
        this.define(({}) => ({
            x: [p.XCoordinateSpec, { field: "x" }],
            bottom: [p.YCoordinateSpec, { value: 0 }],
            width: [p.DistanceSpec, { value: 1 }],
            top: [p.YCoordinateSpec, { field: "top" }],
        }));
    }
}
//# sourceMappingURL=vbar.js.map