import { Tooltip } from "../ui/tooltip";
import { Model } from "../../model";
import { UIElement } from "../ui/ui_element";
export class TabPanel extends Model {
    static __name__ = "TabPanel";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Str, Ref, Nullable }) => ({
            title: [Str, ""],
            tooltip: [Nullable(Ref(Tooltip)), null],
            child: [Ref(UIElement)],
            closable: [Bool, false],
            disabled: [Bool, false],
        }));
    }
}
//# sourceMappingURL=tab_panel.js.map