import { CSSGridBox, CSSGridBoxView } from "./css_grid_box";
import { TracksSizing, GridChild } from "../common/kinds";
import { UIElement } from "../ui/ui_element";
export class GridBoxView extends CSSGridBoxView {
    static __name__ = "GridBoxView";
    connect_signals() {
        super.connect_signals();
        const { children, rows, cols } = this.model.properties;
        this.on_change(children, () => this.update_children());
        this.on_change([rows, cols], () => this.invalidate_layout());
    }
    get _children() {
        return this.model.children;
    }
    get _rows() {
        return this.model.rows;
    }
    get _cols() {
        return this.model.cols;
    }
}
export class GridBox extends CSSGridBox {
    static __name__ = "GridBox";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = GridBoxView;
        this.define(({ List, Nullable }) => {
            return {
                children: [List(GridChild(UIElement)), []],
                rows: [Nullable(TracksSizing), null],
                cols: [Nullable(TracksSizing), null],
            };
        });
    }
}
//# sourceMappingURL=grid_box.js.map