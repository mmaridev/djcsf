import { CSSGridBox, CSSGridBoxView } from "./css_grid_box";
import { TracksSizing, Index, Span } from "../common/kinds";
import { UIElement } from "../ui/ui_element";
import { Struct, Ref, Opt } from "../../core/kinds";
const VBoxChild = Struct({ child: Ref(UIElement), row: Opt(Index), span: Opt(Span) });
export class VBoxView extends CSSGridBoxView {
    static __name__ = "VBoxView";
    connect_signals() {
        super.connect_signals();
        const { children, rows } = this.model.properties;
        this.on_change(children, () => this.update_children());
        this.on_change(rows, () => this.invalidate_layout());
    }
    get _children() {
        return this.model.children.map(({ child, row, span }, i) => [child, row ?? i, 0, span ?? 1, 1]);
    }
    get _rows() {
        return this.model.rows;
    }
    get _cols() {
        return null;
    }
}
export class VBox extends CSSGridBox {
    static __name__ = "VBox";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = VBoxView;
        this.define(({ List, Nullable }) => ({
            children: [List(VBoxChild), []],
            rows: [Nullable(TracksSizing), null],
        }));
    }
}
//# sourceMappingURL=vbox.js.map