import { Model } from "../../model";
export class Selector extends Model {
    static __name__ = "Selector";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str }) => ({
            query: [Str],
        }));
    }
}
//# sourceMappingURL=selector.js.map