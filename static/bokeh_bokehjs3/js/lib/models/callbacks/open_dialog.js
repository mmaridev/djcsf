import { Callback } from "./callback";
import { Dialog } from "../ui/dialog";
export class OpenDialog extends Callback {
    static __name__ = "OpenDialog";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Ref }) => ({
            dialog: [Ref(Dialog)],
        }));
    }
    async execute() {
        const { dialog } = this;
        const views = dialog.document?.views_manager;
        if (views != null) {
            let dialog_view = views.find_one(dialog);
            if (dialog_view == null) {
                dialog_view = await views.build_view(dialog);
            }
            dialog_view.open();
        }
    }
}
//# sourceMappingURL=open_dialog.js.map