import { Callback } from "./callback";
import { UIElement } from "../ui/ui_element";
export class ToggleVisibility extends Callback {
    static __name__ = "ToggleVisibility";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Ref }) => ({
            target: [Ref(UIElement)],
        }));
    }
    execute() {
        const { target } = this;
        target.visible = !target.visible;
    }
}
//# sourceMappingURL=toggle_visibility.js.map