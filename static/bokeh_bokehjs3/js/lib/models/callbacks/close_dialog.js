import { Callback } from "./callback";
import { Dialog } from "../ui/dialog";
export class CloseDialog extends Callback {
    static __name__ = "CloseDialog";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Ref }) => ({
            dialog: [Ref(Dialog)],
        }));
    }
    async execute() {
        const { dialog } = this;
        dialog.document?.views_manager?.find_one(dialog)?.close();
    }
}
//# sourceMappingURL=close_dialog.js.map