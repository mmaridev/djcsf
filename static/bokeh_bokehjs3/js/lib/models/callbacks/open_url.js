import { Callback } from "./callback";
import { replace_placeholders } from "../../core/util/templating";
import { isString } from "../../core/util/types";
export class OpenURL extends Callback {
    static __name__ = "OpenURL";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool, Str }) => ({
            url: [Str, "http://"],
            same_tab: [Bool, false],
        }));
    }
    navigate(url) {
        if (this.same_tab) {
            window.location.href = url;
        }
        else {
            window.open(url);
        }
    }
    execute(_cb_obj, { source }) {
        const open_url = (i) => {
            const url = replace_placeholders(this.url, source, i, undefined, undefined, encodeURI);
            if (!isString(url)) {
                throw new Error("HTML output is not supported in this context");
            }
            this.navigate(url);
        };
        const { selected } = source;
        for (const i of selected.indices) {
            open_url(i);
        }
        for (const i of selected.line_indices) {
            open_url(i);
        }
        // TODO: multiline_indices: {[key: string]: number[]}
    }
}
//# sourceMappingURL=open_url.js.map