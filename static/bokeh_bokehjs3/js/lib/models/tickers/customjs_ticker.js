import { Ticker } from "./ticker";
import { FactorRange } from "../ranges/factor_range";
import { keys, values } from "../../core/util/object";
import { use_strict } from "../../core/util/string";
export class CustomJSTicker extends Ticker {
    static __name__ = "CustomJSTicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Unknown, Str, Dict }) => ({
            args: [Dict(Unknown), {}],
            major_code: [Str, ""],
            minor_code: [Str, ""],
        }));
    }
    get names() {
        return keys(this.args);
    }
    get values() {
        return values(this.args);
    }
    get_ticks(start, end, range, cross_loc) {
        const major_cb_data = { start, end, range, cross_loc };
        const major_ticks = this.major_ticks(major_cb_data);
        // CustomJSTicker for categorical axes only support a single level of major ticks
        if (range instanceof FactorRange) {
            return { major: major_ticks, minor: [], tops: [], mids: [] };
        }
        const minor_cb_data = { major_ticks, ...major_cb_data };
        const minor_ticks = this.minor_ticks(minor_cb_data);
        return {
            major: major_ticks,
            minor: minor_ticks,
        };
    }
    major_ticks(cb_data) {
        if (this.major_code == "") {
            return [];
        }
        const code = use_strict(this.major_code);
        const func = new Function("cb_data", ...this.names, code);
        return func(cb_data, ...this.values);
    }
    minor_ticks(cb_data) {
        if (this.minor_code == "") {
            return [];
        }
        const code = use_strict(this.minor_code);
        const func = new Function("cb_data", ...this.names, code);
        return func(cb_data, ...this.values);
    }
}
//# sourceMappingURL=customjs_ticker.js.map