import { ContinuousTicker } from "./continuous_ticker";
export class BaseSingleIntervalTicker extends ContinuousTicker {
    static __name__ = "BaseSingleIntervalTicker";
    constructor(attrs) {
        super(attrs);
    }
    get_interval(_data_low, _data_high, _n_desired_ticks) {
        return this.interval;
    }
    get_min_interval() {
        return this.interval;
    }
    get_max_interval() {
        return this.interval;
    }
}
export class SingleIntervalTicker extends BaseSingleIntervalTicker {
    static __name__ = "SingleIntervalTicker";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float }) => ({
            interval: [Float],
        }));
    }
    interval;
}
//# sourceMappingURL=single_interval_ticker.js.map