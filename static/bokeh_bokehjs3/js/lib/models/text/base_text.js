import { Model } from "../../model";
import { View } from "../../core/view";
export class BaseTextView extends View {
    static __name__ = "BaseTextView";
}
export class BaseText extends Model {
    static __name__ = "BaseText";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Str }) => ({
            text: [Str],
        }));
    }
}
//# sourceMappingURL=base_text.js.map