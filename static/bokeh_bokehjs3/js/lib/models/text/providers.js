import { Signal0 } from "../../core/signaling";
import { load_module } from "../../core/util/modules";
export class MathJaxProvider {
    static __name__ = "MathJaxProvider";
    ready = new Signal0(this, "ready");
    status = "not_started";
}
export class NoProvider extends MathJaxProvider {
    static __name__ = "NoProvider";
    get MathJax() {
        return null;
    }
    async fetch() {
        this.status = "failed";
    }
}
export class CDNProvider extends MathJaxProvider {
    static __name__ = "CDNProvider";
    get MathJax() {
        return typeof MathJax !== "undefined" ? MathJax : null;
    }
    async fetch() {
        const script = document.createElement("script");
        script.src = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js";
        script.onload = () => {
            this.status = "loaded";
            this.ready.emit();
        };
        script.onerror = () => {
            this.status = "failed";
        };
        this.status = "loading";
        document.head.appendChild(script);
    }
}
export class BundleProvider extends MathJaxProvider {
    static __name__ = "BundleProvider";
    _mathjax;
    get MathJax() {
        return this._mathjax;
    }
    async fetch() {
        this.status = "loading";
        try {
            const mathjax = await load_module(import("./mathjax"));
            this.status = mathjax == null ? "failed" : "loaded";
            this._mathjax = mathjax;
            this.ready.emit();
        }
        catch (error) {
            this.status = "failed";
        }
    }
}
export const default_provider = new BundleProvider();
//# sourceMappingURL=providers.js.map