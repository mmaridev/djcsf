import { Tool, ToolView } from "../tool";
import { OnOffButton } from "../on_off_button";
import * as p from "../../../core/properties";
export class InspectToolView extends ToolView {
    static __name__ = "InspectToolView";
    get plot_view() {
        return this.parent;
    }
}
export class InspectTool extends Tool {
    static __name__ = "InspectTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(() => ({
            toggleable: [new p.PropertyAlias("visible")],
        }));
        this.override({
            active: true,
        });
    }
    event_type = "move";
    tool_button() {
        return new OnOffButton({ tool: this });
    }
}
//# sourceMappingURL=inspect_tool.js.map