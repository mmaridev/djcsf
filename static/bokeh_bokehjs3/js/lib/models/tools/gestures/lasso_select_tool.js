import { RegionSelectTool, RegionSelectToolView } from "./region_select_tool";
import { PolyAnnotation } from "../../annotations/poly_annotation";
import { DEFAULT_POLY_OVERLAY } from "./poly_select_tool";
import { assert } from "../../../core/util/assert";
import { tool_icon_lasso_select } from "../../../styles/icons.css";
export class LassoSelectToolView extends RegionSelectToolView {
    static __name__ = "LassoSelectToolView";
    _is_selecting = false;
    _mappers() {
        const mapper = (units, scale, view, canvas) => {
            switch (units) {
                case "canvas": return canvas;
                case "screen": return view;
                case "data": return scale;
            }
        };
        const { overlay } = this.model;
        const { frame, canvas } = this.plot_view;
        const { x_scale, y_scale } = frame;
        const { x_view, y_view } = frame.bbox;
        const { x_screen, y_screen } = canvas.bbox;
        return {
            x: mapper(overlay.xs_units, x_scale, x_view, x_screen),
            y: mapper(overlay.ys_units, y_scale, y_view, y_screen),
        };
    }
    _v_compute(xs, ys) {
        const { x, y } = this._mappers();
        return [x.v_compute(xs), y.v_compute(ys)];
    }
    _v_invert(sxs, sys) {
        const { x, y } = this._mappers();
        return [x.v_invert(sxs), y.v_invert(sys)];
    }
    connect_signals() {
        super.connect_signals();
        const { pan } = this.model.overlay;
        this.connect(pan, ([phase, ev]) => {
            if ((phase == "pan" && this._is_continuous(ev)) || phase == "pan:end") {
                const { xs, ys } = this.model.overlay;
                const [sxs, sys] = this._v_compute(xs, ys);
                this._do_select(sxs, sys, false, this._select_mode(ev));
            }
        });
        const { active } = this.model.properties;
        this.on_change(active, () => {
            if (!this.model.active && !this.model.persistent) {
                this._clear_overlay();
            }
        });
    }
    _pan_start(ev) {
        const { sx, sy } = ev;
        const { frame } = this.plot_view;
        if (!frame.bbox.contains(sx, sy)) {
            return;
        }
        this._clear_other_overlays();
        this._is_selecting = true;
        const [xs, ys] = this._v_invert([sx], [sy]);
        this.model.overlay.update({ xs, ys });
    }
    _pan(ev) {
        assert(this._is_selecting);
        const [sxs, sys] = (() => {
            const { xs, ys } = this.model.overlay;
            const [sxs, sys] = this._v_compute(xs, ys);
            return [[...sxs], [...sys]];
        })();
        const [sx, sy] = this.plot_view.frame.bbox.clip(ev.sx, ev.sy);
        sxs.push(sx);
        sys.push(sy);
        const [xs, ys] = this._v_invert(sxs, sys);
        this.model.overlay.update({ xs, ys });
        if (this._is_continuous(ev.modifiers)) {
            this._do_select(sxs, sys, false, this._select_mode(ev.modifiers));
        }
    }
    _pan_end(ev) {
        assert(this._is_selecting);
        this._is_selecting = false;
        const { xs, ys } = this.model.overlay;
        const [sxs, sys] = this._v_compute(xs, ys);
        this._do_select(sxs, sys, true, this._select_mode(ev.modifiers));
        this.plot_view.state.push("lasso_select", { selection: this.plot_view.get_selection() });
        if (!this.model.persistent) {
            this._clear_overlay();
        }
    }
    _keyup(ev) {
        if (!this.model.active) {
            return;
        }
        if (ev.key == "Escape") {
            if (this.model.overlay.visible) {
                this._clear_overlay();
                return;
            }
        }
        super._keyup(ev);
    }
    _clear_selection() {
        if (this.model.overlay.visible) {
            this._clear_overlay();
        }
        else {
            super._clear_selection();
        }
    }
    _do_select(sx, sy, final, mode) {
        const { greedy } = this.model;
        const geometry = { type: "poly", sx, sy, greedy };
        this._select(geometry, final, mode);
    }
}
export class LassoSelectTool extends RegionSelectTool {
    static __name__ = "LassoSelectTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = LassoSelectToolView;
        this.define(({ Ref }) => ({
            overlay: [Ref(PolyAnnotation), DEFAULT_POLY_OVERLAY],
        }));
        this.override({
            continuous: true,
        });
        this.register_alias("lasso_select", () => new LassoSelectTool());
    }
    tool_name = "Lasso Select";
    tool_icon = tool_icon_lasso_select;
    event_type = "pan";
    default_order = 12;
}
//# sourceMappingURL=lasso_select_tool.js.map