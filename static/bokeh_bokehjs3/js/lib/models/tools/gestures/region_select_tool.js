import { SelectTool, SelectToolView } from "./select_tool";
import { RegionSelectionMode } from "../../../core/enums";
export class RegionSelectToolView extends SelectToolView {
    static __name__ = "RegionSelectToolView";
    get overlays() {
        return [...super.overlays, this.model.overlay];
    }
    _is_continuous(modifiers) {
        return this.model.continuous != modifiers.alt;
    }
    _select(geometry, final, mode) {
        const renderers_by_source = this._computed_renderers_by_data_source();
        for (const [, renderers] of renderers_by_source) {
            const sm = renderers[0].get_selection_manager();
            const r_views = [];
            for (const r of renderers) {
                const r_view = this.plot_view.views.find_one(r);
                if (r_view != null) {
                    r_views.push(r_view);
                }
            }
            sm.select(r_views, geometry, final, mode);
        }
        this._emit_selection_event(geometry, final);
    }
    _clear_overlay() {
        super._clear_overlay();
        this.model.overlay.clear();
    }
}
export class RegionSelectTool extends SelectTool {
    static __name__ = "RegionSelectTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Bool }) => ({
            mode: [RegionSelectionMode, "replace"],
            continuous: [Bool, false],
            persistent: [Bool, false],
            greedy: [Bool, false],
        }));
    }
}
//# sourceMappingURL=region_select_tool.js.map