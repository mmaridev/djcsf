import { ActionTool, ActionToolView } from "./action_tool";
import * as icons from "../../../styles/icons.css";
export class SaveToolView extends ActionToolView {
    static __name__ = "SaveToolView";
    async _export() {
        return this.parent.export().to_blob();
    }
    async copy() {
        const blob = await this._export();
        const item = new ClipboardItem({ [blob.type]: blob });
        await navigator.clipboard.write([item]);
    }
    async save(name) {
        const blob = await this._export();
        const link = document.createElement("a");
        link.href = URL.createObjectURL(blob);
        link.download = name; // + ".png" | "svg" (inferred from MIME type)
        link.target = "_blank";
        link.dispatchEvent(new MouseEvent("click"));
    }
    async open() {
        const blob = await this._export();
        const url = URL.createObjectURL(blob);
        open(url);
    }
    doit(action = "save") {
        switch (action) {
            case "save": {
                const filename = this.model.filename ?? prompt("Enter filename", "bokeh_plot");
                if (filename != null) {
                    void this.save(filename);
                }
                break;
            }
            case "copy": {
                void this.copy();
                break;
            }
            case "open": {
                void this.open();
                break;
            }
        }
    }
}
export class SaveTool extends ActionTool {
    static __name__ = "SaveTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = SaveToolView;
        this.define(({ Str, Nullable }) => ({
            filename: [Nullable(Str), null],
        }));
        this.register_alias("save", () => new SaveTool());
    }
    tool_name = "Save";
    tool_icon = icons.tool_icon_save;
    get menu() {
        return [
            {
                icon: icons.tool_icon_copy,
                tooltip: "Copy image to clipboard",
                if: () => typeof ClipboardItem !== "undefined",
                handler: () => {
                    this.do.emit("copy");
                },
            },
            {
                icon: icons.tool_icon_open,
                tooltip: "Open image in a new tab",
                handler: () => {
                    this.do.emit("open");
                },
            },
        ];
    }
}
//# sourceMappingURL=save_tool.js.map