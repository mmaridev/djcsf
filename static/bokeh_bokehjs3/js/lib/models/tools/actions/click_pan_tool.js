import { PlotActionTool, PlotActionToolView } from "./plot_action_tool";
import { Float } from "../../../core/kinds";
import { PanDirection } from "../../../core/enums";
import * as icons from "../../../styles/icons.css";
import { update_ranges } from "../gestures/pan_tool";
export class ClickPanToolView extends PlotActionToolView {
    static __name__ = "ClickPanToolView";
    doit() {
        const direction = (() => {
            switch (this.model.direction) {
                case "left":
                case "west":
                    return { x: -1, y: 0 };
                case "right":
                case "east":
                    return { x: +1, y: 0 };
                case "up":
                case "north":
                    return { x: 0, y: -1 };
                case "down":
                case "south":
                    return { x: 0, y: +1 };
            }
        })();
        const { frame } = this.plot_view;
        const { factor } = this.model;
        const x_offset = direction.x * factor * frame.bbox.width;
        const y_offset = direction.y * factor * frame.bbox.height;
        const bbox = frame.bbox.translate(x_offset, y_offset);
        const xrs = update_ranges(frame.x_scales, bbox.x0, bbox.x1);
        const yrs = update_ranges(frame.y_scales, bbox.y0, bbox.y1);
        this.plot_view.update_range({ xrs, yrs }, { panning: true });
    }
}
export class ClickPanTool extends PlotActionTool {
    static __name__ = "ClickPanTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ClickPanToolView;
        this.define(() => ({
            direction: [PanDirection],
            factor: [Float, 0.1],
        }));
        this.register_alias("pan_left", () => new ClickPanTool({ direction: "left" }));
        this.register_alias("pan_right", () => new ClickPanTool({ direction: "right" }));
        this.register_alias("pan_up", () => new ClickPanTool({ direction: "up" }));
        this.register_alias("pan_down", () => new ClickPanTool({ direction: "down" }));
        this.register_alias("pan_west", () => new ClickPanTool({ direction: "west" }));
        this.register_alias("pan_east", () => new ClickPanTool({ direction: "east" }));
        this.register_alias("pan_north", () => new ClickPanTool({ direction: "north" }));
        this.register_alias("pan_south", () => new ClickPanTool({ direction: "south" }));
    }
    tool_name = "Click Pan";
    get tooltip() {
        return `Pan ${this.direction}`;
    }
    get computed_icon() {
        const icon = super.computed_icon;
        if (icon != null) {
            return icon;
        }
        else {
            switch (this.direction) {
                case "left":
                case "west":
                    return `.${icons.tool_icon_pan_left}`;
                case "right":
                case "east":
                    return `.${icons.tool_icon_pan_right}`;
                case "up":
                case "north":
                    return `.${icons.tool_icon_pan_up}`;
                case "down":
                case "south":
                    return `.${icons.tool_icon_pan_down}`;
            }
        }
    }
}
//# sourceMappingURL=click_pan_tool.js.map