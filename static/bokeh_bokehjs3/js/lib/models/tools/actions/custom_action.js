import { ActionTool, ActionToolView } from "./action_tool";
import { execute } from "../../../core/util/callbacks";
import * as icons from "../../../styles/icons.css";
export class CustomActionView extends ActionToolView {
    static __name__ = "CustomActionView";
    doit() {
        const { callback } = this.model;
        if (callback != null) {
            void execute(callback, this.model);
        }
    }
}
export class CustomAction extends ActionTool {
    static __name__ = "CustomAction";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = CustomActionView;
        this.define(({ Any, Nullable }) => ({
            callback: [Nullable(Any /*TODO*/), null],
        }));
        this.override({
            description: "Perform a Custom Action",
        });
    }
    tool_name = "Custom Action";
    tool_icon = icons.tool_icon_unknown;
}
//# sourceMappingURL=custom_action.js.map