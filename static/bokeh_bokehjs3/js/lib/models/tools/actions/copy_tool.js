import { ActionTool, ActionToolView } from "./action_tool";
import { tool_icon_copy } from "../../../styles/icons.css";
export class CopyToolView extends ActionToolView {
    static __name__ = "CopyToolView";
    async copy() {
        const blob = await this.parent.export().to_blob();
        const item = new ClipboardItem({ [blob.type]: blob });
        await navigator.clipboard.write([item]);
    }
    doit() {
        void this.copy();
    }
}
export class CopyTool extends ActionTool {
    static __name__ = "CopyTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = CopyToolView;
        this.register_alias("copy", () => new CopyTool());
    }
    tool_name = "Copy";
    tool_icon = tool_icon_copy;
}
//# sourceMappingURL=copy_tool.js.map