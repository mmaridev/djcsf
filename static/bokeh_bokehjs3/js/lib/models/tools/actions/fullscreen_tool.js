import { ActionTool, ActionToolView } from "./action_tool";
import * as icons from "../../../styles/icons.css";
const request_fullscreen = (() => {
    if (typeof Element.prototype.webkitRequestFullscreen !== "undefined") {
        return (el, options) => el.webkitRequestFullscreen(options);
    }
    else {
        return (el, options) => el.requestFullscreen(options);
    }
})();
export class FullscreenToolView extends ActionToolView {
    static __name__ = "FullscreenToolView";
    async fullscreen() {
        if (document.fullscreenElement != null) {
            await document.exitFullscreen();
        }
        else {
            await request_fullscreen(this.parent.el);
        }
    }
    doit() {
        void this.fullscreen();
    }
}
export class FullscreenTool extends ActionTool {
    static __name__ = "FullscreenTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = FullscreenToolView;
        this.register_alias("fullscreen", () => new FullscreenTool());
    }
    tool_name = "Fullscreen";
    tool_icon = icons.tool_icon_fullscreen;
}
//# sourceMappingURL=fullscreen_tool.js.map