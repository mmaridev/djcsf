import { ActionTool, ActionToolView } from "./action_tool";
import { tool_icon_help } from "../../../styles/icons.css";
export class HelpToolView extends ActionToolView {
    static __name__ = "HelpToolView";
    doit() {
        window.open(this.model.redirect);
    }
}
export class HelpTool extends ActionTool {
    static __name__ = "HelpTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = HelpToolView;
        this.define(({ Str }) => ({
            redirect: [Str, "https://docs.bokeh.org/en/latest/docs/user_guide/interaction/tools.html"],
        }));
        this.override({
            description: "Click the question mark to learn more about Bokeh plot tools.",
        });
        this.register_alias("help", () => new HelpTool());
    }
    tool_name = "Help";
    tool_icon = tool_icon_help;
}
//# sourceMappingURL=help_tool.js.map