import { ActionTool, ActionToolView } from "./action_tool";
import * as icons from "../../../styles/icons.css";
import { Dialog } from "../../ui/dialog";
import { Examiner, HTMLPrinter } from "../../ui/examiner";
import { HTML } from "../../dom/html";
import { build_view } from "../../../core/build_views";
import { div } from "../../../core/dom";
import pretty_css from "../../../styles/pretty.css";
export class ExamineToolView extends ActionToolView {
    static __name__ = "ExamineToolView";
    _dialog;
    *children() {
        yield* super.children();
        yield this._dialog;
    }
    async lazy_initialize() {
        await super.lazy_initialize();
        const target = this.parent.model;
        const printer = new HTMLPrinter();
        const dialog = new Dialog({
            stylesheets: [pretty_css],
            title: new HTML({ html: div("Examine ", printer.to_html(target)) }),
            content: new Examiner({ target }),
            visible: false,
            close_action: "hide",
        });
        this._dialog = await build_view(dialog, { parent: this.parent });
    }
    doit() {
        this._dialog.open();
    }
}
export class ExamineTool extends ActionTool {
    static __name__ = "ExamineTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = ExamineToolView;
        this.register_alias("examine", () => new ExamineTool());
    }
    tool_name = "Examine";
    tool_icon = icons.tool_icon_settings; // TODO: better icon
}
//# sourceMappingURL=examine_tool.js.map