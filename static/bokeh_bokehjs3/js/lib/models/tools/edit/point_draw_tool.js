import { isField } from "../../../core/vectorization";
import { GlyphRenderer } from "../../renderers/glyph_renderer";
import { EditTool, EditToolView } from "./edit_tool";
import { tool_icon_point_draw } from "../../../styles/icons.css";
export class PointDrawToolView extends EditToolView {
    static __name__ = "PointDrawToolView";
    _tap(ev) {
        const renderers = this._select_event(ev, this._select_mode(ev), this.model.renderers);
        if (renderers.length != 0 || !this.model.add) {
            return;
        }
        const renderer = this.model.renderers[0];
        const point = this._map_drag(ev.sx, ev.sy, renderer);
        if (point == null) {
            return;
        }
        const { glyph, data_source } = renderer;
        const xkey = isField(glyph.x) ? glyph.x.field : null;
        const ykey = isField(glyph.y) ? glyph.y.field : null;
        const [x, y] = point;
        this._pop_glyphs(data_source, this.model.num_objects);
        if (xkey != null) {
            data_source.get_array(xkey).push(x);
        }
        if (ykey != null) {
            data_source.get_array(ykey).push(y);
        }
        this._pad_empty_columns(data_source, [xkey, ykey]);
        const { data } = data_source;
        data_source.setv({ data }, { check_eq: false }); // XXX: inplace updates
    }
    _keyup(ev) {
        if (!this.model.active || !this._mouse_in_frame) {
            return;
        }
        for (const renderer of this.model.renderers) {
            if (ev.key == "Backspace") {
                this._delete_selected(renderer);
            }
            else if (ev.key == "Escape") {
                renderer.data_source.selection_manager.clear();
            }
        }
    }
    _pan_start(ev) {
        if (!this.model.drag) {
            return;
        }
        this._select_event(ev, "append", this.model.renderers);
        this._basepoint = [ev.sx, ev.sy];
    }
    _pan(ev) {
        if (!this.model.drag || this._basepoint == null) {
            return;
        }
        this._drag_points(ev, this.model.renderers);
    }
    _pan_end(ev) {
        if (!this.model.drag) {
            return;
        }
        this._pan(ev);
        for (const renderer of this.model.renderers) {
            this._emit_cds_changes(renderer.data_source, false, true, true);
        }
        this._basepoint = null;
    }
}
export class PointDrawTool extends EditTool {
    static __name__ = "PointDrawTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = PointDrawToolView;
        this.define(({ Bool, Int, List, Ref }) => ({
            add: [Bool, true],
            drag: [Bool, true],
            num_objects: [Int, 0],
            renderers: [List(Ref((GlyphRenderer))), []],
        }));
    }
    tool_name = "Point Draw Tool";
    tool_icon = tool_icon_point_draw;
    event_type = ["tap", "pan", "move"];
    default_order = 2;
}
//# sourceMappingURL=point_draw_tool.js.map