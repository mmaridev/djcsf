import { isField } from "../../../core/vectorization";
import { dict } from "../../../core/util/object";
import { isArray } from "../../../core/util/types";
import { assert } from "../../../core/util/assert";
import { GlyphRenderer } from "../../renderers/glyph_renderer";
import { EditTool, EditToolView } from "./edit_tool";
export class PolyToolView extends EditToolView {
    static __name__ = "PolyToolView";
    _set_vertices(xs, ys) {
        const { vertex_renderer } = this.model;
        assert(vertex_renderer != null);
        const point_glyph = vertex_renderer.glyph;
        const point_cds = vertex_renderer.data_source;
        const pxkey = isField(point_glyph.x) ? point_glyph.x.field : null;
        const pykey = isField(point_glyph.y) ? point_glyph.y.field : null;
        const data = dict(point_cds.data);
        if (pxkey != null) {
            if (isArray(xs)) {
                data.set(pxkey, xs);
            }
            else {
                point_glyph.x = { value: xs };
            }
        }
        if (pykey != null) {
            if (isArray(ys)) {
                data.set(pykey, ys);
            }
            else {
                point_glyph.y = { value: ys };
            }
        }
        this._emit_cds_changes(point_cds, true, true, false);
    }
    _hide_vertices() {
        this._set_vertices([], []);
    }
    _snap_to_vertex(ev, x, y) {
        if (this.model.vertex_renderer != null) {
            // If an existing vertex is hit snap to it
            const vertex_selected = this._select_event(ev, "replace", [this.model.vertex_renderer]);
            const point_ds = this.model.vertex_renderer.data_source;
            // Type once dataspecs are typed
            const point_glyph = this.model.vertex_renderer.glyph;
            const pxkey = isField(point_glyph.x) ? point_glyph.x.field : null;
            const pykey = isField(point_glyph.y) ? point_glyph.y.field : null;
            if (vertex_selected.length != 0) {
                const index = point_ds.selected.indices[0];
                const data = dict(point_ds.data);
                if (pxkey != null) {
                    x = data.get(pxkey)[index];
                }
                if (pykey != null) {
                    y = data.get(pykey)[index];
                }
                point_ds.selection_manager.clear();
            }
        }
        return [x, y];
    }
}
export class PolyTool extends EditTool {
    static __name__ = "PolyTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Ref, Nullable }) => ({
            vertex_renderer: [Nullable(Ref((GlyphRenderer))), null],
        }));
    }
}
//# sourceMappingURL=poly_tool.js.map