import { isField } from "../../../core/vectorization";
import { dict } from "../../../core/util/object";
import { isArray } from "../../../core/util/types";
import { GlyphRenderer } from "../../renderers/glyph_renderer";
import { EditTool, EditToolView } from "./edit_tool";
export class LineToolView extends EditToolView {
    static __name__ = "LineToolView";
    _set_intersection(x, y) {
        const point_glyph = this.model.intersection_renderer.glyph;
        const point_cds = this.model.intersection_renderer.data_source;
        const data = dict(point_cds.data);
        const pxkey = isField(point_glyph.x) ? point_glyph.x.field : null;
        const pykey = isField(point_glyph.y) ? point_glyph.y.field : null;
        if (pxkey != null) {
            if (isArray(x)) {
                data.set(pxkey, x);
            }
            else {
                point_glyph.x = { value: x };
            }
        }
        if (pykey != null) {
            if (isArray(y)) {
                data.set(pykey, y);
            }
            else {
                point_glyph.y = { value: y };
            }
        }
        this._emit_cds_changes(point_cds, true, true, false);
    }
    _hide_intersections() {
        this._set_intersection([], []);
    }
}
export class LineTool extends EditTool {
    static __name__ = "LineTool";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Ref }) => ({
            intersection_renderer: [Ref((GlyphRenderer))],
        }));
    }
}
//# sourceMappingURL=line_tool.js.map