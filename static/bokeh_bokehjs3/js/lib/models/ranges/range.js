import { Model } from "../../model";
import { Nullable, Or, Tuple, Float, Auto } from "../../core/kinds";
import { map } from "../../core/util/iterator";
const Bounds = Nullable(Or(Tuple(Nullable(Float), Nullable(Float)), Auto));
export class Range extends Model {
    static __name__ = "Range";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float, Nullable }) => ({
            bounds: [Bounds, null, {
                    on_update(bounds, obj) {
                        const [lower, upper] = bounds == "auto" || bounds == null ? [null, null] : bounds;
                        obj._computed_bounds = [lower ?? -Infinity, upper ?? Infinity];
                    },
                }],
            min_interval: [Nullable(Float), null],
            max_interval: [Nullable(Float), null],
        }));
    }
    _computed_bounds;
    get computed_bounds() {
        return this._computed_bounds;
    }
    have_updated_interactively = false;
    get is_reversed() {
        return this.start > this.end;
    }
    get is_valid() {
        return isFinite(this.min) && isFinite(this.max);
    }
    get interval() {
        return [this.start, this.end];
    }
    get span() {
        return Math.abs(this.end - this.start);
    }
    /** @internal */
    frames = new Set();
    get linked_plots() {
        return new Set(map(this.frames, (frame) => frame.parent));
    }
}
//# sourceMappingURL=range.js.map