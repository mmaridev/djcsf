import { Range } from "./range";
import * as p from "../../core/properties";
import { clamp } from "../../core/util/math";
export class NumericalRange extends Range {
    static __name__ = "NumericalRange";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float }) => ({
            start: [Float, p.unset, {
                    convert(value, obj) {
                        const [lower, upper] = obj.computed_bounds;
                        return clamp(value, lower, upper);
                    },
                }],
            end: [Float, p.unset, {
                    convert(value, obj) {
                        const [lower, upper] = obj.computed_bounds;
                        return clamp(value, lower, upper);
                    },
                }],
        }));
    }
}
//# sourceMappingURL=numerical_range.js.map