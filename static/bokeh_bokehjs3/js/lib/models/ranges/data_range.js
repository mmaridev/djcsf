import { NumericalRange } from "./numerical_range";
export class DataRange extends NumericalRange {
    static __name__ = "DataRange";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ List, AnyRef, Or, Auto }) => ({
            renderers: [Or(List(AnyRef()), Auto), []],
        }));
        this.override({
            start: NaN,
            end: NaN,
        });
    }
}
//# sourceMappingURL=data_range.js.map