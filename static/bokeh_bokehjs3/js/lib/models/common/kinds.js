import { List, Auto, Enum, Int, Mapping, NonNegative, Opt, Or, PartialStruct, Percent, Ref, Str, Tuple, } from "../../core/kinds";
import * as enums from "../../core/enums";
export const Length = NonNegative(Int);
const XY = (type) => PartialStruct({ x: type, y: type });
const LRTB = (type) => PartialStruct({ left: type, right: type, top: type, bottom: type });
export const HAnchor = Or(enums.Align, enums.HAlign, Percent);
export const VAnchor = Or(enums.Align, enums.VAlign, Percent);
export const Anchor = Or(enums.Anchor, Tuple(HAnchor, VAnchor));
export const AutoAnchor = Or(Auto, enums.Anchor, Tuple(Or(HAnchor, Auto), Or(VAnchor, Auto)));
export const TextAnchor = Or(Anchor, Auto);
export const Padding = (Or(Length, Tuple(Length, Length), XY(Length), Tuple(Length, Length, Length, Length), LRTB(Length)));
export const BorderRadius = (Or(Length, Tuple(Length, Length, Length, Length), PartialStruct({
    top_left: Length,
    top_right: Length,
    bottom_right: Length,
    bottom_left: Length,
})));
export const Index = NonNegative(Int);
export const Span = NonNegative(Int);
export const GridChild = (child) => Tuple(Ref(child), Index, Index, Opt(Span), Opt(Span));
export const GridSpacing = Or(Length, Tuple(Length, Length));
export const TrackAlign = Enum("start", "center", "end", "auto");
export const TrackSize = Str;
export const TrackSizing = PartialStruct({ size: TrackSize, align: TrackAlign });
export const TrackSizingLike = Or(TrackSize, TrackSizing);
export const TracksSizing = Or(TrackSizingLike, List(TrackSizingLike), Mapping(Int, TrackSizingLike));
//# sourceMappingURL=kinds.js.map