import { Enum, Nullable, Float, Or, Ref } from "../../core/kinds";
import { Node } from "../coordinates/node";
export const Corner = Enum("top_left", "top_right", "bottom_left", "bottom_right");
export const Edge = Enum("left", "right", "top", "bottom");
export const HitTarget = Enum(...Corner, ...Edge, "area");
export const Resizable = Enum("none", "left", "right", "top", "bottom", "x", "y", "all");
export const Movable = Enum("none", "x", "y", "both");
export const Limit = Nullable(Or(Float, Ref(Node)));
//# sourceMappingURL=box_kinds.js.map