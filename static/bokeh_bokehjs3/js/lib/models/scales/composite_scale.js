import { Scale } from "../scales/scale";
import { map } from "../../core/util/arrayable";
export class CompositeScale extends Scale {
    static __name__ = "CompositeScale";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Ref }) => ({
            source_scale: [Ref(Scale)],
            target_scale: [Ref(Scale)],
        }));
    }
    get s_compute() {
        const source_compute = this.source_scale.s_compute;
        const target_compute = this.target_scale.s_compute;
        return (x) => target_compute(source_compute(x));
    }
    get s_invert() {
        const source_invert = this.source_scale.s_invert;
        const target_invert = this.target_scale.s_invert;
        return (sx) => source_invert(target_invert(sx));
    }
    compute(x) {
        return this.s_compute(x);
    }
    v_compute(xs) {
        const { s_compute } = this;
        return map(xs, s_compute); // XXX
    }
    invert(sx) {
        return this.s_invert(sx);
    }
    v_invert(sxs) {
        const { s_invert } = this;
        return map(sxs, s_invert); // XXX
    }
}
//# sourceMappingURL=composite_scale.js.map