import { LayoutDOM, LayoutDOMView } from "../layouts/layout_dom";
import { GridBox } from "../layouts/grid_box";
import { TracksSizing, GridChild, GridSpacing } from "../common/kinds";
import { Toolbar } from "../tools/toolbar";
import { ActionTool } from "../tools/actions/action_tool";
import { build_views, remove_views } from "../../core/build_views";
import { Location } from "../../core/enums";
export class GridPlotView extends LayoutDOMView {
    static __name__ = "GridPlotView";
    _grid_box;
    get toolbar_view() {
        return this.child_views.find((v) => v.model == this.model.toolbar);
    }
    get grid_box_view() {
        return this.child_views.find((v) => v.model == this._grid_box);
    }
    _update_location() {
        const location = this.model.toolbar_location;
        if (location == null) {
            this.model.toolbar.visible = false;
        }
        else {
            this.model.toolbar.setv({ visible: true, location });
        }
    }
    initialize() {
        super.initialize();
        this._update_location();
        const { children, rows, cols, spacing } = this.model;
        this._grid_box = new GridBox({ children, rows, cols, spacing, sizing_mode: "inherit" });
    }
    async lazy_initialize() {
        await super.lazy_initialize();
        await this.build_tool_views();
    }
    connect_signals() {
        super.connect_signals();
        const { toolbar, toolbar_location, children, rows, cols, spacing } = this.model.properties;
        this.on_change(toolbar_location, async () => {
            this._update_location();
            this.invalidate_layout();
        });
        this.on_change(toolbar, async () => {
            await this.update_children();
        });
        this.on_change([children, rows, cols, spacing], async () => {
            const { children, rows, cols, spacing } = this.model;
            this._grid_box.setv({ children, rows, cols, spacing });
            await this.grid_box_view.ready;
        });
        this.on_change(this.model.toolbar.properties.tools, async () => {
            await this.build_tool_views();
        });
        this.mouseenter.connect(() => {
            this.toolbar_view.set_visibility(true);
        });
        this.mouseleave.connect(() => {
            this.toolbar_view.set_visibility(false);
        });
    }
    remove() {
        remove_views(this._tool_views);
        super.remove();
    }
    _tool_views = new Map();
    async build_tool_views() {
        const tools = this.model.toolbar.tools.filter((tool) => tool instanceof ActionTool);
        await build_views(this._tool_views, tools, { parent: this });
    }
    *children() {
        yield* super.children();
        yield* this._tool_views.values();
    }
    get child_models() {
        return [this.model.toolbar, this._grid_box];
    }
    _intrinsic_display() {
        return { inner: this.model.flow_mode, outer: "flex" };
    }
    _update_layout() {
        super._update_layout();
        const { location } = this.model.toolbar;
        const flex_direction = (() => {
            switch (location) {
                case "above": return "column";
                case "below": return "column-reverse";
                case "left": return "row";
                case "right": return "row-reverse";
            }
        })();
        this.style.append(":host", { flex_direction });
    }
}
export class GridPlot extends LayoutDOM {
    static __name__ = "GridPlot";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = GridPlotView;
        this.define(({ List, Ref, Nullable }) => ({
            toolbar: [Ref(Toolbar), () => new Toolbar()],
            toolbar_location: [Nullable(Location), "above"],
            children: [List(GridChild(LayoutDOM)), []],
            rows: [Nullable(TracksSizing), null],
            cols: [Nullable(TracksSizing), null],
            spacing: [GridSpacing, 0],
        }));
    }
}
//# sourceMappingURL=grid_plot.js.map