import { GMapPlot, GMapPlotView } from "./gmap_plot";
export class GMapView extends GMapPlotView {
    static __name__ = "GMapView";
}
export class GMap extends GMapPlot {
    static __name__ = "GMap";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = GMapView;
    }
}
//# sourceMappingURL=gmap.js.map