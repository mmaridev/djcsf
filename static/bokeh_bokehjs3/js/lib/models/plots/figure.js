import { Plot, PlotView } from "./plot";
export class FigureView extends PlotView {
    static __name__ = "FigureView";
}
export class Figure extends Plot {
    static __name__ = "Figure";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = FigureView;
    }
}
//# sourceMappingURL=figure.js.map