import { Model } from "../../model";
import * as mixins from "../../core/property_mixins";
export class AreaVisuals extends Model {
    static __name__ = "AreaVisuals";
    constructor(attrs) {
        super(attrs);
    }
    clone(attrs) {
        return super.clone(attrs);
    }
    static {
        this.mixins([
            mixins.Line,
            mixins.Fill,
            mixins.Hatch,
            ["hover_", mixins.Line],
            ["hover_", mixins.Fill],
            ["hover_", mixins.Hatch],
        ]);
    }
}
//# sourceMappingURL=area_visuals.js.map