import { Marking, MarkingView } from "../graphics/marking";
import { LineVector, FillVector } from "../../core/property_mixins";
import * as p from "../../core/properties";
export class ArrowHeadView extends MarkingView {
    static __name__ = "ArrowHeadView";
}
export class ArrowHead extends Marking {
    static __name__ = "ArrowHead";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(() => ({
            size: [p.NumberSpec, 25],
        }));
    }
}
export class OpenHeadView extends ArrowHeadView {
    static __name__ = "OpenHeadView";
    clip(ctx, i) {
        this.visuals.line.set_vectorize(ctx, i);
        const size_i = this.size.get(i);
        ctx.moveTo(0.5 * size_i, size_i);
        ctx.lineTo(0.5 * size_i, -2);
        ctx.lineTo(-0.5 * size_i, -2);
        ctx.lineTo(-0.5 * size_i, size_i);
        ctx.lineTo(0, 0);
        ctx.lineTo(0.5 * size_i, size_i);
    }
    paint(ctx, i) {
        const size_i = this.size.get(i);
        ctx.beginPath();
        ctx.moveTo(0.5 * size_i, size_i);
        ctx.lineTo(0, 0);
        ctx.lineTo(-0.5 * size_i, size_i);
        this.visuals.line.apply(ctx, i);
    }
}
export class OpenHead extends ArrowHead {
    static __name__ = "OpenHead";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = OpenHeadView;
        this.mixins(LineVector);
    }
}
export class NormalHeadView extends ArrowHeadView {
    static __name__ = "NormalHeadView";
    clip(ctx, i) {
        this.visuals.line.set_vectorize(ctx, i);
        const size_i = this.size.get(i);
        ctx.moveTo(0.5 * size_i, size_i);
        ctx.lineTo(0.5 * size_i, -2);
        ctx.lineTo(-0.5 * size_i, -2);
        ctx.lineTo(-0.5 * size_i, size_i);
        ctx.lineTo(0.5 * size_i, size_i);
    }
    paint(ctx, i) {
        const size_i = this.size.get(i);
        ctx.beginPath();
        ctx.moveTo(0.5 * size_i, size_i);
        ctx.lineTo(0, 0);
        ctx.lineTo(-0.5 * size_i, size_i);
        ctx.closePath();
        this.visuals.fill.apply(ctx, i);
        this.visuals.line.apply(ctx, i);
    }
}
export class NormalHead extends ArrowHead {
    static __name__ = "NormalHead";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = NormalHeadView;
        this.mixins([LineVector, FillVector]);
        this.override({
            fill_color: "black",
        });
    }
}
export class VeeHeadView extends ArrowHeadView {
    static __name__ = "VeeHeadView";
    clip(ctx, i) {
        this.visuals.line.set_vectorize(ctx, i);
        const size_i = this.size.get(i);
        ctx.moveTo(0.5 * size_i, size_i);
        ctx.lineTo(0.5 * size_i, -2);
        ctx.lineTo(-0.5 * size_i, -2);
        ctx.lineTo(-0.5 * size_i, size_i);
        ctx.lineTo(0, 0.5 * size_i);
        ctx.lineTo(0.5 * size_i, size_i);
    }
    paint(ctx, i) {
        const size_i = this.size.get(i);
        ctx.beginPath();
        ctx.moveTo(0.5 * size_i, size_i);
        ctx.lineTo(0, 0);
        ctx.lineTo(-0.5 * size_i, size_i);
        ctx.lineTo(0, 0.5 * size_i);
        ctx.closePath();
        this.visuals.fill.apply(ctx, i);
        this.visuals.line.apply(ctx, i);
    }
}
export class VeeHead extends ArrowHead {
    static __name__ = "VeeHead";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = VeeHeadView;
        this.mixins([LineVector, FillVector]);
        this.override({
            fill_color: "black",
        });
    }
}
export class TeeHeadView extends ArrowHeadView {
    static __name__ = "TeeHeadView";
    paint(ctx, i) {
        const size_i = this.size.get(i);
        ctx.beginPath();
        ctx.moveTo(0.5 * size_i, 0);
        ctx.lineTo(-0.5 * size_i, 0);
        this.visuals.line.apply(ctx, i);
    }
    clip(_ctx, _i) { }
}
export class TeeHead extends ArrowHead {
    static __name__ = "TeeHead";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = TeeHeadView;
        this.mixins(LineVector);
    }
}
//# sourceMappingURL=arrow_head.js.map