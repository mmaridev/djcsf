import { UpperLower, UpperLowerView } from "./upper_lower";
import * as mixins from "../../core/property_mixins";
export class BandView extends UpperLowerView {
    static __name__ = "BandView";
    _paint_data(ctx) {
        // Draw the band body
        ctx.beginPath();
        ctx.moveTo(this._lower_sx[0], this._lower_sy[0]);
        for (let i = 0, end = this._lower_sx.length; i < end; i++) {
            ctx.lineTo(this._lower_sx[i], this._lower_sy[i]);
        }
        // iterate backwards so that the upper end is below the lower start
        for (let i = this._upper_sx.length - 1; i >= 0; i--) {
            ctx.lineTo(this._upper_sx[i], this._upper_sy[i]);
        }
        ctx.closePath();
        this.visuals.fill.apply(ctx);
        // Draw the lower band edge
        ctx.beginPath();
        ctx.moveTo(this._lower_sx[0], this._lower_sy[0]);
        for (let i = 0, end = this._lower_sx.length; i < end; i++) {
            ctx.lineTo(this._lower_sx[i], this._lower_sy[i]);
        }
        this.visuals.line.apply(ctx);
        // Draw the upper band edge
        ctx.beginPath();
        ctx.moveTo(this._upper_sx[0], this._upper_sy[0]);
        for (let i = 0, end = this._upper_sx.length; i < end; i++) {
            ctx.lineTo(this._upper_sx[i], this._upper_sy[i]);
        }
        this.visuals.line.apply(ctx);
    }
}
export class Band extends UpperLower {
    static __name__ = "Band";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.prototype.default_view = BandView;
        this.mixins([mixins.Line, mixins.Fill]);
        this.override({
            fill_color: "#fff9ba",
            fill_alpha: 0.4,
            line_color: "#cccccc",
            line_alpha: 0.3,
        });
    }
}
//# sourceMappingURL=band.js.map