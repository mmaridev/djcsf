import { Model } from "../../model";
import { keys, values } from "../../core/util/object";
import { use_strict } from "../../core/util/string";
import { isIterable } from "../../core/util/types";
import { Indices, GeneratorFunction } from "../../core/types";
export class LabelingPolicy extends Model {
    static __name__ = "LabelingPolicy";
    constructor(attrs) {
        super(attrs);
    }
}
export class AllLabels extends LabelingPolicy {
    static __name__ = "AllLabels";
    constructor(attrs) {
        super(attrs);
    }
    filter(indices, _bboxes, _distance) {
        return indices;
    }
}
export class NoOverlap extends LabelingPolicy {
    static __name__ = "NoOverlap";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Float }) => ({
            min_distance: [Float, 5],
        }));
    }
    filter(indices, _bboxes, distance) {
        const { min_distance } = this;
        let k = null;
        for (const i of indices) {
            if (k != null && distance(k, i) < min_distance) {
                indices.unset(i);
            }
            else {
                k = i;
            }
        }
        return indices;
    }
}
export class CustomLabelingPolicy extends LabelingPolicy {
    static __name__ = "CustomLabelingPolicy";
    constructor(attrs) {
        super(attrs);
    }
    static {
        this.define(({ Unknown, Str, Dict }) => ({
            args: [Dict(Unknown), {}],
            code: [Str, ""],
        }));
    }
    get names() {
        return keys(this.args);
    }
    get values() {
        return values(this.args);
    }
    get func() {
        const code = use_strict(this.code);
        return new GeneratorFunction("indices", "bboxes", "distance", ...this.names, code);
    }
    filter(indices, bboxes, distance) {
        const obj = Object.create(null);
        const generator = this.func.call(obj, indices, bboxes, distance, ...this.values);
        let result = generator.next();
        if ((result.done ?? false) && result.value !== undefined) {
            const { value } = result;
            if (value instanceof Indices) {
                return value;
            }
            else if (value === undefined) {
                return indices;
            }
            else if (isIterable(value)) {
                return Indices.from_indices(indices.size, value);
            }
            else {
                return Indices.all_unset(indices.size);
            }
        }
        else {
            const array = [];
            do {
                array.push(result.value);
                result = generator.next();
            } while (!(result.done ?? false));
            return Indices.from_indices(indices.size, array);
        }
    }
}
//# sourceMappingURL=labeling.js.map