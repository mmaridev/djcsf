import { AnnularWedge, Annulus, Arc, Bezier, Block, Circle, Ellipse, HArea, HAreaStep, HBar, HSpan, HStrip, HexTile, Image, ImageRGBA, ImageStack, ImageURL, Line, MathMLGlyph as MathML, MultiLine, MultiPolygons, Ngon, Patch, Patches, Quad, Quadratic, Ray, Rect, Scatter, Segment, Spline, Step, TeXGlyph as TeX, Text, VArea, VAreaStep, VBar, VSpan, VStrip, Wedge, } from "../models/glyphs";
export class GlyphAPI {
    static __name__ = "GlyphAPI";
    annular_wedge(...args) {
        return this._glyph(AnnularWedge, "annular_wedge", ["x", "y", "inner_radius", "outer_radius", "start_angle", "end_angle"], args);
    }
    annulus(...args) {
        return this._glyph(Annulus, "annulus", ["x", "y", "inner_radius", "outer_radius"], args);
    }
    arc(...args) {
        return this._glyph(Arc, "arc", ["x", "y", "radius", "start_angle", "end_angle"], args);
    }
    bezier(...args) {
        return this._glyph(Bezier, "bezier", ["x0", "y0", "x1", "y1", "cx0", "cy0", "cx1", "cy1"], args);
    }
    block(...args) {
        return this._glyph(Block, "block", ["x", "y", "width", "height"], args);
    }
    circle(...args) {
        return this._glyph(Circle, "circle", ["x", "y", "radius"], args);
    }
    ellipse(...args) {
        return this._glyph(Ellipse, "ellipse", ["x", "y", "width", "height"], args);
    }
    harea(...args) {
        return this._glyph(HArea, "harea", ["x1", "x2", "y"], args);
    }
    harea_step(...args) {
        return this._glyph(HAreaStep, "harea_step", ["x1", "x2", "y", "step_mode"], args);
    }
    hbar(...args) {
        return this._glyph(HBar, "hbar", ["y", "height", "right", "left"], args);
    }
    hspan(...args) {
        return this._glyph(HSpan, "hspan", ["y"], args);
    }
    hstrip(...args) {
        return this._glyph(HStrip, "hstrip", ["y0", "y1"], args);
    }
    hex_tile(...args) {
        return this._glyph(HexTile, "hex_tile", ["q", "r"], args);
    }
    image(...args) {
        return this._glyph(Image, "image", ["color_mapper", "image", "x", "y", "dw", "dh"], args);
    }
    image_stack(...args) {
        return this._glyph(ImageStack, "image_stack", ["color_mapper", "image", "x", "y", "dw", "dh"], args);
    }
    image_rgba(...args) {
        return this._glyph(ImageRGBA, "image_rgba", ["image", "x", "y", "dw", "dh"], args);
    }
    image_url(...args) {
        return this._glyph(ImageURL, "image_url", ["url", "x", "y", "w", "h"], args);
    }
    line(...args) {
        return this._glyph(Line, "line", ["x", "y"], args);
    }
    mathml(...args) {
        return this._glyph(MathML, "mathml", ["x", "y", "text"], args);
    }
    multi_line(...args) {
        return this._glyph(MultiLine, "multi_line", ["xs", "ys"], args);
    }
    multi_polygons(...args) {
        return this._glyph(MultiPolygons, "multi_polygons", ["xs", "ys"], args);
    }
    ngon(...args) {
        return this._glyph(Ngon, "ngon", ["x", "y", "radius"], args);
    }
    patch(...args) {
        return this._glyph(Patch, "patch", ["x", "y"], args);
    }
    patches(...args) {
        return this._glyph(Patches, "patches", ["xs", "ys"], args);
    }
    quad(...args) {
        return this._glyph(Quad, "quad", ["left", "right", "bottom", "top"], args);
    }
    quadratic(...args) {
        return this._glyph(Quadratic, "quadratic", ["x0", "y0", "x1", "y1", "cx", "cy"], args);
    }
    ray(...args) {
        return this._glyph(Ray, "ray", ["x", "y", "length"], args);
    }
    rect(...args) {
        return this._glyph(Rect, "rect", ["x", "y", "width", "height"], args);
    }
    segment(...args) {
        return this._glyph(Segment, "segment", ["x0", "y0", "x1", "y1"], args);
    }
    spline(...args) {
        return this._glyph(Spline, "spline", ["x", "y"], args);
    }
    step(...args) {
        return this._glyph(Step, "step", ["x", "y", "mode"], args);
    }
    tex(...args) {
        return this._glyph(TeX, "tex", ["x", "y", "text"], args);
    }
    text(...args) {
        return this._glyph(Text, "text", ["x", "y", "text"], args);
    }
    varea(...args) {
        return this._glyph(VArea, "varea", ["x", "y1", "y2"], args);
    }
    varea_step(...args) {
        return this._glyph(VAreaStep, "varea_step", ["x", "y1", "y2", "step_mode"], args);
    }
    vbar(...args) {
        return this._glyph(VBar, "vbar", ["x", "width", "top", "bottom"], args);
    }
    vspan(...args) {
        return this._glyph(VSpan, "vspan", ["x"], args);
    }
    vstrip(...args) {
        return this._glyph(VStrip, "vstrip", ["x0", "x1"], args);
    }
    wedge(...args) {
        return this._glyph(Wedge, "wedge", ["x", "y", "radius", "start_angle", "end_angle"], args);
    }
    _scatter(args, marker) {
        return this._glyph(Scatter, marker ?? "scatter", ["x", "y"], args, marker != null ? { marker } : undefined);
    }
    scatter(...args) {
        return this._scatter(args);
    }
    /** @deprecated */ asterisk(...args) {
        return this._scatter(args, "asterisk");
    }
    /** @deprecated */ circle_cross(...args) {
        return this._scatter(args, "circle_cross");
    }
    /** @deprecated */ circle_dot(...args) {
        return this._scatter(args, "circle_dot");
    }
    /** @deprecated */ circle_x(...args) {
        return this._scatter(args, "circle_x");
    }
    /** @deprecated */ circle_y(...args) {
        return this._scatter(args, "circle_y");
    }
    /** @deprecated */ cross(...args) {
        return this._scatter(args, "cross");
    }
    /** @deprecated */ dash(...args) {
        return this._scatter(args, "dash");
    }
    /** @deprecated */ diamond(...args) {
        return this._scatter(args, "diamond");
    }
    /** @deprecated */ diamond_cross(...args) {
        return this._scatter(args, "diamond_cross");
    }
    /** @deprecated */ diamond_dot(...args) {
        return this._scatter(args, "diamond_dot");
    }
    /** @deprecated */ dot(...args) {
        return this._scatter(args, "dot");
    }
    /** @deprecated */ hex(...args) {
        return this._scatter(args, "hex");
    }
    /** @deprecated */ hex_dot(...args) {
        return this._scatter(args, "hex_dot");
    }
    /** @deprecated */ inverted_triangle(...args) {
        return this._scatter(args, "inverted_triangle");
    }
    /** @deprecated */ plus(...args) {
        return this._scatter(args, "plus");
    }
    /** @deprecated */ square(...args) {
        return this._scatter(args, "square");
    }
    /** @deprecated */ square_cross(...args) {
        return this._scatter(args, "square_cross");
    }
    /** @deprecated */ square_dot(...args) {
        return this._scatter(args, "square_dot");
    }
    /** @deprecated */ square_pin(...args) {
        return this._scatter(args, "square_pin");
    }
    /** @deprecated */ square_x(...args) {
        return this._scatter(args, "square_x");
    }
    /** @deprecated */ star(...args) {
        return this._scatter(args, "star");
    }
    /** @deprecated */ star_dot(...args) {
        return this._scatter(args, "star_dot");
    }
    /** @deprecated */ triangle(...args) {
        return this._scatter(args, "triangle");
    }
    /** @deprecated */ triangle_dot(...args) {
        return this._scatter(args, "triangle_dot");
    }
    /** @deprecated */ triangle_pin(...args) {
        return this._scatter(args, "triangle_pin");
    }
    /** @deprecated */ x(...args) {
        return this._scatter(args, "x");
    }
    /** @deprecated */ y(...args) {
        return this._scatter(args, "y");
    }
}
//# sourceMappingURL=glyph_api.js.map