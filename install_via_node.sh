#!/bin/bash

# Stop on failures
set -e

while IFS= read -r line; do
  i=$(echo "$line" | cut -d ';' -f 1)
  pack_name=$(echo "$line" | cut -d ';' -f 2)
  lib=$(python3 -c "print('$i'[::-1].split('@', 1)[1][::-1])")
  ver=$(echo "$i"|cut -d @ -f 2)
  echo "Installing $i $lib $ver $pack_name"
  npm i "$i"
  rm -rf "static/$pack_name"
  if [ -e "node_modules/$lib/dist" ] ; then
    cp -R "node_modules/$lib/dist" "static/$pack_name"
  elif [ -e "node_modules/$lib/build" ]; then
    cp -R "node_modules/$lib/build" "static/$pack_name"
  elif [ -e "node_modules/$lib/media" ]; then
    cp -R "node_modules/$lib/media" "static/$pack_name"
  else
    cp -R "node_modules/$lib" "static/$pack_name"
  fi
  rm -rf node_modules package*.json
  echo "$lib $ver installed"
done < libraries.txt
