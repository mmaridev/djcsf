#!/usr/bin/python3
# Parse latest versions and update libraries.txt accordingly

import subprocess, json

LIBRARIES = [
    # [ name, [ major versions ] ]
    ["@bokeh/bokehjs", ["3"]],
    ["bootstrap", ["4", "5"]],
    ["bootswatch", ["4", "5"]],
    ["chart.js", ["4"]],
    ["clockpicker", ["0"]],
    ["datatables", ["1"]],
    ["drawflow", ["0"]],
    ["flot", ["4"]],
    ["@fortawesome/fontawesome-free", ["5", "6"]],
    ["fullcalendar", ["3", "6"]],
    ["jquery", ["3"]],
    ["jquery-ui", ["1"]],
    ["leaflet", ["1"]],
    ["modernizr", ["3"]],
    ["moment", ["2"]],
    ["@popperjs/core", ["2"]],
    ["select2", ["4"]],
    ["summernote", ["0"]],
    ["tabulator-tables", ["5", "6"]],
]

with open("libraries.txt", "w") as dest:
    for lib in LIBRARIES:
        # Get all versions from NPM
        output = subprocess.run(
            ["npm", "view", lib[0], "versions", "--json"],
            capture_output=True, text=True
        )
        versions = [
            x for x in json.loads(output.stdout)
            if 'alpha' not in x and 'beta' not in x and 'rc' not in x and 'dev' not in x
        ]
        search_versions = lib[1][::]
        # Parse in reverse order since NPM gives them ordered by release
        for i in versions[::-1]:
            for k in search_versions:
                if i.startswith(k):
                    name = lib[0]
                    if "/" in name:
                        name = name.replace("@", "").replace("/", "_")
                    dest.write(f"{lib[0]}@{i};{name}{k}\n")
                    search_versions.remove(k)
        if search_versions:
            print("WARNING: the following versions could not be found for", lib[0], search_versions)

